/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "CShotgun.hpp"

#define PROJECTILE_BUFFER_SIZE 25

namespace daidalos {

	/**
	* Constructor
	* @param parent The parent
	* @param playerNode The player node
	*/
	CShotgun::CShotgun(daidalosgameengine::ILevel * parent, daidalosengine::ISceneNode * playerNode) : IWeapon(parent) {
		// Initialize the weapon stats
		SetMagazineStats(7, 7);
		SetAmmoStats(0, 43);
		SetOffensiveStats(12, 5, 65, 100);
		SetFireStats(1.0F, 1200.0F);
		SetReloadStats(1.0F, 2800.0F);

		// Initialize the sounds
		SetFireSound("shotgun_fire.wav");
		SetReloadSound("shotgun_reload.wav");

		// Initialize the projectiles & particles
		InitializeProjectileBuffer(PROJECTILE_BUFFER_SIZE);
		InitializeWeaponNode(playerNode);
		InitializeParticleEmitters();
		InitializeFireSmokeEmitter();
		InitializeFireFlashEmitter();
	}

	/**
	* Destructor
	*/
	CShotgun::~CShotgun() = default;

	/**
	* Initialize the weapon node
	* @param playerNode The player node to attach the weapon node to
	*/
	void CShotgun::InitializeWeaponNode(daidalosengine::ISceneNode * playerNode) {
	
	}

	/**
	* Initialize the particle emitters
	*/
	void CShotgun::InitializeParticleEmitters() {
		for(int i = 0; i < PROJECTILE_BUFFER_SIZE; i++) {
			daidalosengine::CParticleEmitter * emitter = GetParent()->GetScene()->GetParticleSystem().InstanciateEmitter("default.dept", "environmentImpactEmitter");
			emitter->SetTextureId(1);
			emitter->SetActive(false);
			emitter->SetReusable(true);
			AddParticleEmitter(emitter);
		}
	}

	/**
	* Initialize the smoke emitter
	*/
	void CShotgun::InitializeFireSmokeEmitter() {
		daidalosengine::CParticleEmitter * emitter = GetParent()->GetScene()->GetParticleSystem().InstanciateEmitter("default.dept", "weaponSmokeEmitter");
		emitter->SetTextureId(1);
		emitter->SetReusable(true);

		daidalosengine::CParticleEmitterSceneNode * node = new daidalosengine::CParticleEmitterSceneNode(GetParent()->GetScene()->GetScene(), GetMeshNode(), "weaponSmokeEmitter", emitter);
		node->SetPosition(daidalosengine::CVector3F(0, 0, 2.0F));
		SetSmokeEmitter(node);
	}

	/**
	* Initialize the flash emitter
	*/
	void CShotgun::InitializeFireFlashEmitter() {
		daidalosengine::CParticleEmitter * emitter = GetParent()->GetScene()->GetParticleSystem().InstanciateEmitter("default.dept", "weaponFlashEmitter");
		emitter->SetTextureId(1);
		emitter->SetReusable(true);

		daidalosengine::CParticleEmitterSceneNode * node = new daidalosengine::CParticleEmitterSceneNode(GetParent()->GetScene()->GetScene(), GetMeshNode(), "weaponFlashEmitter", emitter);
		node->SetPosition(daidalosengine::CVector3F(0, 0, 2.0F));
		SetFlashEmitter(node);
	}
}

