/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "IWeapon.hpp"

#define PARTICLE_LIFE 50
#define PARTICLE_DIRECTION_DECAL 0.08F
#define WOBBLE_SPEED 9.0F
#define WOBBLE_IMPULSE_ATTENUATION 50.0F
#define FIRE_LIGHT_TIMER 80.0F
#define SMOKE_TIMER 3000.0F

namespace daidalos {

	/**
	* Constructor
	* @param parent The parent
	*/
	IWeapon::IWeapon(daidalosgameengine::ILevel * parent) : m_parent(parent) {
		this->m_state = WEAPON_STATE_NONE;
		this->m_damage = 0;
		this->m_magazineSize = 0;
		this->m_maxMagazineSize = 0;
		this->m_remainingAmmo = 0;
		this->m_maxAmmo = 0;
		this->m_precision = 90;
		this->m_hitCount = 1;
		this->m_fireAnimationRate = 1.0F;
		this->m_reloadAnimationRate = 1.0F;
		this->m_fireDelay = 0;
		this->m_reloadDelay = 0;
		this->m_remainingDelay = 0;
		this->m_smokeRemainingDelay = 0;
		this->m_wobblePositionTime = 0;
		this->m_range = 100;
		this->m_fireLight = nullptr;
		this->m_flashEmitter = nullptr;
		this->m_smokeEmitter = nullptr;
	}

	/**
	* Destructor
	*/
	IWeapon::~IWeapon() = default;

	/**
	* Fire the weapon
	*/
	void IWeapon::Fire() {

		// Only if there is ammo left in the magazine
		if(this->m_state == WEAPON_STATE_NONE && this->m_magazineSize > 0) {
			this->m_magazineSize--;

			// Play the sound
			this->m_fireSound.SetStatus(daidalosengine::SOUND_STATUS_PLAY);

			// Start the animation
			if(this->m_meshNode != nullptr) {
				this->m_meshNode->StartAnimation(this->m_fireAnimation.first, this->m_fireAnimation.second, this->m_fireAnimationRate);
			}

			// Generate the bullets
			for(unsigned int bullet = 0; bullet < this->m_hitCount; bullet++) {

				int projectileIndex = this->m_projectiles.New();
				if(projectileIndex > -1) {

					// Add a direction variation
					daidalosengine::CVector3F directionAltered = this->m_parent->GetScene()->GetActiveCamera()->GetForwardVector();
					directionAltered.x += daidalosengine::RandomNumber<float>(-(100 - static_cast<float>(this->m_precision)) / 400.0F, (100 - static_cast<float>(this->m_precision)) / 400.0F);
					directionAltered.y += daidalosengine::RandomNumber<float>(-(100 - static_cast<float>(this->m_precision)) / 400.0F, (100 - static_cast<float>(this->m_precision)) / 400.0F);
					directionAltered.z += daidalosengine::RandomNumber<float>(-(100 - static_cast<float>(this->m_precision)) / 400.0F, (100 - static_cast<float>(this->m_precision)) / 400.0F);
					directionAltered = directionAltered.Normalize();

					// Create the projectile
					SProjectile & projectile = this->m_projectiles.GetReferenceAt(projectileIndex);
					projectile.damage = this->m_damage;
					projectile.direction = directionAltered;
					projectile.distance = this->m_range;
					projectile.position = this->m_parent->GetScene()->GetActiveCamera()->GetAbsolutePosition();
					this->m_activeProjectiles.push_back(projectileIndex);
				}
				
			}

			// Update the state and the delay
			this->m_state = WEAPON_STATE_FIRE;
			this->m_remainingDelay = this->m_fireDelay;

			// Enable the fire light
			if(this->m_fireLight != nullptr) {
				this->m_fireLight->SetEnabled(true);
			}

			// Stop the smoke emitter
			this->m_smokeEmitter->SetActive(false);
			this->m_smokeRemainingDelay = 0;

			// Start the flash emitter
			this->m_flashEmitter->SetActive(true);
		}
	}

	/**
	* Reload the weapon
	*/
	void IWeapon::Reload() {
		// Start the animation
		if(this->m_state == WEAPON_STATE_NONE && this->m_magazineSize < this->m_maxMagazineSize && this->m_remainingAmmo > 0) {

			// Start the animation
			if (this->m_meshNode != nullptr) {
				this->m_meshNode->StartAnimation(this->m_reloadAnimation.first, this->m_reloadAnimation.second, this->m_reloadAnimationRate);
			}

			// Update the state and the delay
			this->m_state = WEAPON_STATE_RELOAD;
			this->m_remainingDelay = this->m_reloadDelay;

			// Play the sound
			this->m_reloadSound.SetStatus(daidalosengine::SOUND_STATUS_PLAY);

			// Stop the smoke emitter
			this->m_smokeEmitter->SetActive(false);
			this->m_smokeRemainingDelay = 0;
		}
	}

	/**
	* Add ammo
	* @param ammo The ammo count
	*/
	void IWeapon::AddAmmo(const unsigned int ammo) {
		this->m_remainingAmmo += ammo;
		if(this->m_remainingAmmo > this->m_maxAmmo) {
			this->m_remainingAmmo = this->m_maxAmmo;
		}
	}

	/**
	* Update the weapon
	* @param time elapsed time
	*/
	void IWeapon::Update(const float time) {

		const float timeSecond = time / 1000.0F;

		// Apply vertical movement
		if(this->m_state == WEAPON_STATE_NONE && this->m_parent->GetPlayer()->HasMoved()) {
			this->m_wobblePositionTime += timeSecond * WOBBLE_SPEED;
			this->m_meshNode->SetPosition(daidalosengine::CVector3F(this->m_meshNode->GetPosition().x, -1.3F - (cosf(this->m_wobblePositionTime) / WOBBLE_IMPULSE_ATTENUATION), this->m_meshNode->GetPosition().z));
		}

		if(this->m_state == WEAPON_STATE_FIRE || this->m_state == WEAPON_STATE_RELOAD) {
			this->m_remainingDelay -= time;

			// Update the fire light
			if ((this->m_fireDelay - this->m_remainingDelay) > FIRE_LIGHT_TIMER && this->m_fireLight != nullptr) {
				this->m_fireLight->SetEnabled(false);
			}
		}

		// Update the state
		if(this->m_state == WEAPON_STATE_FIRE && this->m_remainingDelay <= 0 && (this->m_meshNode == nullptr || (this->m_meshNode != nullptr && this->m_meshNode->GetAnimationState() == daidalosengine::MESH_ANIMATION_STATE_STOPPED))) {
			this->m_state = WEAPON_STATE_NONE;

			// Start the smoke effect
			this->m_smokeEmitter->SetActive(true);
			this->m_smokeRemainingDelay = SMOKE_TIMER;

		} else if(this->m_state == WEAPON_STATE_RELOAD && this->m_remainingDelay <= 0 && (this->m_meshNode == nullptr || (this->m_meshNode != nullptr && this->m_meshNode->GetAnimationState() == daidalosengine::MESH_ANIMATION_STATE_STOPPED))) {
			this->m_state = WEAPON_STATE_NONE;
			
			// Update the magazine size and the remaining ammo
			const unsigned int ammoToShift = std::min(this->m_maxMagazineSize - this->m_magazineSize, this->m_remainingAmmo);
			this->m_magazineSize += ammoToShift;
			this->m_remainingAmmo -= ammoToShift;
		}

		// Update the smoke emitter
		if(this->m_smokeEmitter->IsActive()) {
			this->m_smokeRemainingDelay -= time;
			if(this->m_smokeRemainingDelay <= 0) {
				this->m_smokeRemainingDelay = 0;
				this->m_smokeEmitter->SetActive(false);
			}
		}

		// Update the live projectiles
		for(auto it = this->m_activeProjectiles.begin(); it != this->m_activeProjectiles.end(); ) {
			SProjectile & projectile = this->m_projectiles.GetReferenceAt(*it);

			bool hit = false;
			daidalosengine::CVector3F collisionPoint;
			daidalosengine::CVector3F movement = projectile.direction * 200.0F * timeSecond;

			// Check for impact on the entities
			for(auto it2 = this->m_parent->GetEntities().begin(); it2 != this->m_parent->GetEntities().end() && !hit; ++it2) {
				if((*it2)->IsEnabled() && (*it2)->GetCollisionState() && (*it2)->GetCollisionShape() != nullptr) {

					// If the projectile movement is greater or equal than the projectile/entity distance
					if(((*it2)->GetVisiblePosition() - projectile.position).Length() <= movement.Length()) {
						for(int i = 1; i < (static_cast<int>(movement.Length()) + 1) * 4 && !hit; i++) {
							const daidalosengine::CVector3F movementStep = (movement * static_cast<float>(i)) / (movement.Length() * 4.0F);

							// Collision check
							if((*it2)->GetCollisionShape()->IsCollision(projectile.position + movementStep)) {

								// Take damage and generate entity specific particles
								(*it2)->TakeDamage(projectile.damage);
								(*it2)->GenerateImpactParticles(projectile.position + movementStep - (projectile.direction * PARTICLE_DIRECTION_DECAL));
								hit = true;

								// Kill the projectile
								this->m_projectiles.Delete(*it);
								it = this->m_activeProjectiles.erase(it);
							}
						}
					}
				}
			}

			// If an impact is detected on the static part
			if(!hit && this->m_parent->GetScene()->GetCollisionPoint(projectile.position, movement, collisionPoint)) {
				hit = true;
				// Generate a default impact particle
				bool freeParticleEmitterFound = false;
				for(unsigned int index = 0; index < this->m_bulletImpactParticle.size() && !freeParticleEmitterFound; index++) {
					if(!this->m_bulletImpactParticle[index]->IsActive()) {
						freeParticleEmitterFound = true;
						this->m_bulletImpactParticle[index]->SetLife(0);
						this->m_bulletImpactParticle[index]->SetLifeMax(PARTICLE_LIFE);
						this->m_bulletImpactParticle[index]->SetActive(true);
						this->m_bulletImpactParticle[index]->SetPosition(collisionPoint - (projectile.direction * PARTICLE_DIRECTION_DECAL));
					}
				}

				// Kill the projectile
				this->m_projectiles.Delete(*it);
				it = this->m_activeProjectiles.erase(it);

			}

			if(!hit) {
				// The projectile is moving
				projectile.position += movement;
				projectile.distance -= movement.Length();

				if(projectile.distance <= 0) {
					// Kill the projectile
					this->m_projectiles.Delete(*it);
					it = this->m_activeProjectiles.erase(it);
				} else {
					++it;
				}
			}
		}
	}

	/**
	* Get the magazine size
	* @return The magazine size
	*/
	unsigned int IWeapon::GetMagazineSize() const {
		return this->m_magazineSize;
	}

	/**
	* Get the remaining ammo
	* @return The remaining ammo
	*/
	unsigned int IWeapon::GetRemainingAmmo() const {
		return this->m_remainingAmmo;
	}

	/**
	* Initialize the projectile buffer
	* @param bufferSize The buffer size
	*/
	void IWeapon::InitializeProjectileBuffer(const int bufferSize) {
		this->m_projectiles.Allocate(bufferSize);
	}

	/**
	* Set the magazine size related stats
	* @param initialMagazineSize The initial magazine size
	* @param maxMagazineSize The max magazine size
	*/
	void IWeapon::SetMagazineStats(const unsigned int initialMagazineSize, const unsigned int maxMagazineSize) {
		this->m_magazineSize = initialMagazineSize;
		this->m_maxMagazineSize = maxMagazineSize;
	}

	/**
	* Set the remaining ammo related stats
	* @param remainingAmmo The remaining ammo
	* @param maxAmmo The max ammo
	*/
	void IWeapon::SetAmmoStats(const unsigned int remainingAmmo, const unsigned int maxAmmo) {
		this->m_remainingAmmo = remainingAmmo;
		this->m_maxAmmo = maxAmmo;
	}

	/**
	* Set the offensive stats
	* @param damage The damage
	* @param hitCount The number of impacts
	* @param precision The precision in percentage
	* @param range The range
	*/
	void IWeapon::SetOffensiveStats(const unsigned int damage, const unsigned int hitCount, const unsigned int precision, const unsigned int range) {
		this->m_damage = damage;
		this->m_hitCount = hitCount;
		this->m_precision = precision;
		this->m_range = static_cast<float>(range);
	}

	/**
	* Set the fire stats
	* @param animationRate The animation rate
	* @param animationDelay The delay between two animations
	*/
	void IWeapon::SetFireStats(const float animationRate, const float animationDelay) {
		this->m_fireAnimationRate = animationRate;
		this->m_fireDelay = animationDelay;
	}

	/**
	* Set the reload stats
	* @param animationRate The animation rate
	* @param animationDelay The delay between two animations
	*/
	void IWeapon::SetReloadStats(const float animationRate, const float animationDelay) {
		this->m_reloadAnimationRate = animationRate;
		this->m_reloadDelay = animationDelay;
	}

	/**
	* Set the fire sound
	* @param soundFile The sound file
	*/
	void IWeapon::SetFireSound(const std::string& soundFile) {
		this->m_fireSound.LoadFromFile(soundFile, daidalosengine::SOUND_TYPE_STATIC, daidalosengine::SOUND_CATEGORY_ENVIRONMENT);
		this->m_fireSound.SetRelativeToListener(true);
	}

	/**
	* Set the fire sound
	* @param soundFile The sound file
	*/
	void IWeapon::SetReloadSound(const std::string& soundFile) {
		this->m_reloadSound.LoadFromFile(soundFile, daidalosengine::SOUND_TYPE_STATIC, daidalosengine::SOUND_CATEGORY_ENVIRONMENT);
		this->m_reloadSound.SetRelativeToListener(true);
	}

	/**
	* Set the fire animation
	* @param firstFrame The first frame
	* @param lastFrame The last frame
	*/
	void IWeapon::SetFireAnimation(const float firstFrame, const float lastFrame) {
		this->m_fireAnimation.first = firstFrame;
		this->m_fireAnimation.second = lastFrame;
	}

	/**
	* Set the reload animation
	* @param firstFrame The first frame
	* @param lastFrame The last frame
	*/
	void IWeapon::SetReloadAnimation(const float firstFrame, const float lastFrame) {
		this->m_reloadAnimation.first = firstFrame;
		this->m_reloadAnimation.second = lastFrame;
	}

	/**
	* Set the mesh node
	* @param meshNode The mesh node
	*/
	void IWeapon::SetMeshNode(daidalosengine::CAnimatedMeshSceneNode * meshNode) {
		this->m_meshNode = meshNode;
	}

	/**
	* Set the fire light
	* @param fireLight The fire light
	*/
	void IWeapon::SetFireLight(daidalosengine::CSpotLightSceneNode * fireLight) {

		// Remove previous declared light
		if (this->m_fireLight != nullptr) {
			GetMeshNode()->RemoveChild(this->m_fireLight);
		}

		// Add the new one
		this->m_fireLight = fireLight;
		this->m_fireLight->SetSaved(false);
		GetMeshNode()->AddChild(this->m_fireLight);
	}

	/**
	* Set the smoke emitter
	* @param emitter The emitter
	*/
	void IWeapon::SetSmokeEmitter(daidalosengine::CParticleEmitterSceneNode * emitter) {

		// Remove previous declared emitter
		if (this->m_smokeEmitter != nullptr) {
			GetMeshNode()->RemoveChild(this->m_smokeEmitter);
		}

		// Add the emitter
		this->m_smokeEmitter = emitter;
		this->m_smokeEmitter->SetActive(false);
		GetMeshNode()->AddChild(this->m_smokeEmitter);
	}

	/**
	* Set the fire emitter
	* @param emitter The emitter
	*/
	void IWeapon::SetFlashEmitter(daidalosengine::CParticleEmitterSceneNode * emitter) {

		// Remove previous declared emitter
		if (this->m_flashEmitter != nullptr) {
			GetMeshNode()->RemoveChild(this->m_flashEmitter);
		}

		// Add the emitter
		this->m_flashEmitter = emitter;
		this->m_flashEmitter->SetActive(false);
		GetMeshNode()->AddChild(this->m_flashEmitter);
	}

	/**
	* Add a particle emitter
	* @param emitter The particle emitter
	*/
	void IWeapon::AddParticleEmitter(daidalosengine::CParticleEmitter * emitter) {
		this->m_bulletImpactParticle.push_back(emitter);
	}

	/**
	* Get the parent
	* @return The parent
	*/
	daidalosgameengine::ILevel * IWeapon::GetParent() const {
		return this->m_parent;
	}

	/**
	* Get the mesh node
	* @return The mesh node
	*/
	daidalosengine::CAnimatedMeshSceneNode * IWeapon::GetMeshNode() const {
		return this->m_meshNode;
	}
}
