/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#define FPS_CAMERA_MOVEMENT_SPEED 10.0F
#define FPS_CAMERA_ROTATION_SPEED 90.0F
#define FPS_CAMERA_ROTATION_SPEED_GAMEPAD_FACTOR 0.28F
#define FPS_CAMERA_ROTATION_SPEED_GAMEPAD_VERTICAL_FACTOR 0.23F
#define FPS_CAMERA_GRAVITY (-20.0F)
#define FPS_CAMERA_JUMP_IMPULSION 10.0F
#define FPS_CAMERA_JUMP_DELAY 350
#define FPS_CAMERA_COLLISION_UPDATE 16
#define FPS_CAMERA_WALK_FACTOR 1.0F
#define FPS_CAMERA_RUN_FACTOR 1.7F
#define FPS_CAMERA_CROUCH_FACTOR 0.6F
#define FPS_CAMERA_SHAPE_HORIZONTAL_RADIUS 1.0F
#define FPS_CAMERA_SHAPE_VERTICAL_RADIUS_STAND 2.7F
#define FPS_CAMERA_SHAPE_VERTICAL_RADIUS_CROUCH 1.0F
#define FPS_CAMERA_SHAPE_VERTICAL_RADIUS_DEAD 0.5F
#define FPS_CAMERA_STEP_DISTANCE 7.0F
#define FPS_CAMERA_FALL_STEP_MIN_GRAVITY (-9.0F)
#define FPS_CAMERA_LADDER_DISTANCE 3.0F

#define FIRST_WEAPON PLAYER_OBJECT_HANDGUN_WEAPON
#define LAST_WEAPON PLAYER_OBJECT_SHOTGUN_WEAPON

#include <limits>
#include <Core/CInputDevice.hpp>
#include <Core/CSoundContextManager.hpp>
#include <Core/CConfigurationManager.hpp>
#include <Level/ILevel.hpp>
#include "Enums.hpp"
#include "CFPSCamera.hpp"
#include "Weapons/CHandgun.hpp"

namespace daidalos {
	CFPSCamera::CFPSCamera(daidalosgameengine::ILevel * parent, daidalosengine::ICameraSceneNode * cameraNode, daidalosengine::ISceneNode * flashlightNode) : m_node(cameraNode), m_flashlightNode(flashlightNode), IPlayer(parent){
		this->m_hasMoved = false;
		this->m_actionState = 0;
		this->m_state = FPS_CAMERA_STATE_STAND;
		CFPSCamera::SetHealth(80);
		this->m_armor = 0;
		this->m_stepDistance = 0;
		this->m_ladderDistance = 0;
		this->m_jumpTimer = 0;
		this->m_currentWeapon = nullptr;

		this->m_inputDevice = daidalosengine::CInputDevice::GetInstance();
		this->m_soundContext = daidalosengine::CSoundContextManager::GetInstance();
		this->m_options = daidalosengine::CConfigurationManager::GetInstance()->GetConfiguration(APPLICATION_OPTIONS);

		daidalosengine::SInputActionDeclaration decl[] = {
			{ FPS_CAMERA_EXIT, -1 },
			{ FPS_CAMERA_STRAFELEFT, FPS_CAMERA_STRAFERIGHT },
			{ FPS_CAMERA_STRAFERIGHT, FPS_CAMERA_STRAFELEFT },
			{ FPS_CAMERA_FORWARD, FPS_CAMERA_BACKWARD },
			{ FPS_CAMERA_BACKWARD, FPS_CAMERA_FORWARD },
			{ FPS_CAMERA_JUMP, -1 },
			{ FPS_CAMERA_LOOKUP, FPS_CAMERA_LOOKDOWN },
			{ FPS_CAMERA_LOOKDOWN, FPS_CAMERA_LOOKUP },
			{ FPS_CAMERA_LOOKLEFT, FPS_CAMERA_LOOKRIGHT },
			{ FPS_CAMERA_LOOKRIGHT, FPS_CAMERA_LOOKLEFT },
			{ FPS_CAMERA_RUN, -1 },
			{ FPS_CAMERA_INTERACT, -1 },
			{ FPS_CAMERA_FIRE , -1 },
			{ FPS_CAMERA_RELOAD, -1 },
			{ FPS_CAMERA_NEXT_WEAPON, -1 },
			{ FPS_CAMERA_PREVIOUS_WEAPON, -1 },
			{ FPS_CAMERA_CROUCH, -1 }
		};

		this->m_inputDevice->AddActions(decl);

		// Keyboard mapping
		this->m_inputDevice->SetActionKey(FPS_CAMERA_EXIT, daidalosengine::INPUT_KEY_KEYBOARD_ESCAPE);
		this->m_inputDevice->SetActionKey(FPS_CAMERA_STRAFELEFT, daidalosengine::INPUT_KEY_KEYBOARD_Q);
		this->m_inputDevice->SetActionKey(FPS_CAMERA_STRAFERIGHT, daidalosengine::INPUT_KEY_KEYBOARD_D);
		this->m_inputDevice->SetActionKey(FPS_CAMERA_FORWARD, daidalosengine::INPUT_KEY_KEYBOARD_Z);
		this->m_inputDevice->SetActionKey(FPS_CAMERA_BACKWARD, daidalosengine::INPUT_KEY_KEYBOARD_S);
		this->m_inputDevice->SetActionKey(FPS_CAMERA_JUMP, daidalosengine::INPUT_KEY_KEYBOARD_SPACE);
		this->m_inputDevice->SetActionKey(FPS_CAMERA_LOOKUP, daidalosengine::INPUT_KEY_KEYBOARD_UP);
		this->m_inputDevice->SetActionKey(FPS_CAMERA_LOOKDOWN, daidalosengine::INPUT_KEY_KEYBOARD_DOWN);
		this->m_inputDevice->SetActionKey(FPS_CAMERA_LOOKLEFT, daidalosengine::INPUT_KEY_KEYBOARD_LEFT);
		this->m_inputDevice->SetActionKey(FPS_CAMERA_LOOKRIGHT, daidalosengine::INPUT_KEY_KEYBOARD_RIGHT);
		this->m_inputDevice->SetActionKey(FPS_CAMERA_RUN, daidalosengine::INPUT_KEY_KEYBOARD_LSHIFT);
		this->m_inputDevice->SetActionKey(FPS_CAMERA_INTERACT, daidalosengine::INPUT_KEY_KEYBOARD_E);
		this->m_inputDevice->SetActionKey(FPS_CAMERA_RELOAD, daidalosengine::INPUT_KEY_KEYBOARD_R);	
		this->m_inputDevice->SetActionKey(FPS_CAMERA_CROUCH, daidalosengine::INPUT_KEY_KEYBOARD_C);

		// Mouse mapping
		this->m_inputDevice->SetActionKey(FPS_CAMERA_FIRE, daidalosengine::INPUT_KEY_MOUSE_BUTTON0);
		this->m_inputDevice->SetActionKey(FPS_CAMERA_NEXT_WEAPON, daidalosengine::INPUT_KEY_MOUSE_WHEELUP);
		this->m_inputDevice->SetActionKey(FPS_CAMERA_PREVIOUS_WEAPON, daidalosengine::INPUT_KEY_MOUSE_WHEELDOWN);

		// Gamepad mapping
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_EXIT, 0, daidalosengine::INPUT_KEY_GAMEPAD_BUTTON6);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_FORWARD, 0, daidalosengine::INPUT_KEY_GAMEPAD_AXIS_LYN);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_BACKWARD, 0, daidalosengine::INPUT_KEY_GAMEPAD_AXIS_LYP);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_STRAFELEFT, 0, daidalosengine::INPUT_KEY_GAMEPAD_AXIS_LXN);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_STRAFERIGHT, 0, daidalosengine::INPUT_KEY_GAMEPAD_AXIS_LXP);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_LOOKUP, 0, daidalosengine::INPUT_KEY_GAMEPAD_AXIS_RYN);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_LOOKDOWN, 0, daidalosengine::INPUT_KEY_GAMEPAD_AXIS_RYP);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_LOOKLEFT, 0, daidalosengine::INPUT_KEY_GAMEPAD_AXIS_RXN);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_LOOKRIGHT, 0, daidalosengine::INPUT_KEY_GAMEPAD_AXIS_RXP);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_JUMP, 0, daidalosengine::INPUT_KEY_GAMEPAD_BUTTON0);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_RUN, 0, daidalosengine::INPUT_KEY_GAMEPAD_BUTTON4);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_INTERACT, 0, daidalosengine::INPUT_KEY_GAMEPAD_BUTTON2);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_RELOAD, 0, daidalosengine::INPUT_KEY_GAMEPAD_BUTTON3);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_FIRE, 0, daidalosengine::INPUT_KEY_GAMEPAD_AXIS_LZN);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_NEXT_WEAPON, 0, daidalosengine::INPUT_KEY_GAMEPAD_POV_RIGHT);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_PREVIOUS_WEAPON, 0, daidalosengine::INPUT_KEY_GAMEPAD_POV_LEFT);
		this->m_inputDevice->SetGamepadActionKey(FPS_CAMERA_CROUCH, 0, daidalosengine::INPUT_KEY_GAMEPAD_BUTTON1);

		this->SetCollisionShape(new daidalosengine::CEllipseF(this->m_node->GetPosition() - daidalosengine::CVector3F(0, 1, 0), daidalosengine::CVector2F(FPS_CAMERA_SHAPE_HORIZONTAL_RADIUS, FPS_CAMERA_SHAPE_VERTICAL_RADIUS_STAND)));
		this->m_rotation = this->m_node->GetRotation();
		SetVisiblePosition(this->m_node->GetPosition());

		this->m_weaponChangeSound.LoadFromFile("change_weapon.wav", daidalosengine::SOUND_TYPE_STATIC, daidalosengine::SOUND_CATEGORY_ENVIRONMENT);
		this->m_weaponChangeSound.SetRelativeToListener(true);

		// Initialize the step sounds
		for(int i = 1; i <= 4; ++i) {
			this->m_stepSounds.push_back(std::make_unique<daidalosengine::CSound>());
			this->m_stepSounds[i - 1]->LoadFromFile("footstep" + std::to_string(i) + ".wav", daidalosengine::SOUND_TYPE_STATIC, daidalosengine::SOUND_CATEGORY_ENVIRONMENT);
			this->m_stepSounds[i - 1]->SetRelativeToListener(true);
		}

		// Initialize the fall sound
		this->m_stepFallSound.LoadFromFile("footstep_fall.wav", daidalosengine::SOUND_TYPE_STATIC, daidalosengine::SOUND_CATEGORY_ENVIRONMENT);
		this->m_stepFallSound.SetRelativeToListener(true);

		// Initialize the ladder
		this->m_ladderSound.LoadFromFile("ladder.wav", daidalosengine::SOUND_TYPE_STATIC, daidalosengine::SOUND_CATEGORY_ENVIRONMENT);
		this->m_ladderSound.SetRelativeToListener(true);

		// Initialize the jump sound
		this->m_jumpSound.LoadFromFile("jump.wav", daidalosengine::SOUND_TYPE_STATIC, daidalosengine::SOUND_CATEGORY_ENVIRONMENT);
		this->m_jumpSound.SetRelativeToListener(true);

		// Initialize the hurt sound
		this->m_hurtSound.LoadFromFile("hurt.wav", daidalosengine::SOUND_TYPE_STATIC, daidalosengine::SOUND_CATEGORY_ENVIRONMENT);
		this->m_hurtSound.SetRelativeToListener(true);

		// Initialize the die sound
		this->m_dieSound.LoadFromFile("die.wav", daidalosengine::SOUND_TYPE_STATIC, daidalosengine::SOUND_CATEGORY_ENVIRONMENT);
		this->m_dieSound.SetRelativeToListener(true);

		// Initialize the weapons
		this->m_weapons[PLAYER_OBJECT_HANDGUN_WEAPON] = std::make_unique<CHandgun>(GetParent(), this->m_node);
		this->m_currentWeaponIndex = PLAYER_OBJECT_HANDGUN_WEAPON;
		this->m_currentWeapon = this->m_weapons[PLAYER_OBJECT_HANDGUN_WEAPON].get();
		this->m_weaponsOrder.push_back(PLAYER_OBJECT_HANDGUN_WEAPON);
	}

	/**
	* Destructor
	*/
	CFPSCamera::~CFPSCamera() = default;

	/**
	* Damage the entity
	* @param damage The damage
	*/
	void CFPSCamera::TakeDamage(const unsigned int damage) {
		IPlayer::TakeDamage(damage);

		if(GetHealth() > 0) {
			this->m_hurtSound.SetStatus(daidalosengine::SOUND_STATUS_PLAY);
		} else {
			auto* collisionShapeEllipse = dynamic_cast<daidalosengine::CEllipseF *>(this->GetCollisionShape());
			collisionShapeEllipse->SetRadius(daidalosengine::CVector2F(FPS_CAMERA_SHAPE_HORIZONTAL_RADIUS, FPS_CAMERA_SHAPE_VERTICAL_RADIUS_DEAD));

			this->m_dieSound.SetStatus(daidalosengine::SOUND_STATUS_PLAY);
			this->m_node->SetEnabled(false);
		}
	}

	/**
	* Set the player active state
	* @param enabled The player active state
	*/
	void CFPSCamera::SetEnabled(const bool enabled) {
		this->m_node->SetEnabled(enabled);
	}

	/**
	* Set the player armor
	* @param armor The player armor
	*/
	void CFPSCamera::SetArmor(const int armor) {
		this->m_armor = armor;
	}

	/**
	* Set a player object
	* @param object The player object id
	* @param value The object value
	*/
	void CFPSCamera::SetObject(unsigned int object, const unsigned int value) {

		switch(object) {
		case PLAYER_OBJECT_FLASHLIGHT:
			if(this->m_flashlightNode != nullptr) {
				this->m_flashlightNode->SetEnabled(value >= 1);
			}
			break;

		case PLAYER_OBJECT_HANDGUN_AMMO:
			this->m_weapons[PLAYER_OBJECT_HANDGUN_WEAPON]->AddAmmo(value);
			break;

		case PLAYER_OBJECT_SHOTGUN_AMMO:
			this->m_weapons[PLAYER_OBJECT_SHOTGUN_WEAPON]->AddAmmo(value);
			break;

		default:
			// Set a misc object
			this->m_objects[object] = value;
			break;
		}
	}

	/**
	* Set the player position
	* @param position The position
	*/
	void CFPSCamera::SetPosition(const daidalosengine::CVector3F position) {
		SetVisiblePosition(position);
	}

	void CFPSCamera::Update(const float time) {
		const float timeSecond = time / 1000.0F;

		this->m_actionState = this->m_actionState & FPS_CAMERA_JUMP;
		if(IsEnabled()) {
			if (this->m_currentWeapon != nullptr) {
				this->m_currentWeapon->Update(time);
			}
			
			UpdateState();
			UpdateView(timeSecond);
			UpdateAction();
		}
		UpdateMovement(timeSecond, time);
		
		// Update the node
		this->m_node->SetPosition(GetVisiblePosition() + daidalosengine::CVector3F(0, 1, 0));
		this->m_node->SetRotation(this->m_rotation);

		// Update the listener
		if (IsEnabled()) {
			this->m_soundContext->SetListenerPosition(GetVisiblePosition());
			float orientation[] = { this->m_node->GetForwardVector().x, this->m_node->GetForwardVector().y, this->m_node->GetForwardVector().z, this->m_node->GetUpVector().x, this->m_node->GetUpVector().y, this->m_node->GetUpVector().z };
			this->m_soundContext->SetListenerOrientation(orientation);
		}
		
		// Reset the gravity state
		this->SetGravityState(true);
	}

	/**
	* Check if the player can see a shape
	* @param shape A shape
	* @return True if the player can see the shape
	*/
	bool CFPSCamera::IsShapeVisible(daidalosengine::IShape3DF * shape) {
		return !this->m_node->FrustumCull(shape);
	}

	/**
	* If the player interaction action is active
	* @return True if the player interaction action is active
	*/
	bool CFPSCamera::IsInteracting() const {
		return this->m_inputDevice->IsActionActive(FPS_CAMERA_INTERACT) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_INTERACT);
	}

	/**
	* If the player has moved during the current frame
	* return True if the player has moved during the current frame
	*/
	bool CFPSCamera::HasMoved() {
		return this->m_hasMoved;
	}

	/**
	* Check if the entity is enabled
	* @return True if the entity is enabled
	*/
	bool CFPSCamera::IsEnabled() const {
		return GetHealth() > 0 && this->m_node->IsEnabled();
	}

	/**
	* Get the player health
	* @return The player health
	*/
	int CFPSCamera::GetArmor() const {
		return this->m_armor;
	}

	/**
	* Get the player magazine ammo for the current weapon
	* @return The current magazine ammo
	*/
	unsigned int CFPSCamera::GetCurrentWeaponMagazineAmmo() const {
		return this->m_currentWeapon != nullptr ? this->m_currentWeapon->GetMagazineSize() : 0;
	}

	/**
	* Get the player stock ammo for the current weapon
	* @return The current stock ammo
	*/
	unsigned int CFPSCamera::GetCurrentWeaponStockAmmo() const {
		return this->m_currentWeapon != nullptr ? this->m_currentWeapon->GetRemainingAmmo() : 0;
	}

	/**
	* Get the player's object value
	* @param object The object id
	* @return The object value
	*/
	unsigned int CFPSCamera::GetObjectValue(unsigned int object) const {

		switch(object) {
		case PLAYER_OBJECT_FLASHLIGHT:
			return (this->m_flashlightNode != nullptr && this->m_flashlightNode->IsEnabled()) ? 1 : 0;

		case PLAYER_OBJECT_HANDGUN_WEAPON:
		case PLAYER_OBJECT_SHOTGUN_WEAPON:
			return this->m_weapons.find(object) != this->m_weapons.end();

		default:
			// Check for a misc object
			const auto it = this->m_objects.find(object);
			if(it != this->m_objects.end()) {
				return (*it).second;
			}
			return 0;
		}
	}

	/**
	* Get the camera node
	* @return The camera node
	*/
	daidalosengine::ICameraSceneNode * CFPSCamera::GetCameraNode() const {
		return this->m_node;
	}

	/**
	* Callback used to handle event when the entity fall and touch the ground
	* @param gravity The gravity value before touching the ground
	*/
	void CFPSCamera::OnEntityTouchedGround(const daidalosengine::CVector3F gravity) {

		// Add a delay after a jump
		if((this->m_actionState & FPS_CAMERA_JUMP) != 0) {
			this->m_jumpTimer = FPS_CAMERA_JUMP_DELAY;
		}
		this->m_actionState &= ~FPS_CAMERA_JUMP;

		// Fall sound at a minimal hight
		if(gravity.y <= FPS_CAMERA_FALL_STEP_MIN_GRAVITY) {
			this->m_stepFallSound.SetStatus(daidalosengine::SOUND_STATUS_PLAY);
		}
	}

	/**
	* Update the state
	*/
	void CFPSCamera::UpdateState() {

		if(this->m_inputDevice->IsActionActive(FPS_CAMERA_STRAFELEFT) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_STRAFELEFT)) {
			this->m_actionState |= FPS_CAMERA_STRAFELEFT;
		}

		if(this->m_inputDevice->IsActionActive(FPS_CAMERA_STRAFERIGHT) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_STRAFERIGHT)) {
			this->m_actionState |= FPS_CAMERA_STRAFERIGHT;
		}

		if(this->m_inputDevice->IsActionActive(FPS_CAMERA_FORWARD) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_FORWARD)) {
			this->m_actionState |= FPS_CAMERA_FORWARD;
		}

		if(this->m_inputDevice->IsActionActive(FPS_CAMERA_BACKWARD) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_BACKWARD)) {
			this->m_actionState |= FPS_CAMERA_BACKWARD;
		}

		if(this->m_inputDevice->IsActionActive(FPS_CAMERA_LOOKUP) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_LOOKUP)) {
			this->m_actionState |= FPS_CAMERA_LOOKUP;
		}

		if(this->m_inputDevice->IsActionActive(FPS_CAMERA_LOOKDOWN) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_LOOKDOWN)) {
			this->m_actionState |= FPS_CAMERA_LOOKDOWN;
		}

		if(this->m_inputDevice->IsActionActive(FPS_CAMERA_LOOKLEFT) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_LOOKLEFT)) {
			this->m_actionState |= FPS_CAMERA_LOOKLEFT;
		}

		if(this->m_inputDevice->IsActionActive(FPS_CAMERA_LOOKRIGHT) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_LOOKRIGHT)) {
			this->m_actionState |= FPS_CAMERA_LOOKRIGHT;
		}

		if(this->m_jumpTimer <= 0 && this->GetGravityState() && (this->m_state == FPS_CAMERA_STATE_STAND || this->m_state == FPS_CAMERA_STATE_RUN) && (this->m_inputDevice->IsActionActive(FPS_CAMERA_JUMP) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_JUMP)) && !(this->m_actionState & FPS_CAMERA_JUMP)) {
			this->m_actionState |= FPS_CAMERA_JUMP;
			this->SetGravity(daidalosengine::CVector3F(this->GetGravity().x, FPS_CAMERA_JUMP_IMPULSION, this->GetGravity().z));
			this->m_inputDevice->FreezeAction(FPS_CAMERA_JUMP);
			this->m_inputDevice->FreezeGamepadAction(0, FPS_CAMERA_JUMP);
			this->m_jumpSound.SetStatus(daidalosengine::SOUND_STATUS_PLAY);
		}

		if(this->GetGravityState() && (this->m_state == FPS_CAMERA_STATE_STAND || this->m_state == FPS_CAMERA_STATE_RUN) && (this->m_inputDevice->IsActionActive(FPS_CAMERA_RUN) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_RUN))) {
			this->m_actionState |= FPS_CAMERA_RUN;
			this->m_state = FPS_CAMERA_STATE_RUN;
		} else if(this->m_state == FPS_CAMERA_STATE_RUN) {
			this->m_state = FPS_CAMERA_STATE_STAND;
		}

		if(this->m_inputDevice->IsActionActive(FPS_CAMERA_FIRE) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_FIRE)) {
			this->m_actionState |= FPS_CAMERA_FIRE;
		}

		if(this->m_inputDevice->IsActionActive(FPS_CAMERA_RELOAD) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_RELOAD)) {
			this->m_actionState |= FPS_CAMERA_RELOAD;
			this->m_inputDevice->FreezeAction(FPS_CAMERA_RELOAD);
			this->m_inputDevice->FreezeGamepadAction(0, FPS_CAMERA_RELOAD);
		}

		if(this->m_inputDevice->IsActionActive(FPS_CAMERA_NEXT_WEAPON) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_NEXT_WEAPON)) {
			this->m_actionState |= FPS_CAMERA_NEXT_WEAPON;
			this->m_inputDevice->FreezeAction(FPS_CAMERA_NEXT_WEAPON);
			this->m_inputDevice->FreezeGamepadAction(0, FPS_CAMERA_NEXT_WEAPON);
		}

		if(this->m_inputDevice->IsActionActive(FPS_CAMERA_PREVIOUS_WEAPON) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_PREVIOUS_WEAPON)) {
			this->m_actionState |= FPS_CAMERA_PREVIOUS_WEAPON;
			this->m_inputDevice->FreezeAction(FPS_CAMERA_PREVIOUS_WEAPON);
			this->m_inputDevice->FreezeGamepadAction(0, FPS_CAMERA_PREVIOUS_WEAPON);
		}

		if(this->m_inputDevice->IsActionActive(FPS_CAMERA_CROUCH) || this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_CROUCH)) {
			this->m_actionState |= FPS_CAMERA_CROUCH;
			this->m_inputDevice->FreezeAction(FPS_CAMERA_CROUCH);
			this->m_inputDevice->FreezeGamepadAction(0, FPS_CAMERA_CROUCH);
		}
	}

	/**
	* Update the movement
	* @param timeSecond Elapsed time in secondes
	* @param time Elipsed time
	*/
	void CFPSCamera::UpdateMovement(const float timeSecond, const float time) {
		daidalosengine::CVector3F movement;
		auto* collisionShapeEllipse = dynamic_cast<daidalosengine::CEllipseF *>(this->GetCollisionShape());

		this->m_jumpTimer -= time;
		if(this->m_jumpTimer < 0) {
			this->m_jumpTimer = 0;
		}

		if((this->m_actionState & FPS_CAMERA_CROUCH) != 0 && this->m_state == FPS_CAMERA_STATE_STAND && this->GetGravity().y == 0) {
			this->m_state = FPS_CAMERA_STATE_CROUCH;
			collisionShapeEllipse->SetRadius(daidalosengine::CVector2F(FPS_CAMERA_SHAPE_HORIZONTAL_RADIUS, FPS_CAMERA_SHAPE_VERTICAL_RADIUS_CROUCH));

		} else if((this->m_actionState & FPS_CAMERA_CROUCH) != 0 && this->m_state == FPS_CAMERA_STATE_CROUCH && this->GetGravity().y == 0) {

			// Check if the player can stand up
			const daidalosengine::CVector3F movementUp = daidalosengine::CVector3F(0, (FPS_CAMERA_SHAPE_VERTICAL_RADIUS_STAND - FPS_CAMERA_SHAPE_VERTICAL_RADIUS_CROUCH) * 2, 0);
			daidalosengine::CVector3F collisionPoint;

			// If no up collision detected at the standing position
			if(!GetParent()->GetScene()->GetCollisionPoint(*collisionShapeEllipse, movementUp, collisionPoint)) {
				this->m_state = FPS_CAMERA_STATE_STAND;
				collisionShapeEllipse->SetRadius(daidalosengine::CVector2F(FPS_CAMERA_SHAPE_HORIZONTAL_RADIUS, FPS_CAMERA_SHAPE_VERTICAL_RADIUS_STAND));
				collisionShapeEllipse->SetPosition(collisionShapeEllipse->GetPosition() + movementUp);
			}

		} else {
			// Strafe left
			if((this->m_actionState & FPS_CAMERA_STRAFELEFT) != 0) {
				float factor = 1;

				if(this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_STRAFELEFT)) {
					factor = this->m_inputDevice->GetGamepadActionFactor(0, FPS_CAMERA_STRAFELEFT);
				}

				movement.x += -sin((this->m_rotation.y + 90.0F) * PI_OVER_180) * factor;
				movement.z += -cos((this->m_rotation.y + 90.0F) * PI_OVER_180) * factor;
			}

			// Strafe right
			if((this->m_actionState & FPS_CAMERA_STRAFERIGHT) != 0) {
				float factor = 1;

				if(this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_STRAFERIGHT)) {
					factor = this->m_inputDevice->GetGamepadActionFactor(0, FPS_CAMERA_STRAFERIGHT);
				}

				movement.x += -sin((this->m_rotation.y - 90.0F) * PI_OVER_180) * factor;
				movement.z += -cos((this->m_rotation.y - 90.0F) * PI_OVER_180) * factor;
			}

			// Go forward
			if((this->m_actionState & FPS_CAMERA_FORWARD) != 0) {
				float factor = 1;

				if(this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_FORWARD)) {
					factor = this->m_inputDevice->GetGamepadActionFactor(0, FPS_CAMERA_FORWARD);
				}

				movement.x += sin(this->m_rotation.y * PI_OVER_180) * factor;
				movement.z += cos(this->m_rotation.y * PI_OVER_180) * factor;

				// If no gravity, add the vertical part
				if(!this->GetGravityState()) {
					this->SetGravity(daidalosengine::CVector3F(this->GetGravity().x, -sin(this->m_rotation.x * PI_OVER_180) * factor, this->GetGravity().z));
				}
			}

			// Go backward
			if((this->m_actionState & FPS_CAMERA_BACKWARD) != 0) {
				float factor = 1;

				if(this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_BACKWARD)) {
					factor = this->m_inputDevice->GetGamepadActionFactor(0, FPS_CAMERA_BACKWARD);
				}

				movement.x += -sin(this->m_rotation.y * PI_OVER_180) * factor;
				movement.z += -cos(this->m_rotation.y * PI_OVER_180) * factor;

				// If no gravity, add the vertical part
				if(!this->GetGravityState()) {
					this->SetGravity(daidalosengine::CVector3F(this->GetGravity().x, sin(this->m_rotation.x * PI_OVER_180) * factor, this->GetGravity().z));
				}
			}
		}

		if(this->GetGravityState()) {
			this->SetGravity(daidalosengine::CVector3F(this->GetGravity().x, this->GetGravity().y + FPS_CAMERA_GRAVITY * timeSecond, this->GetGravity().z));
		} else {
			this->SetGravity(daidalosengine::CVector3F(this->GetGravity().x, this->GetGravity().y * FPS_CAMERA_MOVEMENT_SPEED, this->GetGravity().z));
		}
		
		// Visibility position update based on the state
		float speedFactor;
		switch(this->m_state) {
		case FPS_CAMERA_STATE_STAND:
			speedFactor = FPS_CAMERA_WALK_FACTOR;
			break;

		case FPS_CAMERA_STATE_RUN:
			speedFactor = FPS_CAMERA_RUN_FACTOR;
			break;

		default:
			speedFactor = FPS_CAMERA_CROUCH_FACTOR;
			break;
		}

		// Collision test
		const daidalosengine::CVector3F previousVisiblePosition = GetVisiblePosition();
		PerformLevelCollisions(movement * FPS_CAMERA_MOVEMENT_SPEED * speedFactor, time);

		// Step sounds
		if (this->GetGravityState()) {
			if (this->GetGravity().y == 0) {
				this->m_stepDistance += (GetVisiblePosition() - previousVisiblePosition).Length();
				if(this->m_stepDistance >= FPS_CAMERA_STEP_DISTANCE) {
					// Play a random step sound
					this->m_stepSounds[daidalosengine::RandomNumber<int>(0, this->m_stepSounds.size() - 1)]->SetStatus(daidalosengine::SOUND_STATUS_PLAY);
					this->m_stepDistance = 0;
				}
			} else {
				// Reset the step and ladder distance counter
				this->m_stepDistance = 0;
				this->m_ladderDistance = 0;
			}
		} else {
			this->m_ladderDistance += fabsf(GetVisiblePosition().y - previousVisiblePosition.y);
			if (this->m_ladderDistance >= FPS_CAMERA_LADDER_DISTANCE) {
				// Play the ladder sound
				this->m_ladderSound.SetStatus(daidalosengine::SOUND_STATUS_PLAY);
				this->m_ladderDistance = 0;
			}
		}

		this->m_hasMoved = movement.x != 0 || movement.z != 0;
	}

	/**
	* Update the view
	* @param timeSecond Elapsed time
	*/
	void CFPSCamera::UpdateView(const float timeSecond) {
		// Mouse view
		if(this->m_inputDevice->GetMouseMovement().x != 0 || this->m_inputDevice->GetMouseMovement().y != 0) {
			// Update the movement
			this->m_previousMouseMovement.x = static_cast<float>(this->m_inputDevice->GetMouseMovement().x);
			this->m_previousMouseMovement.y = static_cast<float>(this->m_inputDevice->GetMouseMovement().y);

			this->m_rotation.y += this->m_previousMouseMovement.x * FPS_CAMERA_ROTATION_SPEED * timeSecond * this->m_options.GetValueFloat(APPLICATION_OPTION_SECTION_MOUSE, APPLICATION_OPTION_MOUSE_SENSIBILITY, 0.5F);
			this->m_rotation.x += this->m_previousMouseMovement.y * FPS_CAMERA_ROTATION_SPEED * timeSecond * this->m_options.GetValueFloat(APPLICATION_OPTION_SECTION_MOUSE, APPLICATION_OPTION_MOUSE_SENSIBILITY, 0.5F);
		} else {
			// No movement detected this frame, use the smooth factor instead
			this->m_previousMouseMovement *= this->m_options.GetValueFloat(APPLICATION_OPTION_SECTION_MOUSE, APPLICATION_OPTION_MOUSE_SMOOTHING, 0.2F);

			this->m_rotation.y += this->m_previousMouseMovement.x * FPS_CAMERA_ROTATION_SPEED * timeSecond * this->m_options.GetValueFloat(APPLICATION_OPTION_SECTION_MOUSE, APPLICATION_OPTION_MOUSE_SENSIBILITY, 0.5F);
			this->m_rotation.x += this->m_previousMouseMovement.y * FPS_CAMERA_ROTATION_SPEED * timeSecond * this->m_options.GetValueFloat(APPLICATION_OPTION_SECTION_MOUSE, APPLICATION_OPTION_MOUSE_SENSIBILITY, 0.5F);
		}

		// Keyboard view and gamepad

		// Camera up action
		if((this->m_actionState & FPS_CAMERA_LOOKUP) != 0) {
			float factor = 1;

			if(this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_LOOKUP)) {
				factor = FPS_CAMERA_ROTATION_SPEED_GAMEPAD_VERTICAL_FACTOR / (1.3F - (this->m_inputDevice->GetGamepadActionFactor(0, FPS_CAMERA_LOOKUP) * this->m_inputDevice->GetGamepadActionFactor(0, FPS_CAMERA_LOOKUP)));
				this->m_previousGamepadViewMovement.y = -factor;
			}
			this->m_rotation.x -= FPS_CAMERA_ROTATION_SPEED * timeSecond * factor;

		} else if(this->m_previousGamepadViewMovement.y < 0) {
			// Gamepad smooth factor
			this->m_previousGamepadViewMovement.y = this->m_previousGamepadViewMovement.y * this->m_options.GetValueFloat(APPLICATION_OPTION_SECTION_GAMEPAD, APPLICATION_OPTION_GAMEPAD_SMOOTH_FACTOR, 0.0F);
			this->m_rotation.x += FPS_CAMERA_ROTATION_SPEED * timeSecond * this->m_previousGamepadViewMovement.y;
		}

		// Camera down action
		if((this->m_actionState & FPS_CAMERA_LOOKDOWN) != 0) {
			float factor = 1;

			if(this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_LOOKDOWN)) {
				factor = FPS_CAMERA_ROTATION_SPEED_GAMEPAD_VERTICAL_FACTOR / (1.3F - (this->m_inputDevice->GetGamepadActionFactor(0, FPS_CAMERA_LOOKDOWN) * this->m_inputDevice->GetGamepadActionFactor(0, FPS_CAMERA_LOOKDOWN)));
				this->m_previousGamepadViewMovement.y = factor;
			}
			this->m_rotation.x += FPS_CAMERA_ROTATION_SPEED * timeSecond * factor;

		} else if(this->m_previousGamepadViewMovement.y > 0) {
			// Gamepad smooth factor
			this->m_previousGamepadViewMovement.y = this->m_previousGamepadViewMovement.y * this->m_options.GetValueFloat(APPLICATION_OPTION_SECTION_GAMEPAD, APPLICATION_OPTION_GAMEPAD_SMOOTH_FACTOR, 0.0F);
			this->m_rotation.x += FPS_CAMERA_ROTATION_SPEED * timeSecond * this->m_previousGamepadViewMovement.y;
		}

		// Camera left action
		if((this->m_actionState & FPS_CAMERA_LOOKLEFT) != 0) {
			float factor = 1;

			if(this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_LOOKLEFT)) {
				factor = FPS_CAMERA_ROTATION_SPEED_GAMEPAD_FACTOR / (1.3F - (this->m_inputDevice->GetGamepadActionFactor(0, FPS_CAMERA_LOOKLEFT) * this->m_inputDevice->GetGamepadActionFactor(0, FPS_CAMERA_LOOKLEFT)));
				this->m_previousGamepadViewMovement.x = -factor;
			}
			this->m_rotation.y -= FPS_CAMERA_ROTATION_SPEED * timeSecond * factor;

		} else if(this->m_previousGamepadViewMovement.x < 0) {
			// Gamepad smooth factor
			this->m_previousGamepadViewMovement.x = this->m_previousGamepadViewMovement.x * this->m_options.GetValueFloat(APPLICATION_OPTION_SECTION_GAMEPAD, APPLICATION_OPTION_GAMEPAD_SMOOTH_FACTOR, 0.0F);
			this->m_rotation.y += FPS_CAMERA_ROTATION_SPEED * timeSecond * this->m_previousGamepadViewMovement.x;
		}

		// Camera right action
		if((this->m_actionState & FPS_CAMERA_LOOKRIGHT) != 0) {
			float factor = 1;

			if(this->m_inputDevice->IsGamepadActionActive(0, FPS_CAMERA_LOOKRIGHT)) {
				factor = FPS_CAMERA_ROTATION_SPEED_GAMEPAD_FACTOR / (1.3F - (this->m_inputDevice->GetGamepadActionFactor(0, FPS_CAMERA_LOOKRIGHT) * this->m_inputDevice->GetGamepadActionFactor(0, FPS_CAMERA_LOOKRIGHT)));
				this->m_previousGamepadViewMovement.x = factor;
			}
			this->m_rotation.y += FPS_CAMERA_ROTATION_SPEED * timeSecond * factor;

		} else if(this->m_previousGamepadViewMovement.x > 0) {
			// Gamepad smooth factor
			this->m_previousGamepadViewMovement.x = this->m_previousGamepadViewMovement.x * this->m_options.GetValueFloat(APPLICATION_OPTION_SECTION_GAMEPAD, APPLICATION_OPTION_GAMEPAD_SMOOTH_FACTOR, 0.0F);
			this->m_rotation.y += FPS_CAMERA_ROTATION_SPEED * timeSecond * this->m_previousGamepadViewMovement.x;
		}


		if(this->m_rotation.x >= 80.0F)
			this->m_rotation.x = 80;

		if(this->m_rotation.x < -80)
			this->m_rotation.x = -80;

		if(this->m_rotation.y >= 360.0F)
			this->m_rotation.y -= 360.0F;

		if(this->m_rotation.y < 0)
			this->m_rotation.y += 360.0F;
	}

	/**
	* Update the action
	*/
	void CFPSCamera::UpdateAction() {
		if((this->m_actionState & FPS_CAMERA_FIRE) != 0 && this->m_currentWeapon != nullptr) {
			this->m_currentWeapon->Fire();
		}

		if((this->m_actionState & FPS_CAMERA_RELOAD) != 0 && this->m_currentWeapon != nullptr) {
			this->m_currentWeapon->Reload();
		}

		if((this->m_actionState & FPS_CAMERA_NEXT_WEAPON) != 0 && this->m_weaponsOrder.size() > 1) {
			this->m_currentWeaponIndex = this->m_weaponsOrder[(this->m_currentWeaponIndex + 1) % this->m_weaponsOrder.size()];
			this->m_currentWeapon = this->m_weapons[this->m_currentWeaponIndex].get();
			this->m_weaponChangeSound.SetStatus(daidalosengine::SOUND_STATUS_PLAY);
		}

		if((this->m_actionState & FPS_CAMERA_PREVIOUS_WEAPON) != 0 && this->m_weaponsOrder.size() > 1) {
			this->m_currentWeaponIndex = this->m_weaponsOrder[(this->m_currentWeaponIndex - 1) % this->m_weaponsOrder.size()];
			this->m_currentWeapon = this->m_weapons[this->m_currentWeaponIndex].get();
			this->m_weaponChangeSound.SetStatus(daidalosengine::SOUND_STATUS_PLAY);
		}
	}
}
