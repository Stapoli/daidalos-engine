/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include <3D/Enemy/IEnemyEntity.hpp>
#include "CEnemyStateDie.hpp"

namespace daidalos {
	/**
	* Constructor
	* @param priority The priority
	* @param startAnimation The start animation frame
	* @param endAnimation The end animation frame
	* @param animationSpeed The animation speed
	*/
	CEnemyStateDie::CEnemyStateDie(const unsigned int priority, const float startAnimation, const float endAnimation, const float animationSpeed) : IEnemyState(priority) {
		this->m_dead = false;
		this->SetAnimationFrames(daidalosengine::CVector2F(startAnimation, endAnimation));
		this->SetAnimationSpeed(animationSpeed);

		this->SetFallSound("body_fall.wav");
		this->m_fallSound.SetRelativeToListener(false);
	}

	/**
	* Destructor
	*/
	CEnemyStateDie::~CEnemyStateDie() = default;

	/**
	* Set the dead status.
	* @param dead The dead status
	*/
	void CEnemyStateDie::SetDead(const bool dead) {
		this->m_dead = dead;
	}

	/**
	* Set the fall sound.
	* @param soundName The sound name
	*/
	void CEnemyStateDie::SetFallSound(const std::string & soundName) {
		this->m_fallSound.LoadFromFile(soundName, daidalosengine::SOUND_TYPE_STATIC, daidalosengine::SOUND_CATEGORY_ENVIRONMENT);
	}

	/**
	* Update the entity
	* @param entity The entity
	* @param time Elapsed time
	*/
	void CEnemyStateDie::Update(daidalosgameengine::IEnemyEntity * entity, const float time) {

		if(!this->m_dead) {
			entity->GetMeshNode()->SetLooped(false);
			entity->GetMeshNode()->StartAnimation(this->GetAnimationFrames().x, this->GetAnimationFrames().y, this->GetAnimationSpeed());
			entity->SetCollisionState(false);
			this->m_fallSound.SetPosition(entity->GetVisiblePosition());
			this->m_fallSound.SetStatus(daidalosengine::SOUND_STATUS_PLAY);
			this->m_dead = true;
		}

		PerformEnemyLevelCollisions(entity, daidalosengine::CVector3F(), time);

		// Update the sound
		this->m_fallSound.SetPosition(entity->GetVisiblePosition());
	}

	/**
	* If the state is candidate for the next action
	* @param entity The entity
	* @return True if candidate
	*/
	bool CEnemyStateDie::IsCandidateForNextAction(daidalosgameengine::IEnemyEntity * entity) {
		return entity->GetHealth() == 0;
	}

	/**
	* Get the dead status.
	* @return The dead status
	*/
	bool CEnemyStateDie::IsDead() const {
		return this->m_dead;
	}
}

