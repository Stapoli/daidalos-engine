/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include <Scene/3D/CSpotLightSceneNode.hpp>
#include <Level/ILevel.hpp>
#include <Enums.hpp>
#include "CSpotEntity.hpp"

namespace daidalos {
	/**
	* Constructor
	* @param parent The parent
	* @param meshNode The alarm node
	*/
	CSpotEntity::CSpotEntity(daidalosgameengine::ILevel * parent, daidalosengine::CStaticMeshSceneNode * meshNode) : m_node(meshNode), IEntity(parent) {

		this->SetId(meshNode->GetId());
		this->SetType(daidalosgameengine::ENTITY_TYPE_SPOT);

		// Default values
		bool enabled = true;
		bool shadowCaster = false;
		float lightDecalY = 0;
		float lightRange = 15.0F;
		float lightRadius = 20.0F;
		float lightFallOf = 0.5F;
		float lightTightness = 0.5F;
		daidalosengine::CColor lightColor(1.0F, 1.0F, 1.0F, 1.0F);

		// Read the attributes
		if(!meshNode->GetAttribute("lightDecalY").empty()) {
			std::istringstream ss(meshNode->GetAttribute("lightDecalY"));
			ss >> lightDecalY;
		}
		if(!meshNode->GetAttribute("lightEnabled").empty()) {
			enabled = meshNode->GetAttribute("lightEnabled") == "true";
		}
		if(!meshNode->GetAttribute("lightShadow_caster").empty()) {
			shadowCaster = meshNode->GetAttribute("lightShadow_caster") == "true";
		}
		if(!meshNode->GetAttribute("lightColor").empty()) {
			lightColor = daidalosengine::CColor::GetFrom(meshNode->GetAttribute("lightColor"));
		}
		if(!meshNode->GetAttribute("lightRange").empty()) {
			std::istringstream ss(meshNode->GetAttribute("lightRange"));
			ss >> lightRange;
		}
		if(!meshNode->GetAttribute("lightRadius").empty()) {
			std::istringstream ss(meshNode->GetAttribute("lightRadius"));
			ss >> lightRadius;
		}
		if(!meshNode->GetAttribute("lightFalloff").empty()) {
			std::istringstream ss(meshNode->GetAttribute("lightFalloff"));
			ss >> lightFallOf;
		}
		if(!meshNode->GetAttribute("lightTightness").empty()) {
			std::istringstream ss(meshNode->GetAttribute("lightTightness"));
			ss >> lightTightness;
		}

		// Add the light
		daidalosengine::CSpotLightSceneNode * spot = new daidalosengine::CSpotLightSceneNode(this->GetParent()->GetScene()->GetScene(), this->m_node, "", daidalosengine::CVector3F(0, lightDecalY, 0), daidalosengine::CVector3F(90, 0, 0), lightColor, lightRange, lightRadius, lightFallOf, lightTightness, shadowCaster, enabled);
		meshNode->AddChild(spot);
	}

	/**
	* Update the door
	* @param time Elapsed time
	*/
	void CSpotEntity::Update(const float time) {
		// Nothing to do
	}

	/**
	* Trigger the entity action
	*/
	void CSpotEntity::DoAction() {
		// Nothing to do
	}

	/**
	* Check if the action can be triggered
	* @return True if the action can be triggered, false otherwise
	*/
	bool CSpotEntity::CanDoAction() const {
		return false;
	}
}
