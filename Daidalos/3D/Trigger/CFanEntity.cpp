/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#define ANIMATION_SPEED 80.0F
#define ANIMATION_FACTOR 30.0F

#include <Level/ILevel.hpp>
#include <Enums.hpp>
#include "CFanEntity.hpp"

namespace daidalos {
	/**
	* Constructor
	* @param parent The parent
	* @param meshNode The fan node
	*/
	CFanEntity::CFanEntity(daidalosgameengine::ILevel * parent, daidalosengine::CAnimatedMeshSceneNode * meshNode) : m_node(meshNode), ITriggerEntity(parent) {

		SetId(meshNode->GetId());
		SetType(daidalosgameengine::ENTITY_TYPE_FAN);

		this->m_speed = 0;
		this->m_state = FAN_STATE_STOPPED;

		this->m_node->SetLooped(true);
		this->m_node->SetStartFrame(1);
		this->m_node->SetEndFrame(72);
		this->m_node->SetCurrentFrame(1);
	}

	/**
	* Update the entity
	* @param time Elapsed time
	*/
	void CFanEntity::Update(const float time) {
		const float timePerSecond = time / 1000.0F;

		switch(this->m_state) {
		case FAN_STATE_STARTING:
			this->m_speed += (ANIMATION_FACTOR * timePerSecond);
			if(this->m_speed > ANIMATION_SPEED) {
				this->m_speed = ANIMATION_SPEED;
				this->m_state = FAN_STATE_STARTED;
			}
			this->m_node->SetAnimationSpeed(this->m_speed);
			break;

		case FAN_STATE_STOPPING:
			this->m_speed -= (ANIMATION_FACTOR * timePerSecond);
			if(this->m_speed < 0) {
				this->m_speed = 0;
				this->m_state = FAN_STATE_STOPPED;
				this->m_node->StopAnimation();
			}
			this->m_node->SetAnimationSpeed(this->m_speed);
			break;

		default:
			break;
		}
	}

	/**
	* Trigger the entity action
	*/
	void CFanEntity::DoAction() {
		switch(this->m_state) {
		case FAN_STATE_STARTED:
			Stop();
			break;

		case FAN_STATE_STOPPED:
			Start();
			break;

		default:
			break;
		}
	}

	/**
	* Check if the action can be triggered
	* @return True if the action can be triggered, false otherwise
	*/
	bool CFanEntity::CanDoAction() {
		return this->m_state == FAN_STATE_STARTED || this->m_state == FAN_STATE_STOPPED;
	}

	/**
	* Start the fan
	*/
	void CFanEntity::Start() {
		this->m_speed = 0;
		this->m_state = FAN_STATE_STARTING;
		this->m_node->SetAnimationSpeed(this->m_speed);
		this->m_node->ResumeAnimation();
	}

	/**
	* Stop the fan
	*/
	void CFanEntity::Stop() {
		this->m_speed = ANIMATION_SPEED;
		this->m_state = FAN_STATE_STOPPING;
	}
}