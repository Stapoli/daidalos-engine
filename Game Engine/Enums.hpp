/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __GAMEENGINE_ENUMS_HPP
#define __GAMEENGINE_ENUMS_HPP

namespace daidalosgameengine {

	enum EEntityFlag {
		ENTITY_FLAG_COLLIDABLE = 1 << 1,
		ENTITY_FLAG_TRIGGER = 1 << 2,
		ENTITY_FLAG_ENEMY = 1 << 3,
	};

	enum EEntityType {
		ENTITY_TYPE_ALARM,
		ENTITY_TYPE_DOOR,
		ENTITY_TYPE_FAN,
		ENTITY_TYPE_SWITCH,
		ENTITY_TYPE_TELEPORTER,
		ENTITY_TYPE_ARMOR,
		ENTITY_TYPE_HEALTH,
		ENTITY_TYPE_SHOTGUN_AMMO,
		ENTITY_TYPE_HANDGUN_AMMO,
		ENTITY_TYPE_FLASHLIGHT,
		ENTITY_TYPE_MAGNETIC_CARD,
		ENTITY_TYPE_MAGNETIC_CARD_READER,
		ENTITY_TYPE_SECURITY_CAMERA,
		ENTITY_TYPE_LADDER,
		ENTITY_TYPE_AREA_TRIGGER,
		ENTITY_TYPE_SPOT
	};

	enum ETriggeredResult {
		TRIGGERED_RESULT_NONE,
		TRIGGERED_RESULT_SOME,
		TRIGGERED_RESULT_ALL
	};
}

#endif
