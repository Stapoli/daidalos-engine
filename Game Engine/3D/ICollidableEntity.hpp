/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __GAMEENGINE_ICOLLIDABLEENTITY_HPP
#define __GAMEENGINE_ICOLLIDABLEENTITY_HPP

#include <memory>
#include <Core/Utility/CVector3.hpp>
#include <Scene/3D/IShape3D.hpp>
#include "IEntity.hpp"

namespace daidalosgameengine {

	/**
	* Abstract entity with collision properties
	*/
	class ICollidableEntity : public IEntity {
	private:
		bool m_gravityState;
		bool m_collisionState;
		float m_collisionUpdateFrequency;
		float m_lastCollisionUpdateTimer;
		daidalosengine::CVector3F m_gravity;
		std::unique_ptr<daidalosengine::IShape3DF> m_collisionShape;

	public:
		/**
		* Constructor
		* @param parent The parent
		*/
		explicit ICollidableEntity(ILevel * parent);

		/**
		* Destructor
		*/
		virtual ~ICollidableEntity();

		/**
		* Update the entity
		* @param time Elapsed time
		*/
		virtual void Update(const float time) override = 0;

		/**
		* Set the gravity state
		* @param state The gravity state
		*/
		void SetGravityState(const bool state);

		/**
		* Set the collision state
		* @param state The collision state
		*/
		void SetCollisionState(const bool state);

		/**
		* Set the collision update frequency in ms
		* @param frequency The collision frequency
		*/
		void SetCollisionUpdateFrequency(const float frequency);

		/**
		* Set the gravity.
		* @param gravity The gravity
		*/
		void SetGravity(const daidalosengine::CVector3F & gravity);

		/**
		* Set the collision shape.
		* @param shape The collision shape
		*/
		void SetCollisionShape(daidalosengine::IShape3DF * shape);

		/**
		* Get the gravity state
		* @return The gravity state
		*/
		bool GetGravityState() const;

		/**
		* Get the collision state
		* @return The collision state
		*/
		bool GetCollisionState() const override;

		/**
		* Get the collision frequency
		* @return The collision frequency
		*/
		float GetCollisionFrequency() const;

		/**
		* Get the gravity
		* @return The gravity
		*/
		const daidalosengine::CVector3F & GetGravity() const;

		/**
		* Get the entity collision shape
		* @return The entity collision shape
		*/
		daidalosengine::IShape3DF * GetCollisionShape() override;

	protected:
		/**
		* Callback used to handle event when the entity fall and touch the ground
		* @param gravity The gravity value before touching the ground
		*/
		virtual void OnEntityTouchedGround(const daidalosengine::CVector3F gravity) = 0;

		/**
		* Process the level collisions (movement + gravity) based on the shape.
		* The collision depends on the frequencies (in ms) set
		* @param movement The raw movement in unit per second
		* @param time The elapsed time
		*/
		void PerformLevelCollisions(const daidalosengine::CVector3F& movement, const float time);

		/**
		* Process the entity collisions (movement + gravity) based on the shape.
		* The collision depends on the frequencies (in ms) set
		* @param movement The raw movement in unit per second
		* @param time The elapsed time
		*/
		void PerformEntityCollisions(const daidalosengine::CVector3F& movement, const float time);
	};
}

#endif
