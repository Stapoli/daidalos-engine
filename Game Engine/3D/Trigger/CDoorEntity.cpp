/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include <sstream>
#include "../../Enums.hpp"
#include "../../Level/ILevel.hpp"
#include "CDoorEntity.hpp"

#define TRIGGER_RADIUS 10
#define OPENED_TIME 6000
#define ANIMATION_SPEED 1.0F

namespace daidalosgameengine {
	/**
	* Constructor
	* @param parent The parent
	* @param meshNode The door node
	*/
	CDoorEntity::CDoorEntity(ILevel * parent, daidalosengine::CAnimatedMeshSceneNode * meshNode) : ITriggerEntity(parent), m_node(meshNode) {

		SetId(meshNode->GetId());
		SetType(ENTITY_TYPE_DOOR);
		this->m_emitterNode = nullptr;

		this->m_state = DOOR_STATE_CLOSED;
		this->m_closable = true;
		this->m_animationSpeed = ANIMATION_SPEED;
		this->m_triggerRadius = TRIGGER_RADIUS;
		this->m_timer = 0;

		this->m_startSound.LoadFromFile("door_start.wav", daidalosengine::SOUND_TYPE_STATIC, daidalosengine::SOUND_CATEGORY_ENVIRONMENT);
		this->m_startSound.SetRelativeToListener(false);
		this->m_startSound.SetPosition(this->m_node->GetAbsolutePosition());

		this->m_stopSound.LoadFromFile("door_stop.wav", daidalosengine::SOUND_TYPE_STATIC, daidalosengine::SOUND_CATEGORY_ENVIRONMENT);
		this->m_stopSound.SetRelativeToListener(false);
		this->m_stopSound.SetPosition(this->m_node->GetAbsolutePosition());

		// Read the attributes
		if(meshNode->GetAttribute("closable") == "false") {
			this->m_closable = false;
		}
		if(!meshNode->GetAttribute("animationSpeed").empty()) {
			std::istringstream ss(meshNode->GetAttribute("animationSpeed"));
			ss >> this->m_animationSpeed;
		}
		if(!meshNode->GetAttribute("openingParticleEmitter").empty()) {
			this->m_emitterNode = dynamic_cast<daidalosengine::CParticleEmitterSceneNode*>(GetParent()->GetScene()->GetNode(meshNode->GetAttribute("openingParticleEmitter")));
		}
		if(!meshNode->GetAttribute("triggerRadius").empty()) {
			std::istringstream ss(meshNode->GetAttribute("triggerRadius"));
			ss >> this->m_triggerRadius;
		}
	}

	/**
	* Update the entity
	* @param time Elapsed time
	*/
	void CDoorEntity::Update(const float time) {

		ITriggerEntity::Update(time);
		if(this->m_state == DOOR_STATE_OPENED && this->m_closable) {
			this->m_timer += time;

			// The door stay opened for a limited time
			if(this->m_timer >= OPENED_TIME) {
				this->m_timer = 0;
				this->m_state = DOOR_STATE_CLOSING;
				this->m_node->StartAnimation(2, 3, this->m_animationSpeed);
				GetParent()->GetScene()->SetBrushCollision(this->m_node->GetId(), true);
				this->m_startSound.SetPosition(this->m_node->GetAbsolutePosition());
				this->m_startSound.SetStatus(daidalosengine::SOUND_STATUS_PLAY);
			}
		}

		// End of opening animation
		if(this->m_state == DOOR_STATE_OPENING && this->m_node->GetAnimationState() == daidalosengine::MESH_ANIMATION_STATE_STOPPED) {
			this->m_state = DOOR_STATE_OPENED;
			this->m_timer = 0;
			GetParent()->GetScene()->SetBrushCollision(this->m_node->GetId(), false);
			this->m_stopSound.SetPosition(this->m_node->GetAbsolutePosition());
			this->m_stopSound.SetStatus(daidalosengine::SOUND_STATUS_PLAY);
			if(this->m_emitterNode != nullptr) {
				this->m_emitterNode->SetActive(false);
			}
		}

		// End of closing animation
		if(this->m_state == DOOR_STATE_CLOSING && this->m_node->GetAnimationState() == daidalosengine::MESH_ANIMATION_STATE_STOPPED) {
			this->m_state = DOOR_STATE_CLOSED;
			this->m_stopSound.SetPosition(this->m_node->GetAbsolutePosition());
			this->m_stopSound.SetStatus(daidalosengine::SOUND_STATUS_PLAY);
		}
	}

	/**
	* Trigger the entity action
	*/
	void CDoorEntity::DoAction() {
		switch(this->m_state) {
		case DOOR_STATE_CLOSED:
			this->m_state = DOOR_STATE_OPENING;
			this->m_node->StartAnimation(1, 2, this->m_animationSpeed);
			this->m_startSound.SetPosition(this->m_node->GetAbsolutePosition());
			this->m_startSound.SetStatus(daidalosengine::SOUND_STATUS_PLAY);
			if(this->m_emitterNode != nullptr) {
				this->m_emitterNode->SetActive(true);
			}
			break;

		case DOOR_STATE_OPENED:
			this->m_timer = 0;
			break;

		default:
			break;
		}
	}

	/**
	* Check if the action can be triggered
	* @return True if the action can be triggered, false otherwise
	*/
	bool CDoorEntity::CanDoAction() {
		return this->m_node->IsEnabled() && daidalosengine::CVector3F::Distance(this->m_node->GetAbsolutePosition(), GetParent()->GetPlayer()->GetVisiblePosition()) <= this->m_triggerRadius;
	}
}
