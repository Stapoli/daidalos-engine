/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __GAMEENGINE_CDOORENTITY_HPP
#define __GAMEENGINE_CDOORENTITY_HPP

#include <Scene/3D/CAnimatedMeshSceneNode.hpp>
#include <Scene/3D/CParticleEmitterSceneNode.hpp>
#include <Medias/CSound.hpp>
#include "ITriggerEntity.hpp"

namespace daidalosgameengine {
		
	enum EDoorState {
		DOOR_STATE_CLOSED,
		DOOR_STATE_OPENED,
		DOOR_STATE_OPENING,
		DOOR_STATE_CLOSING
	};

	/**
	* Door entity
	*/
	class CDoorEntity : public ITriggerEntity {
	private:
		daidalosengine::CAnimatedMeshSceneNode * m_node;
		daidalosengine::CParticleEmitterSceneNode * m_emitterNode;

		daidalosengine::CSound m_startSound;
		daidalosengine::CSound m_stopSound;
		EDoorState m_state;
		bool m_closable;
		float m_animationSpeed;
		float m_triggerRadius;
		float m_timer;

	public:
		/**
		* Constructor
		* @param parent The parent
		* @param meshNode The door node
		*/
		CDoorEntity(ILevel * parent, daidalosengine::CAnimatedMeshSceneNode * meshNode);

		/**
		* Update the entity
		* @param time Elapsed time
		*/
		void Update(const float time = 0) override;

		/**
		* Trigger the entity action
		*/
		void DoAction() override;

		/**
		* Check if the action can be triggered
		* @return True if the action can be triggered, false otherwise
		*/
		bool CanDoAction() override;
	};
}

#endif
