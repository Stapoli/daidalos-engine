#include "FileTest.hpp"
#include <Core/Core.hpp>
#include <Core/Utility/CFile.hpp>

CPPUNIT_TEST_SUITE_REGISTRATION(FileTest);

void FileTest::setUp() {
	this->fullname = "C:\\Windows\\notepad.exe";
	this->filename = "notepad.exe";
	this->shortname = "notepad";
	this->extension = "exe";
}

void FileTest::TestConstructors() {
	daidalosengine::CFile file;
	CPPUNIT_ASSERT(!file.FileExists());
}

void FileTest::TestFileExists() {
	daidalosengine::CFile file("cppunit_dll.dll");
	CPPUNIT_ASSERT(file.FileExists());
}

void FileTest::TestGetFileSize() {
	daidalosengine::CFile file("cppunit_dll.dll");
	CPPUNIT_ASSERT(file.GetFileSize() == 659456);
}

void FileTest::TestGetFullname() {
	daidalosengine::CFile file(this->fullname);
	CPPUNIT_ASSERT_EQUAL(this->fullname, file.GetFullname());
}

void FileTest::TestGetFilename() {
	daidalosengine::CFile file(this->fullname);
	CPPUNIT_ASSERT_EQUAL(this->filename, file.GetFilename());
}

void FileTest::TestGetShortname() {
	daidalosengine::CFile file(this->fullname);
	CPPUNIT_ASSERT_EQUAL(this->shortname, file.GetShortname());
}

void FileTest::TestGetExtension() {
	daidalosengine::CFile file(this->fullname);
	CPPUNIT_ASSERT_EQUAL(this->extension, file.GetExtension());
}

