var class_test_result_collector =
[
    [ "TestFailures", "class_test_result_collector.html#a8ae051e883095aee10d9f777664fe8af", null ],
    [ "Tests", "class_test_result_collector.html#a6ab73f8a52311857afa98a1ef049e30a", null ],
    [ "TestResultCollector", "class_test_result_collector.html#a8bc475c91000fca9f0ed34e105577767", null ],
    [ "~TestResultCollector", "class_test_result_collector.html#a2e905ace0bcb819bbe190faf16ea1aed", null ],
    [ "addFailure", "class_test_result_collector.html#af53cbc55621f0eb0dc02d8b0203ea321", null ],
    [ "failures", "class_test_result_collector.html#ae861f1cd30963e6cba062703c2c50b5c", null ],
    [ "freeFailures", "class_test_result_collector.html#ae3ca204ba9e242a368843e91acf94766", null ],
    [ "reset", "class_test_result_collector.html#a1e20981cc1e127942154a643639d4740", null ],
    [ "runTests", "class_test_result_collector.html#a347947dd844c504b242ef4437cc4de4b", null ],
    [ "startTest", "class_test_result_collector.html#a647d3e05ffc8fcf7023fea2f7c9f95cb", null ],
    [ "testErrors", "class_test_result_collector.html#ae54053b5bee8ee474e8540603c9bcb2d", null ],
    [ "testFailures", "class_test_result_collector.html#a8c567d8cafc477ec4e70aaed6ac7faf3", null ],
    [ "testFailuresTotal", "class_test_result_collector.html#a19f079745c9794c858d5f9cba550079d", null ],
    [ "tests", "class_test_result_collector.html#a226a75e1c7b12d72258f8f54963c663f", null ],
    [ "m_failures", "class_test_result_collector.html#ae2958ff17d1fd73092bf8ec4fc5db08f", null ],
    [ "m_testErrors", "class_test_result_collector.html#a42fdacb6076281e8abb6eba08063fb4e", null ],
    [ "m_tests", "class_test_result_collector.html#a63950609cce95633b56850da67d4bfe0", null ]
];