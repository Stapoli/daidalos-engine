var NAVTREE =
[
  [ "Daidalos Engine", "index.html", [
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"class_d_e_1_1_c_animated_mesh_scene_node.html#a170b0f5e41f8306302a229f2399a1017",
"class_d_e_1_1_c_d_x9_renderer.html#a9b096e8c83573a0f2fd2e68ba9876a0a",
"class_d_e_1_1_c_linear_velocity_particle_initializer.html#a53cd9340ea29458b96eec5be4a9a52b9",
"class_d_e_1_1_c_particle_system.html#ad40ac4c2c21252affbde5fe18aadd1aa",
"class_d_e_1_1_c_sound_scene_node.html#a0a8d5a9b7c984691eb4f3129c9393369",
"class_d_e_1_1_c_velocity_particle_updater.html#a7e369199aeeec3634c20ed4fbc6570b2",
"class_d_e_1_1_i_mesh_scene_node.html#a1a40b14798e582c2fa1a48bb293e99b7",
"class_d_e_1_1_i_sound_base.html#a625deaa3052d5214e4820b03cc8ad193",
"class_s_a_m_p_l_e_s_1_1_i_sample_application.html#a0b8e8d3d5b5778171f8fa73729b39bde",
"struct_d_e_1_1_s_m_s3_d_data.html#a0e9edb4f249574603996a7081ce92be4"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';