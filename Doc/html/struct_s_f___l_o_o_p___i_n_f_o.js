var struct_s_f___l_o_o_p___i_n_f_o =
[
    [ "bpm", "struct_s_f___l_o_o_p___i_n_f_o.html#a262bdf39e1df370c52671b5147c81924", null ],
    [ "future", "struct_s_f___l_o_o_p___i_n_f_o.html#afb3ef90efaaa811d6748854b3830196d", null ],
    [ "loop_mode", "struct_s_f___l_o_o_p___i_n_f_o.html#a4e33720922b6aeca9e7b6dbea3c102ca", null ],
    [ "num_beats", "struct_s_f___l_o_o_p___i_n_f_o.html#a5e73babc2985c7a94fc81349fb6d8e38", null ],
    [ "root_key", "struct_s_f___l_o_o_p___i_n_f_o.html#a2755451bf6c64d3c0f8202d7e427f6b8", null ],
    [ "time_sig_den", "struct_s_f___l_o_o_p___i_n_f_o.html#a0a90aff8f6d4a38e70beea2ee589df8e", null ],
    [ "time_sig_num", "struct_s_f___l_o_o_p___i_n_f_o.html#a6ece67f0077c22fadbc875e0250ad656", null ]
];