var classtinyxml2_1_1_str_pair =
[
    [ "NEEDS_ENTITY_PROCESSING", "classtinyxml2_1_1_str_pair.html#a13363385382e915f9aeb05f6455eb1fea4f1e01a55f8efe4ca72c32d454060237", null ],
    [ "NEEDS_NEWLINE_NORMALIZATION", "classtinyxml2_1_1_str_pair.html#a13363385382e915f9aeb05f6455eb1fea8f2045d56e70745d718672c0da91d0e0", null ],
    [ "COLLAPSE_WHITESPACE", "classtinyxml2_1_1_str_pair.html#a13363385382e915f9aeb05f6455eb1feab17a85f396c221811fe49263bf6f843f", null ],
    [ "TEXT_ELEMENT", "classtinyxml2_1_1_str_pair.html#a13363385382e915f9aeb05f6455eb1feaae519eb5a639858591763aa5fc6cc953", null ],
    [ "TEXT_ELEMENT_LEAVE_ENTITIES", "classtinyxml2_1_1_str_pair.html#a13363385382e915f9aeb05f6455eb1fea96be48cf899bfeea0aa227f984f1fa63", null ],
    [ "ATTRIBUTE_NAME", "classtinyxml2_1_1_str_pair.html#a13363385382e915f9aeb05f6455eb1feaaab1cbefaa977e6f772b4e2575417aeb", null ],
    [ "ATTRIBUTE_VALUE", "classtinyxml2_1_1_str_pair.html#a13363385382e915f9aeb05f6455eb1fea6d72f9ce15f50e8bcd680edf66235dfd", null ],
    [ "ATTRIBUTE_VALUE_LEAVE_ENTITIES", "classtinyxml2_1_1_str_pair.html#a13363385382e915f9aeb05f6455eb1fea2decbd2513ac14f8befa987938326399", null ],
    [ "COMMENT", "classtinyxml2_1_1_str_pair.html#a13363385382e915f9aeb05f6455eb1fea067a6ec90c8beea1cf5992930d93bffa", null ],
    [ "StrPair", "classtinyxml2_1_1_str_pair.html#a69153963f7052de9f767d3d8c1623a70", null ],
    [ "~StrPair", "classtinyxml2_1_1_str_pair.html#a60bed84d2503296e1c2a73fcef1431f9", null ],
    [ "Empty", "classtinyxml2_1_1_str_pair.html#aca963a7eaa900bfddbea7312f040b39c", null ],
    [ "GetStr", "classtinyxml2_1_1_str_pair.html#a27290a8fec20c0a6ccf9d8362f5c3b1d", null ],
    [ "ParseName", "classtinyxml2_1_1_str_pair.html#a8452c0c7f140d4d613134e952ce54f14", null ],
    [ "ParseText", "classtinyxml2_1_1_str_pair.html#a7ff60a4aaea3ad696e940ecd31806a29", null ],
    [ "Set", "classtinyxml2_1_1_str_pair.html#a4f05549373394266a1eecba26813c166", null ],
    [ "SetInternedStr", "classtinyxml2_1_1_str_pair.html#a2baf6230e18333e02ab65d0897ee3941", null ],
    [ "SetStr", "classtinyxml2_1_1_str_pair.html#a1f82ec6b5bee35ee7466d8565e43b1de", null ]
];