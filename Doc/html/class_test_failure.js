var class_test_failure =
[
    [ "TestFailure", "class_test_failure.html#a0ecf7ad4ab673974d30d10ffb2b01790", null ],
    [ "~TestFailure", "class_test_failure.html#a000c058419a8177d64d1019c6c2f624d", null ],
    [ "clone", "class_test_failure.html#a13d79822f39adb9767bbb963020ed020", null ],
    [ "failedTest", "class_test_failure.html#a9ba800ed7c4c61abbca73cf3bff51929", null ],
    [ "failedTestName", "class_test_failure.html#a3da3ae30b62fc1e24b9c866c690aad41", null ],
    [ "isError", "class_test_failure.html#af3a5bf501d88b0b6eb450f97813f7cec", null ],
    [ "sourceLine", "class_test_failure.html#a91bea2a4ad7eed14edb0571dc9d1ac61", null ],
    [ "thrownException", "class_test_failure.html#acb3a26d7adcfa0819b3715aadc2a3b56", null ],
    [ "m_failedTest", "class_test_failure.html#a2ba79089909967b45aedc4992b51553a", null ],
    [ "m_isError", "class_test_failure.html#abf258e5bec289b51fb13147a62b8b75d", null ],
    [ "m_thrownException", "class_test_failure.html#a96bfae3c1f32d0c1729501d68016f431", null ]
];