var struct_s_f___i_n_s_t_r_u_m_e_n_t =
[
    [ "basenote", "struct_s_f___i_n_s_t_r_u_m_e_n_t.html#ab131f2d1e1297b20b9b64596bd9fef2c", null ],
    [ "count", "struct_s_f___i_n_s_t_r_u_m_e_n_t.html#af1a76686638c1d4d722a543cc09ca9bc", null ],
    [ "detune", "struct_s_f___i_n_s_t_r_u_m_e_n_t.html#a15c6ebfa70d434dae34742c58a8ef9bf", null ],
    [ "end", "struct_s_f___i_n_s_t_r_u_m_e_n_t.html#aaae008f486b2db6b71564456736634a2", null ],
    [ "gain", "struct_s_f___i_n_s_t_r_u_m_e_n_t.html#a1c157f314b852c291150feab66f1f7fe", null ],
    [ "key_hi", "struct_s_f___i_n_s_t_r_u_m_e_n_t.html#ac5d3d85d2218849b73620cb1a70ea609", null ],
    [ "key_lo", "struct_s_f___i_n_s_t_r_u_m_e_n_t.html#a9c59a466ad6937c6d6a5134a642aae6c", null ],
    [ "loop_count", "struct_s_f___i_n_s_t_r_u_m_e_n_t.html#a070427d027c390e850951c83bda44d07", null ],
    [ "loops", "struct_s_f___i_n_s_t_r_u_m_e_n_t.html#a449a4bd4d02863ebc2c6253af051cca0", null ],
    [ "mode", "struct_s_f___i_n_s_t_r_u_m_e_n_t.html#af222d1a939525fe44aa377ff764570be", null ],
    [ "start", "struct_s_f___i_n_s_t_r_u_m_e_n_t.html#a4053f5d0ec83a281264dfd9c17ae921c", null ],
    [ "velocity_hi", "struct_s_f___i_n_s_t_r_u_m_e_n_t.html#a4e1435a98a085a8df95f10ff1bed536f", null ],
    [ "velocity_lo", "struct_s_f___i_n_s_t_r_u_m_e_n_t.html#a3637643126becba84608741033f91f47", null ]
];