var class_d_e_1_1_i_buffer_base =
[
    [ "~IBufferBase", "class_d_e_1_1_i_buffer_base.html#ac1fea86141679cb7778c2d9c7bd2b063", null ],
    [ "IBufferBase", "class_d_e_1_1_i_buffer_base.html#a4ba0ea4214ca91491c769982399b4098", null ],
    [ "GetSize", "class_d_e_1_1_i_buffer_base.html#a3ef84aceaab1c32a5d738d8bf84b2d0b", null ],
    [ "Lock", "class_d_e_1_1_i_buffer_base.html#a6cddb8fea508e2db4b079cd1b5fdd09b", null ],
    [ "Unlock", "class_d_e_1_1_i_buffer_base.html#aeaa896d0578c7c0bec670621b5ff8327", null ]
];