var struct_d_e_1_1_s_m_s3_d_material =
[
    [ "alpha", "struct_d_e_1_1_s_m_s3_d_material.html#ac5e186bc2a8f60122994ea63045f7104", null ],
    [ "ambient", "struct_d_e_1_1_s_m_s3_d_material.html#af762c9bbbf1aeec36c295f106eb41f81", null ],
    [ "comment", "struct_d_e_1_1_s_m_s3_d_material.html#a4ff768ed506d6a46b9e3a3ac23ac709a", null ],
    [ "diffuse", "struct_d_e_1_1_s_m_s3_d_material.html#ab4d9ad0a18ec5351077a2d63c795296d", null ],
    [ "emission", "struct_d_e_1_1_s_m_s3_d_material.html#a1aa4bcc0134bb94f47ed0c8dd822e53e", null ],
    [ "mode", "struct_d_e_1_1_s_m_s3_d_material.html#a490536933325e822ae345ab58f1d321a", null ],
    [ "name", "struct_d_e_1_1_s_m_s3_d_material.html#a518aa0548705ae169dde8c796d06fb2e", null ],
    [ "shininess", "struct_d_e_1_1_s_m_s3_d_material.html#af666de18447149e43e3e80661523a0ca", null ],
    [ "specular", "struct_d_e_1_1_s_m_s3_d_material.html#a60f0af58feab39c6cd98863639677af0", null ],
    [ "texture", "struct_d_e_1_1_s_m_s3_d_material.html#a6266fae666a92fbc2bb9ab2ff0c5872c", null ],
    [ "transparency", "struct_d_e_1_1_s_m_s3_d_material.html#ae1bd32be434ffaafbea680317a2d6f63", null ]
];