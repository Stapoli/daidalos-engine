var class_test_suite_builder_context_base =
[
    [ "Properties", "class_test_suite_builder_context_base.html#a95565394a60a8b8d843a5a64d2572a86", null ],
    [ "Property", "class_test_suite_builder_context_base.html#a1e2319a53ecd7a718c87f4ea5c42d4c6", null ],
    [ "TestSuiteBuilderContextBase", "class_test_suite_builder_context_base.html#a434b04354f7e326c7f88d015921d5e6d", null ],
    [ "~TestSuiteBuilderContextBase", "class_test_suite_builder_context_base.html#a25868530a384995987e2260fda4adae0", null ],
    [ "addProperty", "class_test_suite_builder_context_base.html#a358c75376f4d3df2ef570003cbe2e06a", null ],
    [ "addTest", "class_test_suite_builder_context_base.html#a8b42185139e3efe07a0fe69b9549e928", null ],
    [ "getFixtureName", "class_test_suite_builder_context_base.html#a715ebe9b929a416525e8d505ff5fef5c", null ],
    [ "getStringProperty", "class_test_suite_builder_context_base.html#a00c3107496ea7ec1523eac7928f98809", null ],
    [ "getTestNameFor", "class_test_suite_builder_context_base.html#a524c035a168900df5899b8d64f6044d0", null ],
    [ "makeTestFixture", "class_test_suite_builder_context_base.html#a78fea10d2d75eeeef8c542f9800c9ca4", null ],
    [ "m_factory", "class_test_suite_builder_context_base.html#ad814ae6df45822300bf99dfcee0277f7", null ],
    [ "m_namer", "class_test_suite_builder_context_base.html#abdbbb982c6c41ae8ecc5abe954c56e70", null ],
    [ "m_suite", "class_test_suite_builder_context_base.html#a16ea2e4d82cd9f94b843839dc8ed2931", null ]
];