var _helper_macros_8h =
[
    [ "CPPUNIT_REGISTRY_ADD", "_helper_macros_8h.html#a0785e2e8a821f70c69a8127c35c0a667", null ],
    [ "CPPUNIT_REGISTRY_ADD_TO_DEFAULT", "_helper_macros_8h.html#a9c3be3389213e1dc823ed580cc60878f", null ],
    [ "CPPUNIT_TEST", "group___writing_test_fixture.html#gaac9b03d898b207e1daf2f93867935a96", null ],
    [ "CPPUNIT_TEST_EXCEPTION", "group___writing_test_fixture.html#gaca8eeb6f60714baade6cbfd185868c40", null ],
    [ "CPPUNIT_TEST_FAIL", "group___writing_test_fixture.html#ga5bdaf0444216a8f93ead13d5ae964d7e", null ],
    [ "CPPUNIT_TEST_SUB_SUITE", "group___writing_test_fixture.html#gae19f30ade82172cf6c3ff297367a10c2", null ],
    [ "CPPUNIT_TEST_SUITE", "group___writing_test_fixture.html#gabe1e12200f40d6f25d60c1783c99da81", null ],
    [ "CPPUNIT_TEST_SUITE_ADD_CUSTOM_TESTS", "group___writing_test_fixture.html#ga516fec19cd7a7acb6fbf194bd98c4c09", null ],
    [ "CPPUNIT_TEST_SUITE_ADD_TEST", "group___writing_test_fixture.html#gaace55a4a3a4f3e0cd219d38e98d4f48f", null ],
    [ "CPPUNIT_TEST_SUITE_END", "group___writing_test_fixture.html#ga601b2e1d525f3947b216e28c625abcb1", null ],
    [ "CPPUNIT_TEST_SUITE_END_ABSTRACT", "group___writing_test_fixture.html#gadcb50ede05c9da831bd8b0140fb59094", null ],
    [ "CPPUNIT_TEST_SUITE_NAMED_REGISTRATION", "_helper_macros_8h.html#a028a5855a40ad3836e2a26aa48cd4c91", null ],
    [ "CPPUNIT_TEST_SUITE_PROPERTY", "group___writing_test_fixture.html#gac5ac4d8544ac2648a28bf7e4dcb27b9d", null ],
    [ "CPPUNIT_TEST_SUITE_REGISTRATION", "_helper_macros_8h.html#a2f4071eec88d1e306665ada0f2dd80e4", null ]
];