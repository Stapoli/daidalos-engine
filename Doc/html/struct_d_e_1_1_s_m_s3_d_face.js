var struct_d_e_1_1_s_m_s3_d_face =
[
    [ "flags", "struct_d_e_1_1_s_m_s3_d_face.html#a0dd38e8e6a747c49ccfe135517f304fc", null ],
    [ "groupId", "struct_d_e_1_1_s_m_s3_d_face.html#a2f3d0c1a1ca81d73b545c168cd22d399", null ],
    [ "normals", "struct_d_e_1_1_s_m_s3_d_face.html#a5a8ef1645f9d34432ce1742dafceeba0", null ],
    [ "smoothing", "struct_d_e_1_1_s_m_s3_d_face.html#a0340a287ce5cdfc7f29ad15d06143fe0", null ],
    [ "texCoords", "struct_d_e_1_1_s_m_s3_d_face.html#af0f9d21c7fd398cb392ec050ce2750c5", null ],
    [ "vertexIndices", "struct_d_e_1_1_s_m_s3_d_face.html#a7f081db0a90e77df2f408819189da5a9", null ]
];