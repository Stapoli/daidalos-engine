var class_vector2_test =
[
    [ "setUp", "class_vector2_test.html#a3a2c9de23a881e2d800176d33e15f11b", null ],
    [ "tearDown", "class_vector2_test.html#a14ee9c68e5e311aa6c1865e04e9087a7", null ],
    [ "TestConstructors", "class_vector2_test.html#a6839d7c96eca6043e38b2dc2795df451", null ],
    [ "TestDistance", "class_vector2_test.html#ac5922f7f54a92128959ade25987661fc", null ],
    [ "TestDotProduct", "class_vector2_test.html#a808b300f8217a5a4cdbc3f3da45aaa70", null ],
    [ "TestGetFrom", "class_vector2_test.html#a3f0a24d1375cc911f839eadba7af73f0", null ],
    [ "TestLength", "class_vector2_test.html#a8f2eeaddb89c8677c9833f005cbda9fe", null ],
    [ "TestNormalize", "class_vector2_test.html#a1ca77a5772256bd7190a055a548ccbe8", null ],
    [ "TestOperators", "class_vector2_test.html#a585b4e46996817f2f876055da680d768", null ]
];