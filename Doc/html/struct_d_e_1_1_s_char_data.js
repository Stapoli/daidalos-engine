var struct_d_e_1_1_s_char_data =
[
    [ "height", "struct_d_e_1_1_s_char_data.html#a8335d9b9d676451898ece9c72b91ec04", null ],
    [ "id", "struct_d_e_1_1_s_char_data.html#acec599e916d6ff4758d18d5db5a6c8d5", null ],
    [ "kerning", "struct_d_e_1_1_s_char_data.html#a8b4accfc341366b53c7083fd2b86b9db", null ],
    [ "width", "struct_d_e_1_1_s_char_data.html#a164bc4cdd698cb282de89e8222e88514", null ],
    [ "x", "struct_d_e_1_1_s_char_data.html#a3f473b04167e7b2ffa8cb8fad0c76292", null ],
    [ "xadvance", "struct_d_e_1_1_s_char_data.html#a6b7d93ea8ae5263824f81f049e446340", null ],
    [ "xoffset", "struct_d_e_1_1_s_char_data.html#a24b65aae8012d0f0e8429175060674a6", null ],
    [ "y", "struct_d_e_1_1_s_char_data.html#acb1a58d043b00e5ef1be8719c13f01ca", null ],
    [ "yoffset", "struct_d_e_1_1_s_char_data.html#abd58b2fd8badfbacf6896ae15fc92b97", null ]
];