var class_matrix_test =
[
    [ "setUp", "class_matrix_test.html#af86aea13086705fdb94ea5af691ce0e1", null ],
    [ "tearDown", "class_matrix_test.html#a249ec7ee7e4d2f5bf57c869c65c9a9fd", null ],
    [ "TestConstructors", "class_matrix_test.html#a188e9d60220fde67d2ea4eef033da93a", null ],
    [ "TestDecompose", "class_matrix_test.html#a2439246316b240f722a4b068b9f27249", null ],
    [ "TestDeterminant", "class_matrix_test.html#add1a2fd488485763a3a4e7b4bd076c5e", null ],
    [ "TestGetRotation", "class_matrix_test.html#aa239e0adb3eb965f197b307634b10f7d", null ],
    [ "TestGetScale", "class_matrix_test.html#a41e4741b64226d61e8c0b15666d04e86", null ],
    [ "TestGetTranslation", "class_matrix_test.html#a9a759b798cb0b6b4f061ef4831d42d29", null ],
    [ "TestIdentity", "class_matrix_test.html#acde148b7b2af7a9bf43017dcf82e5314", null ],
    [ "TestInverse", "class_matrix_test.html#a1f785878d9d6315423f30cd0ba9283ac", null ],
    [ "TestOperators", "class_matrix_test.html#a0c9e3f62f243671620d031c2fd020465", null ],
    [ "TestRotationQuaternion", "class_matrix_test.html#af68ddd38b0baec598f7e2116ffea9af5", null ],
    [ "TestSetRotation", "class_matrix_test.html#a67c409040e9c1b1bbf2570783c5599cf", null ],
    [ "TestSetScale", "class_matrix_test.html#a67f5ade558ab9a136baafa4d6d65158b", null ],
    [ "TestSetTranslation", "class_matrix_test.html#a23c6c832bcae4404e3682fd0c7595542", null ],
    [ "TestTransformArray", "class_matrix_test.html#a90f84288aa7ec0c55563a8c6a7fe1049", null ],
    [ "TestTransformVector3", "class_matrix_test.html#a1fa16dd3d3fa0e543f1f8e4d19befbad", null ],
    [ "TestTransformVector4", "class_matrix_test.html#a71fae1f6543a24a85f9dce3b50600b83", null ],
    [ "TestTranspose", "class_matrix_test.html#afe4a9fb4a8ecd59fddc0044ce68a23a0", null ]
];