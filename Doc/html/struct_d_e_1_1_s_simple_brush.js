var struct_d_e_1_1_s_simple_brush =
[
    [ "filename", "struct_d_e_1_1_s_simple_brush.html#ac61a9ce92c91f79e32e5d1f275d205ab", null ],
    [ "geometry", "struct_d_e_1_1_s_simple_brush.html#accafcea0ea0166a24b4e6520645f57b0", null ],
    [ "isMultiBrush", "struct_d_e_1_1_s_simple_brush.html#a9ce9d52363fca46d9ff31538ffd5d669", null ],
    [ "position", "struct_d_e_1_1_s_simple_brush.html#ac9ffee7ab4cd6358b01f7dab848283a3", null ],
    [ "rotation", "struct_d_e_1_1_s_simple_brush.html#a494c3fca926ac724e69ea4aba8287bdd", null ],
    [ "scale", "struct_d_e_1_1_s_simple_brush.html#a71cf1f0887fef4d78c033238fea7fdb8", null ],
    [ "visibilityShape", "struct_d_e_1_1_s_simple_brush.html#aeb6f6518147afd3d328a4576f8eb3d7a", null ]
];