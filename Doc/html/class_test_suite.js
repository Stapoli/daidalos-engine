var class_test_suite =
[
    [ "TestSuite", "class_test_suite.html#aedef358022b725810f2f145ac078c01b", null ],
    [ "~TestSuite", "class_test_suite.html#a1a4603e985169c62d251876dd3910b5e", null ],
    [ "addTest", "class_test_suite.html#a8cd9628eee382a9670076765ec25cfa8", null ],
    [ "deleteContents", "class_test_suite.html#ac968917f934d102227abd8b2130e67f9", null ],
    [ "doGetChildTestAt", "class_test_suite.html#a2d9006318f7ceecf368f8cc9d4e7abd3", null ],
    [ "getChildTestCount", "class_test_suite.html#a43d23da8ce3225af676ac259452098b0", null ],
    [ "getTests", "class_test_suite.html#a65a0cee9e68fdf8d1416c5f06a4016b5", null ]
];