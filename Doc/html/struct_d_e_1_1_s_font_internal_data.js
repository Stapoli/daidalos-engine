var struct_d_e_1_1_s_font_internal_data =
[
    [ "base", "struct_d_e_1_1_s_font_internal_data.html#aa417e72b50586558681ae029088a8eec", null ],
    [ "bold", "struct_d_e_1_1_s_font_internal_data.html#a6b0ad369752f628ab2f3afed77a83f0e", null ],
    [ "charData", "struct_d_e_1_1_s_font_internal_data.html#aed39fd86cb87ae5d4045cb182dcd2458", null ],
    [ "fontName", "struct_d_e_1_1_s_font_internal_data.html#a84e6c39ea0f1b442d7e3650196070de7", null ],
    [ "italic", "struct_d_e_1_1_s_font_internal_data.html#af8ff393e0d22b47e299ec8406e02a185", null ],
    [ "lineHeight", "struct_d_e_1_1_s_font_internal_data.html#a9f925370ac96e77e717a7ff3da37e9e7", null ],
    [ "padding", "struct_d_e_1_1_s_font_internal_data.html#a8bc6597589586fc369ed437f63e1b9e0", null ],
    [ "size", "struct_d_e_1_1_s_font_internal_data.html#a1af3b5d216cea7b337217f665e0eb5ff", null ],
    [ "spacing", "struct_d_e_1_1_s_font_internal_data.html#a5cc2da8793b39c776a9e97431049d2b7", null ],
    [ "texture", "struct_d_e_1_1_s_font_internal_data.html#a62dbcc7b921d72e7edf4bf08ce51bfe1", null ]
];