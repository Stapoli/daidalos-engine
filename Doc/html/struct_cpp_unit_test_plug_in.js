var struct_cpp_unit_test_plug_in =
[
    [ "~CppUnitTestPlugIn", "struct_cpp_unit_test_plug_in.html#a62e68f9d54b9791f1f3556fb8df2af6a", null ],
    [ "addListener", "struct_cpp_unit_test_plug_in.html#aad8038dc72d0f9798379937fe5692c97", null ],
    [ "addXmlOutputterHooks", "struct_cpp_unit_test_plug_in.html#a547cfddd0513dc9182721f723e27d9e3", null ],
    [ "initialize", "struct_cpp_unit_test_plug_in.html#aec670330e7fced26c2a66b1dcd56edc0", null ],
    [ "removeListener", "struct_cpp_unit_test_plug_in.html#a8f36157014b515d38efbc8ab67923d85", null ],
    [ "removeXmlOutputterHooks", "struct_cpp_unit_test_plug_in.html#a045727ad9658525838b0b9157065fbcd", null ],
    [ "uninitialize", "struct_cpp_unit_test_plug_in.html#a8628d2026e76c58f715e17af88f77458", null ]
];