var class_sini_p_property =
[
    [ "SiniPProperty", "class_sini_p_property.html#a4a5008bd95b5764ab69ab5eaea71f18d", null ],
    [ "SiniPProperty", "class_sini_p_property.html#a120d6f1c645feb47e50d10c66c01866d", null ],
    [ "GetComment", "class_sini_p_property.html#a889eee148cd056237f961a7daa73f516", null ],
    [ "GetKey", "class_sini_p_property.html#a91301b90f5cfb156f04fa4a486e4a09c", null ],
    [ "GetValue", "class_sini_p_property.html#ae2a6dfea69441767a01b273a3195c77b", null ],
    [ "SetComment", "class_sini_p_property.html#ad92405bf0732eb11e0a9512bba41862d", null ],
    [ "SetValue", "class_sini_p_property.html#ac12c34008acd0d9937c5defb08ed79b2", null ],
    [ "ToString", "class_sini_p_property.html#ae8622208b21423dc5453c5d21bcee65c", null ],
    [ "m_comment", "class_sini_p_property.html#a84456fcecf04df667727e0042b0d68ae", null ],
    [ "m_key", "class_sini_p_property.html#ab3da1a14fca0d7c7fa47bb77a2ba1735", null ],
    [ "m_value", "class_sini_p_property.html#ad7979f059d8748ffde0ca8d9f5494c94", null ]
];