var class_d_e_1_1_c_impact =
[
    [ "CImpact", "class_d_e_1_1_c_impact.html#a413f73199d1bc7e181e0a8ff7cf7d713", null ],
    [ "ClearImpact", "class_d_e_1_1_c_impact.html#ae1783a46054a436feed271bcd752d44b", null ],
    [ "GetDistance", "class_d_e_1_1_c_impact.html#a86b4c437a9642c2fd834c6e7569b5dcb", null ],
    [ "GetEndPoint", "class_d_e_1_1_c_impact.html#a5295b80f08d7c098232ab5f8e4a2efa7", null ],
    [ "GetImpactFraction", "class_d_e_1_1_c_impact.html#a6a854d7e40b4ee79a4b5c387dfca29a0", null ],
    [ "GetPlaneNormal", "class_d_e_1_1_c_impact.html#a62a40d55d2bf01dc1146897befebf756", null ],
    [ "GetRealFraction", "class_d_e_1_1_c_impact.html#a1464ece3ce543e57573f00d1425a70b2", null ],
    [ "GetStartPoint", "class_d_e_1_1_c_impact.html#acd40096bcd8f623555b9662e865eb128", null ],
    [ "Initialize", "class_d_e_1_1_c_impact.html#a41b1d72c01feca89aa7c5d802444fcc7", null ],
    [ "IsImpact", "class_d_e_1_1_c_impact.html#a7fbc6b5e6c600b53c1d9331caea639ac", null ],
    [ "SetImpact", "class_d_e_1_1_c_impact.html#a337cdcc5258fe71f2ef989b43ae32419", null ],
    [ "SetNearestImpact", "class_d_e_1_1_c_impact.html#ae3d55b4c772a2b39563f094d2c0e52ad", null ]
];