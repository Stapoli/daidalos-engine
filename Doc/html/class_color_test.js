var class_color_test =
[
    [ "setUp", "class_color_test.html#a7fe71f4bb8f310a679bedd1ecc7690a2", null ],
    [ "tearDown", "class_color_test.html#a19389e76c0e7bf30582c735ccf8bc485", null ],
    [ "TestConstructors", "class_color_test.html#a57d67b473896d6c34638670e522eddd7", null ],
    [ "TestConverters", "class_color_test.html#a0b66ab6a6b187d760e4d6094fd2e41d6", null ],
    [ "TestGetters", "class_color_test.html#a995290b02a9f73b823ab4e28161d8fa1", null ],
    [ "TestOperators", "class_color_test.html#a4be5889296530aa3b5a121e5acec9b74", null ],
    [ "TestSetters", "class_color_test.html#a7e35a595353cbeef78a46cb06ca3cb58", null ]
];