var class_mfc_test_runner =
[
    [ "Tests", "class_mfc_test_runner.html#a6911063cb903f782927897e33df1692f", null ],
    [ "MfcTestRunner", "class_mfc_test_runner.html#a604ac682ac85e92ee2753f724e6c8f41", null ],
    [ "~MfcTestRunner", "class_mfc_test_runner.html#ae6411992b85b9b761efe6d5a9f1dbbeb", null ],
    [ "addTest", "class_mfc_test_runner.html#aedb029517afb5863e483bf9ff0d7d1ed", null ],
    [ "addTests", "class_mfc_test_runner.html#a47e956b56a547344dc30e1c5db73f4e5", null ],
    [ "getRootTest", "class_mfc_test_runner.html#ad810643faf74943741660ca94b9811ee", null ],
    [ "run", "class_mfc_test_runner.html#aea503f886c3a992b557d4dfc223bb0b2", null ],
    [ "m_suite", "class_mfc_test_runner.html#a7700f0b285e70f42f3e84b56be890658", null ],
    [ "m_tests", "class_mfc_test_runner.html#a041d453efb2f9e262676f1d68f1c22af", null ]
];