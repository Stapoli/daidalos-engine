var struct_d_e_1_1_s_m_s3_d_vertex =
[
    [ "boneId", "struct_d_e_1_1_s_m_s3_d_vertex.html#a8af8c3ecd7e33ae0cfc8796e77be95e8", null ],
    [ "boneIds", "struct_d_e_1_1_s_m_s3_d_vertex.html#a7fde32060b2568fe106ffd0c9e0cf946", null ],
    [ "extra", "struct_d_e_1_1_s_m_s3_d_vertex.html#a1bfb0128a2ccd4daf4a7532b3ab6c750", null ],
    [ "flag", "struct_d_e_1_1_s_m_s3_d_vertex.html#aaf06c42f23acab2e118008d3263a8678", null ],
    [ "position", "struct_d_e_1_1_s_m_s3_d_vertex.html#ac63f11531eee9510ae3f2690c0b4b213", null ],
    [ "unused", "struct_d_e_1_1_s_m_s3_d_vertex.html#a8baeaf2954d60f744b0ca61ac34eb7db", null ],
    [ "weights", "struct_d_e_1_1_s_m_s3_d_vertex.html#aa02ef1e6bd5c3f212aca93957978a342", null ]
];