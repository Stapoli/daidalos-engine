var struct_d_e_1_1_s_s_pot_lights_shader_data =
[
    [ "color", "struct_d_e_1_1_s_s_pot_lights_shader_data.html#a03ddb2421d02d74359957c583e5f4bab", null ],
    [ "direction", "struct_d_e_1_1_s_s_pot_lights_shader_data.html#a340fda27e23fe045e70c40faccfbbccb", null ],
    [ "distanceToCamera", "struct_d_e_1_1_s_s_pot_lights_shader_data.html#ace302c9b92869fa821a6cf136c8e4e54", null ],
    [ "enabled", "struct_d_e_1_1_s_s_pot_lights_shader_data.html#a9aa7d49d74e5b9164be4a6d62322d976", null ],
    [ "node", "struct_d_e_1_1_s_s_pot_lights_shader_data.html#a2dcf241b24bd6af77045a8c6f8c79529", null ],
    [ "numberOfLights", "struct_d_e_1_1_s_s_pot_lights_shader_data.html#ac61b19c48aa62f3984b87520a32376ea", null ],
    [ "position", "struct_d_e_1_1_s_s_pot_lights_shader_data.html#ac8d1c560344fd51cb55dc7f47bbb33ab", null ],
    [ "rangeRadiusFalloffTightness", "struct_d_e_1_1_s_s_pot_lights_shader_data.html#a7cdc2f68590e2ffd6e34a4e83fb66291", null ],
    [ "scissor", "struct_d_e_1_1_s_s_pot_lights_shader_data.html#a606053da18f71fdbc16bc7c0e56062fd", null ],
    [ "shadowCaster", "struct_d_e_1_1_s_s_pot_lights_shader_data.html#aba0cb9bdcb1e2d9e2bdf229e0cd40edf", null ],
    [ "visibilityOrder", "struct_d_e_1_1_s_s_pot_lights_shader_data.html#ac77a3f9e0bc4472dcf1372458264900a", null ]
];