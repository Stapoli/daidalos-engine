var struct_d_e_1_1_s_texture2_d_filter_policy =
[
    [ "addressU", "struct_d_e_1_1_s_texture2_d_filter_policy.html#a676dec45a026e5ec81b7c8347caacd49", null ],
    [ "addressV", "struct_d_e_1_1_s_texture2_d_filter_policy.html#aae69b21026cd482896ca44bc8e641af6", null ],
    [ "magFilter", "struct_d_e_1_1_s_texture2_d_filter_policy.html#ac3ad9a2c34e9f566f0155bf5d9eec5e8", null ],
    [ "maxAnisotropy", "struct_d_e_1_1_s_texture2_d_filter_policy.html#a7045b47309b2eb9abe295451ed95368b", null ],
    [ "minFilter", "struct_d_e_1_1_s_texture2_d_filter_policy.html#a844ff6252caa6beee2f864ab31c4035b", null ],
    [ "mipFilter", "struct_d_e_1_1_s_texture2_d_filter_policy.html#a4a1b86520836da4abaa290b137eaf5d0", null ]
];