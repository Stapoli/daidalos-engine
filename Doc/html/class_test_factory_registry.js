var class_test_factory_registry =
[
    [ "TestFactoryRegistry", "class_test_factory_registry.html#a704548465fea8b52fa449845b8b42caf", null ],
    [ "~TestFactoryRegistry", "class_test_factory_registry.html#a1c0c5589eb993ad6a5b1a17d439db457", null ],
    [ "addRegistry", "class_test_factory_registry.html#abb68b347450c97ff0dfc9ea3dfe03f4c", null ],
    [ "addTestToSuite", "class_test_factory_registry.html#a39180636ddd11a499d15614a335b17d5", null ],
    [ "makeTest", "class_test_factory_registry.html#a75fd01e6d565fb0f576ed1a887655089", null ],
    [ "registerFactory", "class_test_factory_registry.html#a632c38375727ca735e2c1897bd625b99", null ],
    [ "registerFactory", "class_test_factory_registry.html#aff8d8215ec83fbb77d46706264e2f161", null ],
    [ "unregisterFactory", "class_test_factory_registry.html#afa3fb925b07eb34e9ccfab84812afc18", null ]
];