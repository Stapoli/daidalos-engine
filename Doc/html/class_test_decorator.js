var class_test_decorator =
[
    [ "TestDecorator", "class_test_decorator.html#a227e67c84146b23a37b44c24877b45d9", null ],
    [ "~TestDecorator", "class_test_decorator.html#a91a5d50dce7e822430154c2193dcaf7e", null ],
    [ "countTestCases", "class_test_decorator.html#a7d4cffe147c9febf9e3b828392a2f5b4", null ],
    [ "doGetChildTestAt", "class_test_decorator.html#a927deddcf5725ba159a0c677570ff8d8", null ],
    [ "getChildTestCount", "class_test_decorator.html#a8c3e4d2a31eb269a6acc0a8a464c4b4c", null ],
    [ "getName", "class_test_decorator.html#a45af5bfeed053b75bb604fc501064eb0", null ],
    [ "run", "class_test_decorator.html#a64dd70aae76f31f2e2f7b5ac84a8e829", null ],
    [ "m_test", "class_test_decorator.html#a621516914e5489ff3f874230f40b7d0d", null ]
];