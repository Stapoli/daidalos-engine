var class_d_e_1_1_c_sprite =
[
    [ "CSprite", "class_d_e_1_1_c_sprite.html#a5dd4441f656dc42bd1f639d1be4b0772", null ],
    [ "GetRotation", "class_d_e_1_1_c_sprite.html#acb31412437953787bfd5c7fcabfd9d80", null ],
    [ "Render", "class_d_e_1_1_c_sprite.html#a48fa90ee75ac0a81abbf08ebed7e9d11", null ],
    [ "SetBackgroundColor", "class_d_e_1_1_c_sprite.html#acbd04adfac6d6b8ffa8ce26f08de3f5e", null ],
    [ "SetRotation", "class_d_e_1_1_c_sprite.html#ab45716c3f028c7e7ce02d8da9bf0693b", null ],
    [ "SetTexture", "class_d_e_1_1_c_sprite.html#a02692fc8b6f296d7fbb3424e4143c82f", null ],
    [ "Update", "class_d_e_1_1_c_sprite.html#ab447aa67a04a2b545d93d307e0a64d5e", null ]
];