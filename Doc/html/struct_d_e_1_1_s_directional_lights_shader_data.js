var struct_d_e_1_1_s_directional_lights_shader_data =
[
    [ "color", "struct_d_e_1_1_s_directional_lights_shader_data.html#aa046f71ead2c2fa0fd400f206f43e1d2", null ],
    [ "direction", "struct_d_e_1_1_s_directional_lights_shader_data.html#a75fd35f729904233656d11d8e7365147", null ],
    [ "enabled", "struct_d_e_1_1_s_directional_lights_shader_data.html#a00d48c33f782de8da1c74f37dbe41794", null ],
    [ "node", "struct_d_e_1_1_s_directional_lights_shader_data.html#a494e9aecec94151df4dfc9c6bde756fd", null ],
    [ "numberOfLights", "struct_d_e_1_1_s_directional_lights_shader_data.html#a159992c965e86ca959bfad34eef5458e", null ],
    [ "position", "struct_d_e_1_1_s_directional_lights_shader_data.html#aed94cb6e5bbc06bd3672376c5360d776", null ],
    [ "shadowCaster", "struct_d_e_1_1_s_directional_lights_shader_data.html#ad7892f623778817580ef478b59835e50", null ]
];