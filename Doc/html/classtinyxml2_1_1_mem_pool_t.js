var classtinyxml2_1_1_mem_pool_t =
[
    [ "COUNT", "classtinyxml2_1_1_mem_pool_t.html#aec328311b169ceaa066a4b4bbc38846da4eeedbaa09fc9968120af6190e9e0988", null ],
    [ "MemPoolT", "classtinyxml2_1_1_mem_pool_t.html#a8a69a269ea72e292dde65309528ef64b", null ],
    [ "~MemPoolT", "classtinyxml2_1_1_mem_pool_t.html#ad6bb8346ad5b9a34f8f0051da5e3ed3f", null ],
    [ "Alloc", "classtinyxml2_1_1_mem_pool_t.html#aa9d785a48ffe6ea1be679bab13464486", null ],
    [ "CurrentAllocs", "classtinyxml2_1_1_mem_pool_t.html#acafff0b90381d30902bbae54eb1c0b28", null ],
    [ "Free", "classtinyxml2_1_1_mem_pool_t.html#a4f1a0c434e9e3d7391e5c16ed4ee8c70", null ],
    [ "ItemSize", "classtinyxml2_1_1_mem_pool_t.html#a0d808a013738c549fe675e0c125bcad8", null ],
    [ "SetTracked", "classtinyxml2_1_1_mem_pool_t.html#a7798932414916199a1bc0f9c3f368521", null ],
    [ "Trace", "classtinyxml2_1_1_mem_pool_t.html#a0bc596f271e0f139822c534238b3f244", null ],
    [ "Untracked", "classtinyxml2_1_1_mem_pool_t.html#a365d95526f22abe08e306d63e30c0b51", null ]
];