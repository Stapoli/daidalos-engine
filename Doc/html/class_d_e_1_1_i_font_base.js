var class_d_e_1_1_i_font_base =
[
    [ "IFontBase", "class_d_e_1_1_i_font_base.html#a11a0077069872c14f1ae1a063349681c", null ],
    [ "GetBase", "class_d_e_1_1_i_font_base.html#a6e38e83bea13895f7155119eaa562130", null ],
    [ "GetCharDescriptor", "class_d_e_1_1_i_font_base.html#a1166c3eb2a43e444b8e4b475cba68620", null ],
    [ "GetFontName", "class_d_e_1_1_i_font_base.html#a4ce8078c0457e8dc02a61a868d8e4562", null ],
    [ "GetLineHeight", "class_d_e_1_1_i_font_base.html#ab33e1c6fd0265ece36b22b8af1f65fbc", null ],
    [ "GetSize", "class_d_e_1_1_i_font_base.html#aa0e42236bbb16d9f05b283c0a7ed2205", null ],
    [ "GetTexture", "class_d_e_1_1_i_font_base.html#aab40e355559873d2809b805fcf8300ed", null ],
    [ "IsBold", "class_d_e_1_1_i_font_base.html#a14f46794548dd8959d880e72f0aa5403", null ],
    [ "IsItalic", "class_d_e_1_1_i_font_base.html#a8d100f76033c26bc3e94da0639ace7d1", null ]
];