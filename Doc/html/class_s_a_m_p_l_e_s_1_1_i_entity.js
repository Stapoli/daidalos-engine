var class_s_a_m_p_l_e_s_1_1_i_entity =
[
    [ "AddTarget", "class_s_a_m_p_l_e_s_1_1_i_entity.html#a50b7e7e6eaf8be357f2064b215641027", null ],
    [ "CanDoAction", "class_s_a_m_p_l_e_s_1_1_i_entity.html#a8295be9e8d57ab495f23c263ac9d74b0", null ],
    [ "DoAction", "class_s_a_m_p_l_e_s_1_1_i_entity.html#a9d2effe5915a2d2e903786ab090c2ee6", null ],
    [ "GetType", "class_s_a_m_p_l_e_s_1_1_i_entity.html#a4ea2dfeb0d1168c4214e8ac222ac9830", null ],
    [ "RemoveTarget", "class_s_a_m_p_l_e_s_1_1_i_entity.html#a772543b451795a374dfac92adb22e15b", null ],
    [ "Update", "class_s_a_m_p_l_e_s_1_1_i_entity.html#ad282ad4ba26a7bd85968680715005266", null ],
    [ "m_targetsEntity", "class_s_a_m_p_l_e_s_1_1_i_entity.html#a0dc18054d3e5a0eba58a13d67f626312", null ],
    [ "m_type", "class_s_a_m_p_l_e_s_1_1_i_entity.html#a04187cfb5dd8eedf53d49ce7b14a00fa", null ]
];