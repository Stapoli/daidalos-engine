var class_quaternion_test =
[
    [ "setUp", "class_quaternion_test.html#a6415cb27d8ffb600a2754485333222f8", null ],
    [ "tearDown", "class_quaternion_test.html#a9aa45beb5c1a05b773437568e6f6c75d", null ],
    [ "TestConstructors", "class_quaternion_test.html#a008b95c0d0c591c44087c81e579c45ac", null ],
    [ "TestDotProduct", "class_quaternion_test.html#a93e028d9a13b1717562d3247e72dbdfd", null ],
    [ "TestExp", "class_quaternion_test.html#a71e1612a69f23456bfad9358e28962b2", null ],
    [ "TestInverse", "class_quaternion_test.html#ad69e1f683a2855424e1b9769a896438f", null ],
    [ "TestLength", "class_quaternion_test.html#affbb2f97ec76c96f761212a09728e3cc", null ],
    [ "TestLerp", "class_quaternion_test.html#ac3efa467d01c574b5a70bef7dd454b7d", null ],
    [ "TestLog", "class_quaternion_test.html#a88a3317ede39625fea1e99f5094f0f24", null ],
    [ "TestNormalize", "class_quaternion_test.html#aa0db33a6f9ec6855e7fd69d9a8ed4bc7", null ],
    [ "TestOperators", "class_quaternion_test.html#a9d43b6f9e9b73a036e76610eeac9af4b", null ],
    [ "TestPower", "class_quaternion_test.html#a2d0449ae784bf7a38c75b87bc632bedd", null ],
    [ "TestSlerp", "class_quaternion_test.html#a6eb5027280f06682c553b3248c221b78", null ],
    [ "TestYawPitchRoll", "class_quaternion_test.html#a9051438a677753aaa4007437ebc9eed1", null ]
];