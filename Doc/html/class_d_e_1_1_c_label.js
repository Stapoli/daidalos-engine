var class_d_e_1_1_c_label =
[
    [ "CLabel", "class_d_e_1_1_c_label.html#a1ed674ab1720a72d26d1a12a1981ccb9", null ],
    [ "GetColor", "class_d_e_1_1_c_label.html#a1d30df9cabadabed299bf1e24715ea58", null ],
    [ "GetFontName", "class_d_e_1_1_c_label.html#aafeee4bb9116896143be2553be9ce6eb", null ],
    [ "GetHorizontalAlignment", "class_d_e_1_1_c_label.html#aa6255ef2471c6742be91c5c2da131bdf", null ],
    [ "GetSize", "class_d_e_1_1_c_label.html#a4bfbf12e8ee3c9b0e94a48c638cc1488", null ],
    [ "GetText", "class_d_e_1_1_c_label.html#a493a59c35837e1feb77be0d4bd01e6fd", null ],
    [ "GetVerticalAlignment", "class_d_e_1_1_c_label.html#ac65912ba0ed3581c0f943d944f8817c7", null ],
    [ "Render", "class_d_e_1_1_c_label.html#a47e49ed8d2915dacb9c7c872e5f9b3cd", null ],
    [ "SetColor", "class_d_e_1_1_c_label.html#a50d180ab990f8bbec7e8585e86547482", null ],
    [ "SetFont", "class_d_e_1_1_c_label.html#a5a75c7de99fcf0466959022ac3bf040d", null ],
    [ "SetHorizontalAlignment", "class_d_e_1_1_c_label.html#a81f1e8cac599886776ca4606105bd755", null ],
    [ "SetSize", "class_d_e_1_1_c_label.html#ac616c3e122c64b1f3a11bbc6db647b09", null ],
    [ "SetText", "class_d_e_1_1_c_label.html#acea5dfd11e1b28ab86a7bf693c583b6e", null ],
    [ "SetVerticalAlignment", "class_d_e_1_1_c_label.html#a81ff71731c328780c7db9a9b0f93bc1b", null ],
    [ "Update", "class_d_e_1_1_c_label.html#ab2c64382f96c75d59147a03aa848d35e", null ]
];