var class_d_e_1_1_i_resource =
[
    [ "IResource", "class_d_e_1_1_i_resource.html#a38744f29337a6276ead7c7836d4c6042", null ],
    [ "IResource", "class_d_e_1_1_i_resource.html#a9636714c8acecdb8fbd212344c6eceaf", null ],
    [ "~IResource", "class_d_e_1_1_i_resource.html#afe2eeaa87ddcb56b9561c7e114e881a3", null ],
    [ "AddReference", "class_d_e_1_1_i_resource.html#afe38669a8350aebe3d2bfa7ec144f853", null ],
    [ "GetName", "class_d_e_1_1_i_resource.html#a888aa26e6588fe02313cfb9c5cdd1d54", null ],
    [ "OnDeviceLost", "class_d_e_1_1_i_resource.html#a5f269b747940884a68c0f2cb55d4b5e5", null ],
    [ "OnDeviceReset", "class_d_e_1_1_i_resource.html#a5bb36517b72544144fe6ec6b18e77907", null ],
    [ "Release", "class_d_e_1_1_i_resource.html#a327cb6435407e1083c088c7a8c2f3aad", null ],
    [ "m_name", "class_d_e_1_1_i_resource.html#aa9a3fbb5fa26e60fcd97bdd50bd64818", null ],
    [ "m_referenceCount", "class_d_e_1_1_i_resource.html#ad75ff063c5cf66d54fe2e6ccaf6d2a7b", null ]
];