var class_synchronized_object =
[
    [ "ExclusiveZone", "class_synchronized_object_1_1_exclusive_zone.html", "class_synchronized_object_1_1_exclusive_zone" ],
    [ "SynchronizationObject", "class_synchronized_object_1_1_synchronization_object.html", "class_synchronized_object_1_1_synchronization_object" ],
    [ "SynchronizedObject", "class_synchronized_object.html#a9ae8017720af72aac92964f23b55d6f1", null ],
    [ "~SynchronizedObject", "class_synchronized_object.html#ae2c081d2e252b35a49edbf887579cfe0", null ],
    [ "setSynchronizationObject", "class_synchronized_object.html#a72f0856ba79569fea9907916e085c555", null ],
    [ "m_syncObject", "class_synchronized_object.html#a51605ed5ba10c06e795f21a98ea5ef37", null ]
];