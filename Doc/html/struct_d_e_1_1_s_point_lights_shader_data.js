var struct_d_e_1_1_s_point_lights_shader_data =
[
    [ "colorFalloff", "struct_d_e_1_1_s_point_lights_shader_data.html#a1c7bd857bef24c0863885b913e78fe69", null ],
    [ "distanceToCamera", "struct_d_e_1_1_s_point_lights_shader_data.html#a8728ed122f97fc7d9348c42930ac5520", null ],
    [ "enabled", "struct_d_e_1_1_s_point_lights_shader_data.html#a81bc7bdfa1e095dd54cfaa0baa5e4072", null ],
    [ "numberOfLights", "struct_d_e_1_1_s_point_lights_shader_data.html#a58442af3f2d46382815b90cf65903237", null ],
    [ "positionRange", "struct_d_e_1_1_s_point_lights_shader_data.html#aca9e7a9191eaabdc2151bcf985be4ddb", null ],
    [ "scissor", "struct_d_e_1_1_s_point_lights_shader_data.html#aca0bc1c878ccaa84fd83e9fbc05368c5", null ],
    [ "shadowCaster", "struct_d_e_1_1_s_point_lights_shader_data.html#a27b3c30245ffb56d3b20013fac125eb1", null ],
    [ "visibilityOrder", "struct_d_e_1_1_s_point_lights_shader_data.html#a31245834f64e3d771435068616cbb22f", null ]
];