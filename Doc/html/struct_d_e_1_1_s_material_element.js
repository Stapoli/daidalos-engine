var struct_d_e_1_1_s_material_element =
[
    [ "m_kAmbient", "struct_d_e_1_1_s_material_element.html#a83cca2ab5b6f1a87cdf33a8bd2afbf1b", null ],
    [ "m_kDiffuse", "struct_d_e_1_1_s_material_element.html#ae1d6f55912394b88fe6d24117be2e4a0", null ],
    [ "m_kEmission", "struct_d_e_1_1_s_material_element.html#a3824764bcaa7fa3f2508c95192b47097", null ],
    [ "m_kSpecular", "struct_d_e_1_1_s_material_element.html#a24acda277a8e9f41e491e47ea6cbbade", null ],
    [ "m_ns", "struct_d_e_1_1_s_material_element.html#a9d52deedaff5e41d6ceebce701e3f77a", null ]
];