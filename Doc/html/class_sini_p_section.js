var class_sini_p_section =
[
    [ "SiniPSection", "class_sini_p_section.html#a0fe41ae65340b5a9d8f5be51208151ee", null ],
    [ "SiniPSection", "class_sini_p_section.html#a3743b355b9f229e31ed8311d99ef6536", null ],
    [ "~SiniPSection", "class_sini_p_section.html#a0bae199a4360e2a2e7dfc79e2070de3e", null ],
    [ "AddElement", "class_sini_p_section.html#aa8299d5315eff06b9b6417db592752f4", null ],
    [ "Clear", "class_sini_p_section.html#a7e275e5fbf988af9b08815cd850e209c", null ],
    [ "Copy", "class_sini_p_section.html#a64b27e29a5eb915c4dcf551c5a1fb0c9", null ],
    [ "GetElements", "class_sini_p_section.html#a51688142429d5fdf8c279c154cc2c648", null ],
    [ "GetName", "class_sini_p_section.html#a644e31f1bdc7572ef911b42873509293", null ],
    [ "GetProperty", "class_sini_p_section.html#a1b4163e35a653210fa45aaeb8cb0d847", null ],
    [ "operator=", "class_sini_p_section.html#a653e40d3c9e3f4e895566a74456f70d3", null ],
    [ "RemoveAllComments", "class_sini_p_section.html#abb89de4ac05a7266c128645ff6f97a8f", null ],
    [ "SetName", "class_sini_p_section.html#aed784e59dbc06146afa81a1cbe6db467", null ],
    [ "ToString", "class_sini_p_section.html#ade43555f02491c886c84c5ab978bc461", null ],
    [ "m_data", "class_sini_p_section.html#a5af3ba4a5db45be0eec65c8be6a7952b", null ],
    [ "m_name", "class_sini_p_section.html#af76de6df32a38585434d95fb0d98a8e1", null ]
];