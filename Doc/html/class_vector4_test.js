var class_vector4_test =
[
    [ "setUp", "class_vector4_test.html#add2d48ac23e27baad747c8153e1988d7", null ],
    [ "tearDown", "class_vector4_test.html#a42522bbbd73d38801e28425560745189", null ],
    [ "TestConstructors", "class_vector4_test.html#ac75aaad4592273113f07263c1f3a0ff9", null ],
    [ "TestGetFrom", "class_vector4_test.html#a385cd18f97c67774208a71bbc2014c29", null ],
    [ "TestLength", "class_vector4_test.html#af20e6961f439992c01a21bcaccc336ce", null ],
    [ "TestNormalize", "class_vector4_test.html#affce966f6b8aa13dc85a007f284852e8", null ],
    [ "TestOperators", "class_vector4_test.html#a5841b30392ef2e21f22f716842d93b24", null ]
];