var class_plug_in_manager =
[
    [ "PlugInInfo", "struct_plug_in_manager_1_1_plug_in_info.html", "struct_plug_in_manager_1_1_plug_in_info" ],
    [ "PlugInManager", "class_plug_in_manager.html#a7789a7e258e750bd14267d20cac7d288", null ],
    [ "~PlugInManager", "class_plug_in_manager.html#a093797f2d979b22fcf8df2230985b246", null ],
    [ "addListener", "class_plug_in_manager.html#a3c51a256e3d6ee3bf6fa6c98d74053d1", null ],
    [ "addXmlOutputterHooks", "class_plug_in_manager.html#a4a9c64e0ac3f762b100b993d7e33f889", null ],
    [ "load", "class_plug_in_manager.html#aa617ef22c19ff90d7a56cd048c975afa", null ],
    [ "removeListener", "class_plug_in_manager.html#aea9b8b61e256169b823516868e1d8ad0", null ],
    [ "removeXmlOutputterHooks", "class_plug_in_manager.html#a0fe59f82fd430ea57159a6ffad9a4035", null ],
    [ "unload", "class_plug_in_manager.html#a5366b1e0e2e96e84d09e26893c00c4bf", null ],
    [ "unload", "class_plug_in_manager.html#afc1fa045afaac73cd44f69839056f1e1", null ]
];