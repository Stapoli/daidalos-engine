var class_xml_element =
[
    [ "XmlElement", "class_xml_element.html#a94d6c15e996994d62316b198f369d675", null ],
    [ "XmlElement", "class_xml_element.html#a879b90d96dc87b6f7ebb9c67ba92cb73", null ],
    [ "~XmlElement", "class_xml_element.html#ac862c3db543830858fba97c6196afd6b", null ],
    [ "addAttribute", "class_xml_element.html#a14387be9ca6d014a38e95a7aa98f86c5", null ],
    [ "addAttribute", "class_xml_element.html#aec69fb49cf9e563cff9858ddc818f055", null ],
    [ "addElement", "class_xml_element.html#a444321a1de2a7f52994c53f9e6e0d942", null ],
    [ "content", "class_xml_element.html#a53d24b7838e5e76daeb2ca2d14ce086d", null ],
    [ "elementAt", "class_xml_element.html#aa4279da43f6fb45f059ab1a1a15d2e69", null ],
    [ "elementCount", "class_xml_element.html#ada7e66977cd0f62bae9dcc3f96cb95e5", null ],
    [ "elementFor", "class_xml_element.html#a0fd5bcf32bf74bdd341e97bcf0818e3d", null ],
    [ "name", "class_xml_element.html#ac3a480c2b65a9b6fa529f32f7fce33b9", null ],
    [ "setContent", "class_xml_element.html#aaf32abf7cdaf31b8896f52a7859d9826", null ],
    [ "setContent", "class_xml_element.html#ae8d473f1d193157158ae36e12929d387", null ],
    [ "setName", "class_xml_element.html#a0ed6ad08fd972865cca13ae2594fedda", null ],
    [ "toString", "class_xml_element.html#ac61eb02df41acf8de22d50898ccfe371", null ]
];