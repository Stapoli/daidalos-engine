var class_test_runner_1_1_wrapping_suite =
[
    [ "WrappingSuite", "class_test_runner_1_1_wrapping_suite.html#a3edce0cce0e3fdc996994d3dfe8f35fb", null ],
    [ "doGetChildTestAt", "class_test_runner_1_1_wrapping_suite.html#a4f6a549ccfe6a571c328aa4f245fcd89", null ],
    [ "getChildTestCount", "class_test_runner_1_1_wrapping_suite.html#a5ffc3769e32dcde719bcf61191fb3c53", null ],
    [ "getName", "class_test_runner_1_1_wrapping_suite.html#a0015e42bd741cbf8ca64e754cd93e7c6", null ],
    [ "getUniqueChildTest", "class_test_runner_1_1_wrapping_suite.html#a45c2ef4cedcaf0c0ded069f731ef3f5a", null ],
    [ "hasOnlyOneTest", "class_test_runner_1_1_wrapping_suite.html#ae25829c384e39b6ebb11bb7369058971", null ],
    [ "run", "class_test_runner_1_1_wrapping_suite.html#aa486bfea64b60206ac1d8fbb006f2f64", null ]
];