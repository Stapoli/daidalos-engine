var class_s_a_m_p_l_e_s_1_1_i_player =
[
    [ "GetArmor", "class_s_a_m_p_l_e_s_1_1_i_player.html#a7d9b0db43ab27476224f71f2c2d3f31f", null ],
    [ "GetCurrentWeaponMagazineAmmo", "class_s_a_m_p_l_e_s_1_1_i_player.html#a62e52b928e183b33476c4e6b442d6a20", null ],
    [ "GetCurrentWeaponStockAmmo", "class_s_a_m_p_l_e_s_1_1_i_player.html#abd6f7aab08cf3c8dcb63f74deb69ea13", null ],
    [ "GetGravity", "class_s_a_m_p_l_e_s_1_1_i_player.html#a51606aa978c7cad2d6a4e05a4407366f", null ],
    [ "GetHealth", "class_s_a_m_p_l_e_s_1_1_i_player.html#a0da7944fb1be8e59c87f16aa85f8008a", null ],
    [ "GetObjectValue", "class_s_a_m_p_l_e_s_1_1_i_player.html#a8c17d39f9530a6bb18866e9fbd931657", null ],
    [ "GetPosition", "class_s_a_m_p_l_e_s_1_1_i_player.html#aec1dae3fdf39f9c216761be4d2ad7bfe", null ],
    [ "GetShape", "class_s_a_m_p_l_e_s_1_1_i_player.html#ab643d519d4a65a6eb256b83618e57789", null ],
    [ "IsInteracting", "class_s_a_m_p_l_e_s_1_1_i_player.html#acde1ad9f4d76f72ab9fd85f2e2a3f103", null ],
    [ "IsShapeVisible", "class_s_a_m_p_l_e_s_1_1_i_player.html#abce94010caf56a6d3b64c03203a3ade4", null ],
    [ "SetArmor", "class_s_a_m_p_l_e_s_1_1_i_player.html#ac5b1eb3c2a28e015164d61032947d2de", null ],
    [ "SetHealth", "class_s_a_m_p_l_e_s_1_1_i_player.html#aa07ac85ab99870a3894385ef69c5414f", null ],
    [ "SetObject", "class_s_a_m_p_l_e_s_1_1_i_player.html#a943840f0281e32b9580c32c43f03e043", null ],
    [ "SetPosition", "class_s_a_m_p_l_e_s_1_1_i_player.html#a5e3cdb44adca97663ca158434899cd6f", null ],
    [ "Update", "class_s_a_m_p_l_e_s_1_1_i_player.html#a6c27ec0e885077f03a0ef117bb4feb1a", null ]
];