/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __CCONFIGURATIONMANAGER_HPP
#define __CCONFIGURATIONMANAGER_HPP

#include <map>
#include <SiniP/SiniP.h>
#include "../Core/CSingletonManager.hpp"

#define APPLICATION_OPTION_SECTION_GENERAL "general"
#define APPLICATION_OPTION_SECTION_LIGHTING "lighting"
#define APPLICATION_OPTION_SECTION_GAMEPAD "gamepad"
#define APPLICATION_OPTION_SECTION_MOUSE "mouse"
#define APPLICATION_OPTION_SECTION_SOUND "sound"

#define APPLICATION_OPTION_GENERAL_RENDERER "renderer"
#define APPLICATION_OPTION_GENERAL_RESOLUTION_X "resolution_x"
#define APPLICATION_OPTION_GENERAL_RESOLUTION_Y "resolution_y"
#define APPLICATION_OPTION_GENERAL_INTERNAL_RESOLUTION_X "internal_resolution_x"
#define APPLICATION_OPTION_GENERAL_INTERNAL_RESOLUTION_Y "internal_resolution_y"
#define APPLICATION_OPTION_GENERAL_FULLSCREEN "fullscreen"
#define APPLICATION_OPTION_GENERAL_SYNCHRO "synchro"
#define APPLICATION_OPTION_GENERAL_ANISOTROPIC "anisotropic"
#define APPLICATION_OPTION_GENERAL_ANTIALIASING "antialiasing"
#define APPLICATION_OPTION_GENERAL_DEBUG "debug"
#define APPLICATION_OPTION_GENERAL_FILL_MODE "fill_mode"
#define APPLICATION_OPTION_GENERAL_SHOW_FPS "show_fps"
#define APPLICATION_OPTION_GENERAL_BRIGHTNESS "brightness"
#define APPLICATION_OPTION_GENERAL_BACKBUFFERS "backbuffers"

#define APPLICATION_OPTION_LIGHTING_ENABLED "enable_shadows"
#define APPLICATION_OPTION_LIGHTING_SHADOW_MAP_SIZE "shadowmap_size"
#define APPLICATION_OPTION_LIGHTING_MAX_SPOTLIGHT_SOURCES "max_spotlight_sources"
#define APPLICATION_OPTION_LIGHTING_MAX_POINTLIGHT_SOURCES "max_pointlight_sources"
#define APPLICATION_OPTION_LIGHTING_MAX_DIRECTIONALLIGHT_SOURCES "max_directionallight_sources"
#define APPLICATION_OPTION_LIGHTING_MAX_POINTLIGHT_SOURCES "max_pointlight_sources"

#define APPLICATION_OPTION_GAMEPAD_DEAD_ZONE "dead_zone"
#define APPLICATION_OPTION_GAMEPAD_SMOOTH_FACTOR "smooth_factor"

#define APPLICATION_OPTION_MOUSE_SENSIBILITY "sensibility"
#define APPLICATION_OPTION_MOUSE_SMOOTHING "smoothing"

#define APPLICATION_OPTION_SOUND_MUSIC "music"
#define APPLICATION_OPTION_SOUND_ENVIRONMENT "environment"
#define APPLICATION_OPTION_SOUND_DIALOGUE "dialogue"

namespace daidalosengine {
	/**
	* Handle the ini configuration files of the engine
	*/
	class CConfigurationManager : public CSingletonManager < CConfigurationManager > {
		friend class CSingletonManager < CConfigurationManager > ;

	private:
		std::map<std::string, SiniP> m_files;

	public:
		/**
		* Destructor
		*/
		virtual ~CConfigurationManager();

		/**
		* Get the configuration
		*/
		SiniP & GetConfiguration(const std::string & filename);

	private:
		/**
		* Constructor
		*/
		CConfigurationManager();

	public:
		CConfigurationManager(const CConfigurationManager & copy) = delete;
		const CConfigurationManager & operator=(const CConfigurationManager & copy) = delete;
	};
}

#endif
