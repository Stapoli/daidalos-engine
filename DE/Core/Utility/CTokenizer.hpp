/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __CTOKENIZER_HPP
#define __CTOKENIZER_HPP

#define TOKENIZER_COMMENT "//"

#include <vector>
#include <string>

namespace daidalosengine {
	typedef std::vector<std::string> TToken;

	/**
	* CTokenizer class.
	* Manages the files parsing implementation.
	* The goal of this class is to create a token table that will be used to read configuration files.
	*/
	class CTokenizer {
	private:
		std::vector<std::string> m_tokens;
		std::vector<std::string> m_buffer;

	public:
		/**
		* Constructor.
		*/
		CTokenizer();

		/**
		* Constructor.
		* @param filename The file to tokenize.
		*/
		explicit CTokenizer(const std::string & filename);

		/**
		* Constructor.
		* @param filename The file to tokenize.
		* @param filterList The filter list that contains all the accepted keywords.
		*/
		CTokenizer(const std::string & filename, const std::vector<std::string> & filterList);

		/**
		* Destructor.
		*/
		~CTokenizer();

		/**
		* Open a file, read its content and put it in the token list.
		* @param filename The file to open.
		*/
		void OpenFile(const std::string & filename);

		/**
		* Add a line to the line list.
		* @param line The line to add.
		*/
		void AddLine(const std::string & line);

		/**
		* Clear the tokens.
		*/
		void Clear();

		/**
		* Tokenize the token list.
		* @param delimiter The delimiter that is used to split.
		* @param maxSplit The maximum split accepted for each split process.
		* @return True if the split is valid
		*/
		bool Tokenize(const std::string & delimiter, const int maxSplit = 0);

		/**
		* Count the number of 'token' tokens.
		* @param token The token to count.
		* @return The number of 'token' tokens found.
		*/
		int Count(const std::string & token) const;

		/**
		* Get the tokens.
		* @return The tokens.
		*/
		const TToken & GetTokens() const;

	private:
		/**
		* Split a token with a delimiter and push the result in the buffer.
		* @param token The token to split.
		* @param delimiter The delimiter to use.
		* @return The number of pushed tokens.
		*/
		int SplitString(const std::string & token, const std::string & delimiter);
	};
}

#endif
