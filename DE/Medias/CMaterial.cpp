/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../Core/Logger/ILogger.hpp"
#include "../Core/CResourceManager.hpp"
#include "../Core/CMediaManager.hpp"
#include "../Medias/IMaterialBase.hpp"
#include "../Medias/CMaterial.hpp"

namespace daidalosengine {
	CMaterial::CMaterial() {
		LOG_TRACE();

		this->m_material = nullptr;
	}

	CMaterial::CMaterial(const std::string & filename) {
		LOG_TRACE();

		this->m_material = nullptr;
		LoadFromFile(filename);
	}

	CMaterial::CMaterial(const CMaterial & copy) {
		LOG_TRACE();

		*this = copy;
	}

	CMaterial::~CMaterial() {
		LOG_TRACE();

		if(this->m_material != nullptr)
			CResourceManager::GetInstance()->ReleaseResource(this->m_material->GetName());
	}

	void CMaterial::LoadFromFile(const std::string & filename) {
		LOG_TRACE();

		// Release previously used resource
		if (this->m_material != nullptr) {
			CResourceManager::GetInstance()->ReleaseResource(this->m_material->GetName());
		}

		// Check the existence of the resource and load it if necessary
		auto * tmp = CResourceManager::GetInstance()->GetResource<IMaterialBase>(filename);

		if(tmp == nullptr) {
			this->m_material = CMediaManager::GetInstance()->LoadMediaFromFile<IMaterialBase>(filename);
			CResourceManager::GetInstance()->AddResource(filename, this->m_material);
		} else
			this->m_material = tmp;
	}

	CMaterial & CMaterial::operator=(const CMaterial & copy) {
		LOG_TRACE();

		if (this != &copy) {
			if (this->m_material != nullptr) {
				CResourceManager::GetInstance()->ReleaseResource(this->m_material->GetName());
			}

			this->m_material = copy.GetMaterial();

			if (this->m_material != nullptr) {
				this->m_material->AddReference();
			}
		}

		return *this;
	}

	IMaterialBase * CMaterial::GetMaterial() const {
		LOG_TRACE();

		return this->m_material;
	}
}
