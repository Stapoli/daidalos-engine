/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../Scene/3D/I3DScene.hpp"
#include "../Core/Core.hpp"
#include "../Core/Utility/CTokenizer.hpp"
#include "../Core/Utility/CVector4.hpp"
#include "../Core/Memory/CExceptionLoad.hpp"
#include "../Core/Logger/ILogger.hpp"
#include "../Scene/3D/CProjectionCameraSceneNode.hpp"
#include "../Scene/3D/CSpotLightSceneNode.hpp"
#include "../Scene/3D/CPointLightSceneNode.hpp"
#include "../Scene/3D/CDirectionalLightSceneNode.hpp"
#include "../Scene/3D/CStaticMeshSceneNode.hpp"
#include "../Scene/3D/CAnimatedMeshSceneNode.hpp"
#include "../Scene/3D/CSoundSceneNode.hpp"
#include "../Scene/3D/CEmptySceneNode.hpp"
#include "../Scene/3D/CParticleEmitterSceneNode.hpp"
#include "../Scene/3D/CPlane3D.hpp"
#include "../Medias/CTexture2D.hpp"
#include "../Medias/CMaterial.hpp"
#include "../Loaders/CSceneLoader.hpp"

namespace daidalosengine {
	CSceneLoader::CSceneLoader() {
		LOG_TRACE();

		// Nodes default configuration file
		this->m_nodesConf = CConfigurationManager::GetInstance()->GetConfiguration(APPLICATION_NODES_CONFIGURATION);
		this->m_materialsConf = CConfigurationManager::GetInstance()->GetConfiguration(APPLICATION_MATERIALS_CONFIGURATION);
		this->m_structure = SCENE_NODE_ROOT_STRUCTURE_TYPE_BSP;
	}

	CSceneLoader::~CSceneLoader() {
		LOG_TRACE();
	}

	I3DScene * CSceneLoader::LoadFromFile(const std::string & filename) {
		LOG_TRACE();

		auto* scene = new I3DScene(filename);
		this->m_filename = filename;
		ISceneNode * root = nullptr;

		tinyxml2::XMLDocument doc;
		const int loadOk = doc.LoadFile(filename.c_str());
		if(loadOk == tinyxml2::XML_NO_ERROR) {
			tinyxml2::XMLHandle hDoc(&doc);

			auto tmpInt = 0;
			hDoc.FirstChildElement("scene").ToElement()->QueryIntAttribute("structure", &tmpInt);
			this->m_structure = static_cast<ESceneNodeRootStructureType>(tmpInt);

			// Read the particle system declaration
			auto* element = hDoc.FirstChildElement("scene").FirstChildElement("particleSystem").ToElement();
			if(element != nullptr) {
				const auto handle = tinyxml2::XMLHandle(element);
				LoadParticleSystem(scene, handle);
			} else {
				LoadDefaultParticleSystem(scene);
			}

			// Read the structure nodes
			element = hDoc.FirstChildElement("scene").FirstChildElement("node").ToElement();
			for(auto* elementIt = element; elementIt != nullptr; elementIt = elementIt->NextSiblingElement()) {
				LoadNode(scene, root, tinyxml2::XMLHandle(elementIt));
			}
		} else {
			LOG_ERROR("Unable to load the scene file '" << this->m_filename << "' (" << doc.GetErrorStr1() << ")");
			ILogger::Kill();

			throw CExceptionLoad(this->m_filename);
		}

		scene->SetRoot(root);

		return scene;
	}

	void CSceneLoader::SaveToFile(I3DScene * object, const std::string & filename) {
		LOG_TRACE();

		this->m_structure = SCENE_NODE_ROOT_STRUCTURE_TYPE_BSP;

		tinyxml2::XMLDocument document;

		// Create the root element
		auto* sceneElement = document.NewElement("scene");
		sceneElement->SetAttribute("structure", SCENE_NODE_ROOT_STRUCTURE_TYPE_BSP);
		document.InsertEndChild(sceneElement);

		// Create the particle system element
		SaveParticleSystem(object, document, sceneElement);

		// Save the nodes
		SaveNode(object->GetRoot(), document, sceneElement);

		// Save the xml
		document.SaveFile(filename.c_str());

		// Update the scene filename
		object->SetFilename(filename);
	}

	void CSceneLoader::LoadNode(I3DScene * scene, ISceneNode *& parent, tinyxml2::XMLHandle handle) {
		LOG_TRACE();

		// Global data variables
		ISceneNode * node = nullptr;
		int type = -1;
		handle.ToElement()->QueryIntAttribute("type", &type);

		std::string name = "undefined";
		if(handle.ToElement()->Attribute("name")) {
			name = handle.ToElement()->Attribute("name");
		}

		int id = 0;
		handle.ToElement()->QueryIntAttribute("id", &id);

		switch(type) {
		case SCENE_NODE_ROOT:

			switch(this->m_structure) {
				// BSP structure
			case SCENE_NODE_ROOT_STRUCTURE_TYPE_BSP:
				LoadBSPStructure(scene, parent, handle, node);
				if(parent == nullptr) {
					parent = node;
				}
				break;

				// Simple structure
			default:
				LoadSimpleStructure(scene, parent, handle, node);
				if (parent == nullptr) {
					parent = node;
				}
				break;
			}

			break;

		case SCENE_NODE_CAMERA:			
			LoadCameraNode(scene, parent, handle, node, name);
			break;

		case SCENE_NODE_SPOTLIGHT:
			LoadSpotLightNode(scene, parent, handle, node, name);;
			break;

		case SCENE_NODE_POINTLIGHT:
			LoadPointLightNode(scene, parent, handle, node, name);
			break;

		case SCENE_NODE_DIRECTIONALLIGHT:
			LoadDirectionalLightNode(scene, parent, handle, node, name);
			break;

		case SCENE_NODE_STATICMESH:
			LoadStaticMeshNode(scene, parent, handle, node, name);
			break;

		case SCENE_NODE_ANIMATEDMESH:
			LoadAnimatedMeshNode(scene, parent, handle, node, name);
			break;

		case SCENE_NODE_SOUND:
			LoadSoundNode(scene, parent, handle, node, name);
			break;

		case SCENE_NODE_EMPTY:
			LoadEmptyNode(scene, parent, handle, node, name);
			break;

		case SCENE_NODE_PARTICLE_EMITTER:
			LoadParticleEmitterNode(scene, parent, handle, node, name);
			break;

		default:
			// Invalid or missing type
			LOG_WARNING("Invalid or missing node attribute 'type' in '" << this->m_filename << "'");
			break;
		}

		// If a node has been successfully created
		if(node != nullptr) {
			// Set the id
			node->SetId(id);

			// Get the node attributes
			const auto attributes = GetNodeAttributes(handle);

			// Add the attributes
			node->SetAttributes(attributes);

			// Add the node to the parent
			if(parent != node) {
				parent->AddChild(node);
			}

			// Check for attached nodes
			auto* child = handle.FirstChildElement("node").ToElement();
			for(auto* childIt = child; childIt != nullptr; childIt = childIt->NextSiblingElement()) {
				// Read only nodes
				if(std::string(childIt->Name()) == "node") {
					LoadNode(scene, node, tinyxml2::XMLHandle(childIt));
				}
			}
		}
	}

	void CSceneLoader::LoadBSPStructure(I3DScene * scene, ISceneNode *& parent, tinyxml2::XMLHandle handle, ISceneNode *& node) {
		LOG_TRACE();

		std::vector<SBSPBrushDefinition> brushDefinition;
		node = new CBSPSceneNode(scene);
		std::string skyName;
		CVector3F ambientColor;
		CVector3F skyAmbientColor;

		// Get the ambient color
		if (handle.ToElement()->Attribute("ambientColor") != nullptr) {
			ambientColor = CVector3F::GetFrom(handle.ToElement()->Attribute("ambientColor"));
		}
		dynamic_cast<CBSPSceneNode*>(node)->SetAmbientColor(CVector4F(ambientColor.x, ambientColor.y, ambientColor.z, 1));

		// Get the sky name
		if (handle.ToElement()->Attribute("skyName") != nullptr) {
			skyName = handle.ToElement()->Attribute("skyName");
		}
		dynamic_cast<CBSPSceneNode*>(node)->SetSkyName(skyName);

		// Get the sky ambient color
		if (handle.ToElement()->Attribute("skyAmbientColor") != nullptr) {
			skyAmbientColor = CVector3F::GetFrom(handle.ToElement()->Attribute("skyAmbientColor"));
		}
		dynamic_cast<CBSPSceneNode*>(node)->SetSkyAmbientColor(CVector4F(skyAmbientColor.x, skyAmbientColor.y, skyAmbientColor.z, 1));

		// Read the bsp geometry
		auto* child = handle.FirstChildElement("multi_brush").ToElement();
		for(auto* childIt = child; childIt != nullptr; childIt = childIt->NextSiblingElement()) {
			// A brush created with a bunch of static models
			if(std::string(childIt->Name()) == "multi_brush") {
				tinyxml2::XMLHandle bspHandle(childIt);
				auto* child2 = bspHandle.FirstChildElement("brush").ToElement();
				SBSPBrushDefinition brush;

				// Default values
				brush.id = SCENE_DEFAULT_ID;
				brush.collision = true;

				// Get the brush options
				bspHandle.ToElement()->QueryBoolAttribute("collisions", &brush.collision);
				bspHandle.ToElement()->QueryIntAttribute("id", &brush.id);

				for(auto* child2It = child2; child2It != nullptr; child2It = child2It->NextSiblingElement()) {
					if (std::string(child2It->Name()) == "brush") {
						AddBSPBrushData(tinyxml2::XMLHandle(child2It), brush);
					}
				}

				if(!brush.geometry.empty()) {
					brushDefinition.push_back(brush);
				}
			}
		}

		child = handle.FirstChildElement("brush").ToElement();
		for (auto* childIt = child; childIt != nullptr; childIt = childIt->NextSiblingElement()) {
			// A simple brush
			if(std::string(childIt->Name()) == "brush") {
				tinyxml2::XMLHandle bspHandle(childIt);
				SBSPBrushDefinition brush;

				// Default values
				brush.id = SCENE_DEFAULT_ID;
				brush.collision = true;

				// Get the brush options
				bspHandle.ToElement()->QueryBoolAttribute("collisions", &brush.collision);
				bspHandle.ToElement()->QueryIntAttribute("id", &brush.id);

				AddBSPBrushData(bspHandle, brush);
				brushDefinition.push_back(brush);
			}
		}

		// Check for bounding box declared in nodes
		child = handle.FirstChildElement("node").ToElement();
		for (auto* childIt = child; childIt != nullptr; childIt = childIt->NextSiblingElement()) {
			// A simple brush
			if(std::string(childIt->Name()) == "node") {
				tinyxml2::XMLHandle bspHandle(childIt);
				std::string boundBoxFile;

				GetNodeData(bspHandle, "boundingBox", boundBoxFile, "");

				// Bounding box detected
				if(!boundBoxFile.empty()) {
					CVector3F position;
					CVector3F rotation;
					CVector3F scale;

					// Get the node position, rotation etc
					GetNodeData(bspHandle, "position", position, CVector3F(0, 0, 0));
					GetNodeData(bspHandle, "rotation", rotation, CVector3F(0, 0, 0));
					GetNodeData(bspHandle, "scale", scale, CVector3F(1, 1, 1));

					SBSPBrushDefinition brush;

					// Default values
					brush.id = SCENE_DEFAULT_ID;
					brush.collision = true;

					// Get the brush options
					bspHandle.ToElement()->QueryIntAttribute("id", &brush.id);

					AddBSPBoundingBoxData(boundBoxFile, position, rotation, scale, brush);
					brushDefinition.push_back(brush);
				}
			}
		}

		// Add all the geometry in the bsp scene node
		dynamic_cast<CBSPSceneNode*>(node)->LoadGeometry(brushDefinition);
	}

	void CSceneLoader::LoadSimpleStructure(I3DScene * scene, ISceneNode *& parent, tinyxml2::XMLHandle handle, ISceneNode *& node) {
		LOG_TRACE();

		std::vector<SSimpleBrush> brushes;
		node = new CSimpleSceneNode(scene);
		std::string skyName;
		CVector3F ambientColor;
		CVector3F skyAmbientColor;

		// Get the ambient color
		if (handle.ToElement()->Attribute("ambientColor") != nullptr) {
			ambientColor = CVector3F::GetFrom(handle.ToElement()->Attribute("ambientColor"));
		}
		dynamic_cast<CSimpleSceneNode*>(node)->SetAmbientColor(CVector4F(ambientColor.x, ambientColor.y, ambientColor.z, 1));

		// Get the sky name
		if (handle.ToElement()->Attribute("skyName") != nullptr) {
			skyName = handle.ToElement()->Attribute("skyName");
		}
		dynamic_cast<CSimpleSceneNode*>(node)->SetSkyName(skyName);

		// Get the sky ambient color
		if (handle.ToElement()->Attribute("skyAmbientColor") != nullptr) {
			skyAmbientColor = CVector3F::GetFrom(handle.ToElement()->Attribute("skyAmbientColor"));
		}
		dynamic_cast<CSimpleSceneNode*>(node)->SetSkyAmbientColor(CVector4F(skyAmbientColor.x, skyAmbientColor.y, skyAmbientColor.z, 1));

		// Read the level geometry
		auto* child = handle.FirstChildElement("multi_brush").ToElement();
		for (auto* childIt = child; childIt != nullptr; childIt = childIt->NextSiblingElement()) {
			// A brush created with a bunch of static models
			if(std::string(childIt->Name()) == "multi_brush") {
				tinyxml2::XMLHandle bspHandle(childIt);
				auto* child2 = bspHandle.FirstChildElement("brush").ToElement();
				SSimpleBrush brush;

				for (auto* child2It = child2; child2It != nullptr; child2It = child2It->NextSiblingElement()) {
					if(std::string(child2It->Name()) == "brush") {
						AddSimpleBrushData(tinyxml2::XMLHandle(child2It), brush);
						brush.isMultiBrush = true;
					}
				}

				if(!brush.geometry.empty()) {
					brushes.push_back(brush);
				}
			}
		}

		child = handle.FirstChildElement("brush").ToElement();
		for (tinyxml2::XMLElement * childIt = child; childIt != nullptr; childIt = childIt->NextSiblingElement()) {
			// A simple brush
			if(std::string(childIt->Name()) == "brush") {
				const tinyxml2::XMLHandle bspHandle(childIt);
				SSimpleBrush brush;

				AddSimpleBrushData(bspHandle, brush);
				brush.isMultiBrush = false;
				brushes.push_back(brush);
			}
		}

		// Add all the geometry in the bsp scene node
		dynamic_cast<CSimpleSceneNode*>(node)->LoadGeometry(brushes);
	}

	void CSceneLoader::LoadParticleSystem(I3DScene * scene, tinyxml2::XMLHandle handle) {
		LOG_TRACE();

		int particleSystemSize;
		std::string atlasTexture;
		CVector2I atlasSize;
		CVector3F gravity;

		// Read the system size
		GetNodeData(handle, "systemSize", particleSystemSize, 0);

		// Read the atlas texture
		GetNodeData(handle, "atlasTextureFile", atlasTexture, "particleAtlas.tga");
		GetNodeData(handle, "atlasTextureSize", atlasSize, CVector2I(1, 1));

		// Read the gravity
		GetNodeData(handle, "gravity", gravity, CVector3F());

		// Read the emitters templates
		auto* emittersTemplates = handle.FirstChildElement("particleEmittersTemplates").ToElement();
		if (emittersTemplates != nullptr) {
			// Read the templates
			auto templatesHandle = tinyxml2::XMLHandle(emittersTemplates);
			const auto templateElement = templatesHandle.FirstChildElement("template").ToElement();

			if (templateElement != nullptr) {
				for (auto templateElementIt = templateElement; templateElementIt != nullptr; templateElementIt = templateElementIt->NextSiblingElement()) {
					auto templateHandle = tinyxml2::XMLHandle(templateElementIt);
					const auto text = templateHandle.FirstChild().ToText();
					if(text != nullptr) {
						scene->GetParticleSystem().LoadEmittersTemplate(text->Value());
					}
				}
			}
		}

		scene->GetParticleSystem().InitializeSystem(particleSystemSize);
		scene->GetParticleSystem().SetAtlasTexture(atlasTexture, atlasSize);
		scene->GetParticleSystem().SetGravity(gravity);
	}

	void CSceneLoader::LoadDefaultParticleSystem(I3DScene * scene) {
		LOG_TRACE();

		scene->GetParticleSystem().InitializeSystem(5000);
		scene->GetParticleSystem().SetAtlasTexture("particleAtlas.tga", CVector2I(1, 1));
	}

	/**
	* Load a camera node
	* @param scene The scene
	* @param parent The node parent
	* @param handle The xml handle
	* @param node The node to fill
	* @param name The name
	*/
	void CSceneLoader::LoadCameraNode(I3DScene * scene, ISceneNode *& parent, tinyxml2::XMLHandle handle, ISceneNode *& node, const std::string & name) {
		LOG_TRACE();

		float fov;
		float ratio;
		float near;
		float far;
		CVector3F position;
		CVector3F rotation;
		CVector3F scale;
		auto* renderer = IRenderer::GetRenderer();

		// Search for defined values in the element or use default values
		GetNodeData(handle, "position", position, CVector3F(0, 0, 0));
		GetNodeData(handle, "rotation", rotation, CVector3F(0, 0, 0));
		GetNodeData(handle, "scale", scale, CVector3F(1, 1, 1));
		GetNodeData(handle, "fov", fov, DegreeToRadian(this->m_nodesConf.GetValueFloat("ICameraSceneNode", "fov", 55)));
		GetNodeData(handle, "ratio", ratio, renderer->GetResolution().x / static_cast<float>(renderer->GetResolution().y));
		GetNodeData(handle, "near", near, this->m_nodesConf.GetValueFloat("ICameraSceneNode", "near", 0.1F));
		GetNodeData(handle, "far", far, this->m_nodesConf.GetValueFloat("ICameraSceneNode", "far", 100.0F));

		// Create the node
		node = new CProjectionCameraSceneNode(scene, parent, name, position, rotation, DegreeToRadian(fov), ratio, near, far);
		node->SetScale(scale);
	}

	/**
	* Load a spot light node
	* @param scene The scene
	* @param parent The node parent
	* @param handle The xml handle
	* @param node The node to fill
	* @param name The name
	*/
	void CSceneLoader::LoadSpotLightNode(I3DScene * scene, ISceneNode *& parent, tinyxml2::XMLHandle handle, ISceneNode *& node, const std::string & name) {
		LOG_TRACE();

		bool enabled;
		bool shadowCaster;
		float range;
		float radius;
		float falloff;
		float tightness;
		CVector3F position;
		CVector3F rotation;
		CColor color;

		// Search for defined values in the element or use default values
		GetNodeData(handle, "enabled", enabled, true);
		GetNodeData(handle, "shadow_caster", shadowCaster, false);
		GetNodeData(handle, "position", position, CVector3F(0, 0, 0));
		GetNodeData(handle, "rotation", rotation, CVector3F(0, 0, 0));
		const auto nodeDefaultColor = CVector4F::GetFrom(this->m_nodesConf.GetValueString("ISpotLightSceneNode", "color", "1.0f:1.0f:1.0f:1.0f"));
		GetNodeData(handle, "color", color, CColor(nodeDefaultColor.x, nodeDefaultColor.y, nodeDefaultColor.z, nodeDefaultColor.w));
		GetNodeData(handle, "range", range, this->m_nodesConf.GetValueFloat("ISpotLightSceneNode", "range", 100.0F));
		GetNodeData(handle, "radius", radius, this->m_nodesConf.GetValueFloat("ISpotLightSceneNode", "radius", 30.0F));
		GetNodeData(handle, "falloff", falloff, this->m_nodesConf.GetValueFloat("ISpotLightSceneNode", "falloff", 0.8F));
		GetNodeData(handle, "tightness", tightness, this->m_nodesConf.GetValueFloat("ISpotLightSceneNode", "tightness", 0.6F));

		// Create the node
		node = new CSpotLightSceneNode(scene, parent, name, position, rotation, color, range, radius, falloff, tightness, shadowCaster, enabled);
	}

	/**
	* Load a camera node
	* @param scene The scene
	* @param parent The node parent
	* @param handle The xml handle
	* @param node The node to fill
	* @param name The name
	*/
	void CSceneLoader::LoadPointLightNode(I3DScene * scene, ISceneNode *& parent, tinyxml2::XMLHandle handle, ISceneNode *& node, const std::string & name) {
		LOG_TRACE();

		bool enabled;
		bool shadowCaster;
		float range;
		float falloff;
		CVector3F position;
		CColor color;

		// Search for defined values in the element or use default values
		GetNodeData(handle, "enabled", enabled, true);
		GetNodeData(handle, "shadow_caster", shadowCaster, false);
		GetNodeData(handle, "position", position, CVector3F(0, 0, 0));
		const auto nodeDefaultColor = CVector4F::GetFrom(this->m_nodesConf.GetValueString("IPointLightSceneNode", "color", "1.0f:1.0f:1.0f:1.0f"));
		GetNodeData(handle, "color", color, CColor(nodeDefaultColor.x, nodeDefaultColor.y, nodeDefaultColor.z, nodeDefaultColor.w));
		GetNodeData(handle, "range", range, this->m_nodesConf.GetValueFloat("IPointLightSceneNode", "range", 100.0F));
		GetNodeData(handle, "falloff", falloff, this->m_nodesConf.GetValueFloat("IPointLightSceneNode", "falloff", 0.8F));

		// Create the node
		node = new CPointLightSceneNode(scene, parent, name, position, color, range, falloff, shadowCaster, enabled);
	}

	/**
	* Load a directional light node
	* @param scene The scene
	* @param parent The node parent
	* @param handle The xml handle
	* @param node The node to fill
	* @param name The name
	*/
	void CSceneLoader::LoadDirectionalLightNode(I3DScene * scene, ISceneNode *& parent, tinyxml2::XMLHandle handle, ISceneNode *& node, const std::string & name) {
		LOG_TRACE();

		bool enabled;
		bool shadowCaster;
		CVector3F rotation;
		CColor color;

		// Search for defined values in the element or use default values
		GetNodeData(handle, "enabled", enabled, true);
		GetNodeData(handle, "shadow_caster", shadowCaster, false);
		GetNodeData(handle, "rotation", rotation, CVector3F(0, 0, 0));
		const auto nodeDefaultColor = CVector4F::GetFrom(this->m_nodesConf.GetValueString("IPointLightSceneNode", "color", "1.0f:1.0f:1.0f:1.0f"));
		GetNodeData(handle, "color", color, CColor(nodeDefaultColor.x, nodeDefaultColor.y, nodeDefaultColor.z, nodeDefaultColor.w));

		// Create the node
		node = new CDirectionalLightSceneNode(scene, parent, name, rotation, color, shadowCaster, enabled);
	}

	/**
	* Load a static mesh node
	* @param scene The scene
	* @param parent The node parent
	* @param handle The xml handle
	* @param node The node to fill
	* @param name The name
	*/
	void CSceneLoader::LoadStaticMeshNode(I3DScene * scene, ISceneNode *& parent, tinyxml2::XMLHandle handle, ISceneNode *& node, const std::string & name) {
		LOG_TRACE();

		bool enabled;
		bool shadowCaster;
		int meshShading;
		std::string diffuseTexture;
		std::string normalTexture;
		std::string material;
		std::string boundingBox;
		CVector3F position;
		CVector3F rotation;
		CVector3F scale;
		auto* renderer = IRenderer::GetRenderer();

		// Search for defined values in the element or use default values
		GetNodeData(handle, "enabled", enabled, true);
		GetNodeData(handle, "shadow_caster", shadowCaster, true);
		GetNodeData(handle, "position", position, CVector3F(0, 0, 0));
		GetNodeData(handle, "rotation", rotation, CVector3F(0, 0, 0));
		const auto nodeDefaultScale = CVector3F::GetFrom(this->m_nodesConf.GetValueString("IStaticMeshSceneNode", "scale", "1.0f:1.0f:1.0f"));
		GetNodeData(handle, "scale", scale, CVector3F(nodeDefaultScale.x, nodeDefaultScale.y, nodeDefaultScale.z));
		GetNodeData(handle, "diffuse_texture", diffuseTexture, "default.png");
		GetNodeData(handle, "normal_texture", normalTexture, "default.png");
		GetNodeData(handle, "meshShading", meshShading, 0);
		GetNodeData(handle, "material", material, "default.mtl");
		GetNodeData(handle, "boundingBox", boundingBox, "");
		const std::string file = handle.ToElement()->Attribute("file");

		// Create the node
		node = new CStaticMeshSceneNode(scene, parent, name, file, position, rotation, scale);
		node->SetEnabled(enabled);
		node->SetShadowCaster(shadowCaster);
		dynamic_cast<CStaticMeshSceneNode*>(node)->SetMeshTexture(TEXTURE_TYPE_DIFFUSE, diffuseTexture, PIXEL_FORMAT_A8R8G8B8, renderer->GetDefaultTextureFilter());
		dynamic_cast<CStaticMeshSceneNode*>(node)->SetMeshTexture(TEXTURE_TYPE_NORMAL, normalTexture, PIXEL_FORMAT_A8R8G8B8, renderer->GetDefaultTextureFilter());
		dynamic_cast<CStaticMeshSceneNode*>(node)->SetMeshShadingType(static_cast<EMeshShadingType>(meshShading));
		dynamic_cast<CStaticMeshSceneNode*>(node)->SetMeshMaterial(material);
		dynamic_cast<CStaticMeshSceneNode*>(node)->SetBoundingBox(boundingBox);
	}

	/**
	* Load an animated mesh node
	* @param scene The scene
	* @param parent The node parent
	* @param handle The xml handle
	* @param node The node to fill
	* @param name The name
	*/
	void CSceneLoader::LoadAnimatedMeshNode(I3DScene * scene, ISceneNode *& parent, tinyxml2::XMLHandle handle, ISceneNode *& node, const std::string & name) {
		LOG_TRACE();

		bool enabled;
		bool shadowCaster;
		int meshShading;
		std::string diffuseTexture;
		std::string normalTexture;
		std::string material;
		std::string boundingBox;
		CVector3F position;
		CVector3F rotation;
		CVector3F scale;
		auto* renderer = IRenderer::GetRenderer();

		// Search for defined values in the element or use default values
		GetNodeData(handle, "enabled", enabled, true);
		GetNodeData(handle, "shadow_caster", shadowCaster, true);
		GetNodeData(handle, "position", position, CVector3F(0, 0, 0));
		GetNodeData(handle, "rotation", rotation, CVector3F(0, 0, 0));
		const auto nodeDefaultScale = CVector3F::GetFrom(this->m_nodesConf.GetValueString("IStaticMeshSceneNode", "scale", "1.0f:1.0f:1.0f"));
		GetNodeData(handle, "scale", scale, CVector3F(nodeDefaultScale.x, nodeDefaultScale.y, nodeDefaultScale.z));
		GetNodeData(handle, "diffuse_texture", diffuseTexture, "default.png");
		GetNodeData(handle, "normal_texture", normalTexture, "default.png");
		GetNodeData(handle, "meshShading", meshShading, 0);
		GetNodeData(handle, "material", material, "default.mtl");
		GetNodeData(handle, "boundingBox", boundingBox, "");
		const std::string file = handle.ToElement()->Attribute("file");

		// Create the node
		node = new CAnimatedMeshSceneNode(scene, parent, name, file, position, rotation, scale);
		node->SetEnabled(enabled);
		node->SetShadowCaster(shadowCaster);
		dynamic_cast<CAnimatedMeshSceneNode*>(node)->SetMeshTexture(TEXTURE_TYPE_DIFFUSE, diffuseTexture, PIXEL_FORMAT_A8R8G8B8, renderer->GetDefaultTextureFilter());
		dynamic_cast<CAnimatedMeshSceneNode*>(node)->SetMeshTexture(TEXTURE_TYPE_NORMAL, normalTexture, PIXEL_FORMAT_A8R8G8B8, renderer->GetDefaultTextureFilter());
		dynamic_cast<CAnimatedMeshSceneNode*>(node)->SetMeshShadingType(static_cast<EMeshShadingType>(meshShading));
		dynamic_cast<CAnimatedMeshSceneNode*>(node)->SetMeshMaterial(material);
		dynamic_cast<CAnimatedMeshSceneNode*>(node)->SetBoundingBox(boundingBox);
	}

	/**
	* Load a sound node
	* @param scene The scene
	* @param parent The node parent
	* @param handle The xml handle
	* @param node The node to fill
	* @param name The name
	*/
	void CSceneLoader::LoadSoundNode(I3DScene * scene, ISceneNode *& parent, tinyxml2::XMLHandle handle, ISceneNode *& node, const std::string & name) {
		LOG_TRACE();

		bool relativeToListener;
		bool looped;
		bool playOnStartup;
		int soundType;
		int soundCategory;
		float coneInnerAngle;
		float coneOuterAngle;
		float referenceDistance;
		float maxDistance;
		std::string file;
		CVector3F position;
		CVector3F rotation;
		
		GetNodeData(handle, "position", position, CVector3F(0, 0, 0));
		GetNodeData(handle, "rotation", rotation, CVector3F(0, 0, 0));
		GetNodeData(handle, "type", soundType, this->m_nodesConf.GetValueBool("ISoundSceneNode", "type", false));
		GetNodeData(handle, "category", soundCategory, this->m_nodesConf.GetValueBool("ISoundSceneNode", "category", false));
		GetNodeData(handle, "cone_inner_angle", coneInnerAngle, this->m_nodesConf.GetValueFloat("ISoundSceneNode", "cone_inner_angle", 360.0F));
		GetNodeData(handle, "cone_outer_angle", coneOuterAngle, this->m_nodesConf.GetValueFloat("ISoundSceneNode", "cone_outer_angle", 360.0F));
		GetNodeData(handle, "reference_distance", referenceDistance, this->m_nodesConf.GetValueFloat("ISoundSceneNode", "reference_distance", 2));
		GetNodeData(handle, "max_distance", maxDistance, this->m_nodesConf.GetValueFloat("ISoundSceneNode", "max_distance", 20));
		GetNodeData(handle, "relative_to_listener", relativeToListener, this->m_nodesConf.GetValueBool("ISoundSceneNode", "relative_to_listener", false));
		GetNodeData(handle, "looped", looped, this->m_nodesConf.GetValueBool("ISoundSceneNode", "looped", false));
		GetNodeData(handle, "play_on_startup", playOnStartup, this->m_nodesConf.GetValueBool("ISoundSceneNode", "play_on_startup", false));
		GetNodeData(handle, "file", file, "");

		node = new CSoundSceneNode(scene, file, static_cast<ESoundType>(soundType), static_cast<ESoundCategory>(soundCategory), parent, name);
		dynamic_cast<CSoundSceneNode*>(node)->SetPosition(position);
		dynamic_cast<CSoundSceneNode*>(node)->SetRotation(rotation);
		dynamic_cast<CSoundSceneNode*>(node)->SetSoundConeInnerAngle(coneInnerAngle);
		dynamic_cast<CSoundSceneNode*>(node)->SetSoundConeOuterAngle(coneInnerAngle);
		dynamic_cast<CSoundSceneNode*>(node)->SetSoundReferenceDistance(referenceDistance);
		dynamic_cast<CSoundSceneNode*>(node)->SetSoundMaxDistance(maxDistance);
		dynamic_cast<CSoundSceneNode*>(node)->SetSoundRelativeToListener(relativeToListener);
		dynamic_cast<CSoundSceneNode*>(node)->SetSoundLooped(looped);
		if(playOnStartup) {
			dynamic_cast<CSoundSceneNode*>(node)->SetSoundAutoStart(true);
			dynamic_cast<CSoundSceneNode*>(node)->Update(0);
			dynamic_cast<CSoundSceneNode*>(node)->SetSoundStatus(SOUND_STATUS_PLAY);
		}
	}

	/**
	* Load an empty node
	* @param scene The scene
	* @param parent The node parent
	* @param handle The xml handle
	* @param node The node to fill
	* @param name The name
	*/
	void CSceneLoader::LoadEmptyNode(I3DScene * scene, ISceneNode *& parent, tinyxml2::XMLHandle handle, ISceneNode *& node, const std::string & name) {
		LOG_TRACE();

		bool enabled;
		CVector3F position;
		CVector3F rotation;

		// Search for defined values in the element or use default values
		GetNodeData(handle, "enabled", enabled, true);
		GetNodeData(handle, "position", position, CVector3F(0, 0, 0));
		GetNodeData(handle, "rotation", rotation, CVector3F(0, 0, 0));

		// Create the node
		node = new CEmptySceneNode(scene, parent, name, position);
		node->SetEnabled(enabled);
		dynamic_cast<CEmptySceneNode*>(node)->SetRotation(rotation);
	}

	/**
	* Load a particle emitter node
	* @param scene The scene
	* @param parent The node parent
	* @param handle The xml handle
	* @param node The node to fill
	* @param name The name
	*/
	void CSceneLoader::LoadParticleEmitterNode(I3DScene * scene, ISceneNode *& parent, tinyxml2::XMLHandle handle, ISceneNode *& node, const std::string & name) {
		LOG_TRACE();

		bool emitterActive;
		int emitterTextureId;
		CVector3F emitterPosition;

		// Template attributes
		std::string templateName = "";
		if(handle.ToElement()->Attribute("templateName") != nullptr) {
			templateName = handle.ToElement()->Attribute("templateName");
		}
		std::string emitterName = "";
		if(handle.ToElement()->Attribute("emitterName") != nullptr) {
			emitterName = handle.ToElement()->Attribute("emitterName");
		}

		// Create the emitter with a template
		const auto emitter = scene->GetParticleSystem().InstanciateEmitter(templateName, emitterName);

		// Basic values
		GetNodeData(handle, "active", emitterActive, emitter->IsActive());
		GetNodeData(handle, "position", emitterPosition, CVector3F(0, 0, 0));
		GetNodeData(handle, "textureId", emitterTextureId, 0);
		
		node = new CParticleEmitterSceneNode(scene, parent, name, emitter);
		dynamic_cast<CParticleEmitterSceneNode*>(node)->SetPosition(emitterPosition);
		dynamic_cast<CParticleEmitterSceneNode*>(node)->SetTemplateName(templateName);
		dynamic_cast<CParticleEmitterSceneNode*>(node)->SetEmitterName(emitterName);
		dynamic_cast<CParticleEmitterSceneNode*>(node)->SetActive(emitterActive);
		dynamic_cast<CParticleEmitterSceneNode*>(node)->SetTextureId(emitterTextureId);
	}

	void CSceneLoader::AddBSPBrushData(tinyxml2::XMLHandle handle, SBSPBrushDefinition & brushDefinition) {
		LOG_TRACE();

		bool visible;
		CVector3F position;
		CVector3F rotation;
		CVector3F scale;
		std::string diffuseTexture;
		std::string normalTexture;
		std::string material;
		std::ostringstream ss;
		auto* renderer = IRenderer::GetRenderer();

		const std::string name = handle.ToElement()->Attribute("file");
		auto id = SCENE_DEFAULT_ID;
		handle.ToElement()->QueryIntAttribute("id", &id);

		GetNodeData(handle, "visible", visible, true);
		GetNodeData(handle, "position", position, CVector3F(0, 0, 0));
		GetNodeData(handle, "rotation", rotation, CVector3F(0, 0, 0));
		GetNodeData(handle, "scale", scale, CVector3F(1, 1, 1));
		GetNodeData(handle, "diffuse_texture", diffuseTexture, "");
		GetNodeData(handle, "normal_texture", normalTexture, "");
		GetNodeData(handle, "material", material, "default.mtl");

		// Modification matrixes
		auto worldMatrix = CMatrix(scale, CVector3F(DegreeToRadian(rotation.x), DegreeToRadian(rotation.y), DegreeToRadian(rotation.z)), position);
		auto rotationMatrix = CMatrix(CVector3F(1, 1, 1), CVector3F(DegreeToRadian(rotation.x), DegreeToRadian(rotation.y), DegreeToRadian(rotation.z)), CVector3F(0, 0, 0));

		const CMaterial mat(material);

		// Create the geometry
		CStaticMesh mesh(name);
		for(auto i = 0; i < mesh.GetMesh()->GetNumberOfGroups(); ++i) {
			CTexture2D diffuse;
			CTexture2D normal;

			if(!diffuseTexture.empty()) {
				diffuse.CreateFromFile(diffuseTexture, PIXEL_FORMAT_A8R8G8B8, renderer->GetDefaultTextureFilter());
			}

			if(!normalTexture.empty()) {
				normal.CreateFromFile(normalTexture, PIXEL_FORMAT_A8R8G8B8, renderer->GetDefaultTextureFilter());
			}

			for(auto j = 0; j < mesh.GetMesh()->GetGroupSize(i); j += 3) {
				auto* triangle = new CBSPTriangle();
				triangle->SetId(id);

				ss.str("");
				ss << "group_" << (i + 1);

				for(auto k = 0; k < 3; ++k) {
					triangle->SetTexcoord(mesh.GetMesh()->GetTexcoords(i)[j + k], k);
					triangle->SetVertex(worldMatrix.Transform(mesh.GetMesh()->GetVertices(i)[j + k]), k);
					triangle->SetNormal(rotationMatrix.Transform(mesh.GetMesh()->GetNormals(i)[j + k], true), k);
				}

				triangle->SetVisible(visible);
				triangle->SetTexture(diffuse, TEXTURE_TYPE_DIFFUSE);
				triangle->SetTexture(normal, TEXTURE_TYPE_NORMAL);
				triangle->SetMaterial(mat, this->m_materialsConf.GetValueString(name, ss.str(), "default"));
				triangle->CreatePlane();
				triangle->Update();

				// Add the triangle to the geometry
				brushDefinition.geometry.push_back(triangle);
			}
		}
	}

	/**
	* Add data into the bsp brush definition
	* @param file The mesh name
	* @param position The position
	* @param rotation The rotation
	* @param scale The scale
	* @param brushDefinition The brush definition
	*/
	void CSceneLoader::AddBSPBoundingBoxData(const std::string& file, const CVector3F& position, const CVector3F &rotation, const CVector3F& scale, SBSPBrushDefinition & brushDefinition) {
		LOG_TRACE();

		std::stringstream ss;

		// Modification matrixes
		auto worldMatrix = CMatrix(scale, CVector3F(DegreeToRadian(rotation.x), DegreeToRadian(rotation.y), DegreeToRadian(rotation.z)), position);
		auto rotationMatrix = CMatrix(CVector3F(1, 1, 1), CVector3F(DegreeToRadian(rotation.x), DegreeToRadian(rotation.y), DegreeToRadian(rotation.z)), CVector3F(0, 0, 0));

		const CMaterial mat("default.mtl");

		// Create the geometry
		CStaticMesh mesh(file);
		for(auto i = 0; i < mesh.GetMesh()->GetNumberOfGroups(); ++i) {

			for(auto j = 0; j < mesh.GetMesh()->GetGroupSize(i); j += 3) {
				auto* triangle = new CBSPTriangle();
				triangle->SetId(brushDefinition.id);

				ss.str("");
				ss << "group_" << (i + 1);

				for(int k = 0; k < 3; ++k) {
					triangle->SetTexcoord(mesh.GetMesh()->GetTexcoords(i)[j + k], k);
					triangle->SetVertex(worldMatrix.Transform(mesh.GetMesh()->GetVertices(i)[j + k]), k);
					triangle->SetNormal(rotationMatrix.Transform(mesh.GetMesh()->GetNormals(i)[j + k], true), k);
				}

				triangle->SetVisible(false);
				triangle->SetMaterial(mat, this->m_materialsConf.GetValueString(file, ss.str(), "default"));
				triangle->CreatePlane();
				triangle->Update();

				// Add the triangle to the geometry
				brushDefinition.geometry.push_back(triangle);
			}
		}
	}

	void CSceneLoader::AddSimpleBrushData(tinyxml2::XMLHandle handle, SSimpleBrush & brush) {
		LOG_TRACE();

		bool visible;
		CVector3F position;
		CVector3F rotation;
		CVector3F scale;
		std::string diffuseTexture;
		std::string normalTexture;
		std::string material;
		std::ostringstream ss;
		auto* renderer = IRenderer::GetRenderer();

		std::string name = handle.ToElement()->Attribute("file");
		auto id = SCENE_DEFAULT_ID;
		handle.ToElement()->QueryIntAttribute("id", &id);

		GetNodeData(handle, "visible", visible, true);
		GetNodeData(handle, "position", position, CVector3F(0, 0, 0));
		GetNodeData(handle, "rotation", rotation, CVector3F(0, 0, 0));
		GetNodeData(handle, "scale", scale, CVector3F(1, 1, 1));
		GetNodeData(handle, "diffuse_texture", diffuseTexture, "");
		GetNodeData(handle, "normal_texture", normalTexture, "");
		GetNodeData(handle, "material", material, "default.mtl");

		// Modification matrixes
		auto worldMatrix = CMatrix(scale, CVector3F(DegreeToRadian(rotation.x), DegreeToRadian(rotation.y), DegreeToRadian(rotation.z)), position);
		auto rotationMatrix = CMatrix(CVector3F(1, 1, 1), CVector3F(DegreeToRadian(rotation.x), DegreeToRadian(rotation.y), DegreeToRadian(rotation.z)), CVector3F(0, 0, 0));

		const CMaterial mat(material);

		// Create the geometry
		CStaticMesh mesh(name);
		auto minVector = CVector3F(std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
		auto maxVector = CVector3F(std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min());
		for(auto i = 0; i < mesh.GetMesh()->GetNumberOfGroups(); ++i) {
			CTexture2D diffuse;
			CTexture2D normal;
			
			if(!diffuseTexture.empty()) {
				diffuse.CreateFromFile(diffuseTexture, PIXEL_FORMAT_A8R8G8B8, renderer->GetDefaultTextureFilter());
			} 

			if(!normalTexture.empty()) {
				normal.CreateFromFile(normalTexture, PIXEL_FORMAT_A8R8G8B8, renderer->GetDefaultTextureFilter());
			}

			for(auto j = 0; j < mesh.GetMesh()->GetGroupSize(i); j += 3) {
				auto* triangle = new CTriangle3D();
				triangle->SetId(id);

				ss.str("");
				ss << "group_" << (i + 1);

				for(auto k = 0; k < 3; ++k) {
					triangle->SetTexcoord(mesh.GetMesh()->GetTexcoords(i)[j + k], k);
					triangle->SetVertex(worldMatrix.Transform(mesh.GetMesh()->GetVertices(i)[j + k]), k);
					triangle->SetNormal(rotationMatrix.Transform(mesh.GetMesh()->GetNormals(i)[j + k], true), k);

					// Update the min and max values
					minVector.x = std::min(minVector.x, triangle->GetVertex(k).x);
					minVector.y = std::min(minVector.y, triangle->GetVertex(k).y);
					minVector.z = std::min(minVector.z, triangle->GetVertex(k).z);

					maxVector.x = std::max(maxVector.x, triangle->GetVertex(k).x);
					maxVector.y = std::max(maxVector.y, triangle->GetVertex(k).y);
					maxVector.z = std::max(maxVector.z, triangle->GetVertex(k).z);
				}

				triangle->SetTexture(diffuse, TEXTURE_TYPE_DIFFUSE);
				triangle->SetTexture(normal, TEXTURE_TYPE_NORMAL);
				triangle->SetMaterial(mat, this->m_materialsConf.GetValueString(name, ss.str(), "default"));
				triangle->Update();

				// Find the triangle plane
				CVector3F tmp[] = { triangle->GetVertex(0), triangle->GetVertex(1), triangle->GetVertex(2) };
				auto plane = CPlane3DF(tmp);
				triangle->SetFlatNormal(plane.GetNormal());

				// Add the triangle to the geometry
				brush.geometry.push_back(triangle);
			}
		}
		// Add the visibility shape to the geometry
		brush.visibilityShape = new CSphereF((maxVector + minVector) / 2.0F, (maxVector - minVector).Length() / 2.0F);

		// Add the original values
		brush.filename = name;
		brush.position = position;
		brush.rotation = rotation;
		brush.scale = scale;
	}

	void CSceneLoader::SaveNode(daidalosengine::ISceneNode * node, tinyxml2::XMLDocument & document, tinyxml2::XMLElement * parent) {

		if(node->IsSaved()) {
			switch(node->GetType()) {
			case SCENE_NODE_ROOT:
				if(this->m_structure == SCENE_NODE_ROOT_STRUCTURE_TYPE_SIMPLE) {
					SaveSimpleStructure(node, document, parent);
				} else {
					SaveBSPStructure(node, document, parent);
				}
				break;

			case SCENE_NODE_CAMERA:
				SaveCameraNode(node, document, parent);
				break;

			case SCENE_NODE_SPOTLIGHT:
				SaveSpotLightNode(node, document, parent);
				break;

			case SCENE_NODE_POINTLIGHT:
				SavePointLightNode(node, document, parent);
				break;

			case SCENE_NODE_DIRECTIONALLIGHT:
				SaveDirectionalLightNode(node, document, parent);
				break;

			case SCENE_NODE_STATICMESH:
				SaveStaticMeshNode(node, document, parent);
				break;

			case SCENE_NODE_ANIMATEDMESH:
				SaveAnimatedMeshNode(node, document, parent);
				break;

			case SCENE_NODE_SOUND:
				SaveSoundNode(node, document, parent);
				break;

			case SCENE_NODE_EMPTY:
				SaveEmptyNode(node, document, parent);
				break;

			case SCENE_NODE_PARTICLE_EMITTER:
				SaveParticleEmitterNode(node, document, parent);
				break;

			default:
				break;
			}
		}
	}

	void CSceneLoader::SaveParticleSystem(daidalosengine::I3DScene * scene, tinyxml2::XMLDocument & document, tinyxml2::XMLElement * parent) {

		parent->InsertEndChild(document.NewComment(" Particle System Parameters "));

		auto* particleSystemNode = document.NewElement("particleSystem");

		auto* atlasTextureFileNode = document.NewElement("atlasTextureFile");
		atlasTextureFileNode->InsertEndChild(document.NewText(scene->GetParticleSystem().GetAtlasTextureName().c_str()));
		particleSystemNode->InsertEndChild(atlasTextureFileNode);

		auto* atlasTextureSizeNode = document.NewElement("atlasTextureSize");
		atlasTextureSizeNode->InsertEndChild(document.NewText(scene->GetParticleSystem().GetAtlasTextureSize().ToString().c_str()));
		particleSystemNode->InsertEndChild(atlasTextureSizeNode);

		auto* systemSizeNode = document.NewElement("systemSize");
		systemSizeNode->InsertEndChild(document.NewText(std::to_string(scene->GetParticleSystem().GetSystemSize()).c_str()));
		particleSystemNode->InsertEndChild(systemSizeNode);

		auto emitterTemplates = scene->GetParticleSystem().GetEmittersTemplatesName();
		if(!emitterTemplates.empty()) {
			// Templates
			auto* particleSystemEmitterTemplatesNode = document.NewElement("particleEmittersTemplates");
			
			// Template
			for (auto& emitterTemplate : emitterTemplates) {
				auto* templateNode = document.NewElement("template");
				templateNode->InsertEndChild(document.NewText(emitterTemplate.c_str()));
				particleSystemEmitterTemplatesNode->InsertEndChild(templateNode);
			}
			particleSystemNode->InsertEndChild(particleSystemEmitterTemplatesNode);
		}

		parent->InsertEndChild(particleSystemNode);
	}

	/**
	* Save the simple structure
	* @param node The node
	* @param document The xml document
	* @param parent The element parent
	*/
	void CSceneLoader::SaveSimpleStructure(ISceneNode * node, tinyxml2::XMLDocument & document, tinyxml2::XMLElement * parent) {
		// Nothing to do
	}

	/**
	* Save the bsp structure
	* @param node The node
	* @param document The xml document
	* @param parent The element parent
	*/
	void CSceneLoader::SaveBSPStructure(ISceneNode * node, tinyxml2::XMLDocument & document, tinyxml2::XMLElement * parent) {
		// Nothing to do
	}

	/**
	* Save a camera node
	* @param node The node
	* @param document The xml document
	* @param parent The element parent
	*/
	void CSceneLoader::SaveCameraNode(ISceneNode * node, tinyxml2::XMLDocument & document, tinyxml2::XMLElement * parent) {

		parent->InsertEndChild(document.NewComment(" Projection Camera Node "));

		std::stringstream ss;
		auto* sceneNode = dynamic_cast<CProjectionCameraSceneNode*>(node);
		auto* cameraNode = document.NewElement("node");

		// Type
		cameraNode->SetAttribute("type", SCENE_NODE_CAMERA);

		// Id
		if(sceneNode->GetId() != SCENE_DEFAULT_ID) {
			cameraNode->SetAttribute("id", sceneNode->GetId());
		}

		// Name
		cameraNode->SetAttribute("name", sceneNode->GetName().c_str());
		
		// Position
		auto* child = document.NewElement("position");
		child->InsertEndChild(document.NewText(sceneNode->GetPosition().ToString().c_str()));
		cameraNode->InsertEndChild(child);

		// Rotation
		child = document.NewElement("rotation");
		child->InsertEndChild(document.NewText(sceneNode->GetRotation().ToString().c_str()));
		cameraNode->InsertEndChild(child);

		// Scale
		child = document.NewElement("scale");
		child->InsertEndChild(document.NewText(sceneNode->GetScale().ToString().c_str()));
		cameraNode->InsertEndChild(child);

		// Fov
		child = document.NewElement("fov");
		ss << RadianToDegree(sceneNode->GetAngle());
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		cameraNode->InsertEndChild(child);

		// Near
		child = document.NewElement("near");
		ss.str("");
		ss << sceneNode->GetNearDistance();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		cameraNode->InsertEndChild(child);

		// Far
		child = document.NewElement("far");
		ss.str("");
		ss << sceneNode->GetFarDistance();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		cameraNode->InsertEndChild(child);

		// Attributes
		SaveNodeAttributes(sceneNode, document, cameraNode);

		parent->InsertEndChild(cameraNode);

		// Save the children
		for (auto it : node->GetChildren()) {
			SaveNode(it, document, cameraNode);
		}
	}

	/**
	* Save a spot light node
	* @param node The node
	* @param document The xml document
	* @param parent The element parent
	*/
	void CSceneLoader::SaveSpotLightNode(ISceneNode * node, tinyxml2::XMLDocument & document, tinyxml2::XMLElement * parent) {

		parent->InsertEndChild(document.NewComment(" SpotLight Node "));

		std::stringstream ss;
		auto* sceneNode = dynamic_cast<CSpotLightSceneNode*>(node);
		auto* lightNode = document.NewElement("node");

		// Type
		lightNode->SetAttribute("type", SCENE_NODE_SPOTLIGHT);

		// Id
		if(sceneNode->GetId() != SCENE_DEFAULT_ID) {
			lightNode->SetAttribute("id", sceneNode->GetId());
		}

		// Name
		lightNode->SetAttribute("name", sceneNode->GetName().c_str());

		// Enabled
		auto* child = document.NewElement("enabled");
		child->InsertEndChild(document.NewText(sceneNode->IsEnabled() ? "true" : "false"));
		lightNode->InsertEndChild(child);

		// Shadow
		child = document.NewElement("shadow_caster");
		child->InsertEndChild(document.NewText(sceneNode->IsShadowCaster() ? "true" : "false"));
		lightNode->InsertEndChild(child);

		// Position
		child = document.NewElement("position");
		child->InsertEndChild(document.NewText(sceneNode->GetPosition().ToString().c_str()));
		lightNode->InsertEndChild(child);

		// Rotation
		child = document.NewElement("rotation");
		child->InsertEndChild(document.NewText(sceneNode->GetRotation().ToString().c_str()));
		lightNode->InsertEndChild(child);

		// Color
		child = document.NewElement("color");
		child->InsertEndChild(document.NewText(sceneNode->GetColor().ToString().c_str()));
		lightNode->InsertEndChild(child);

		// Range
		child = document.NewElement("range");
		ss << sceneNode->GetRange();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		lightNode->InsertEndChild(child);

		// Radius
		ss.str("");
		child = document.NewElement("radius");
		ss << RadianToDegree(sceneNode->GetRadius());
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		lightNode->InsertEndChild(child);

		// Falloff
		ss.str("");
		child = document.NewElement("falloff");
		ss << sceneNode->GetFalloff();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		lightNode->InsertEndChild(child);

		// Tightness
		ss.str("");
		child = document.NewElement("tightness");
		ss << sceneNode->GetTightness();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		lightNode->InsertEndChild(child);

		// Attributes
		SaveNodeAttributes(sceneNode, document, lightNode);

		parent->InsertEndChild(lightNode);

		// Save the children
		for (auto it : node->GetChildren()) {
			SaveNode(it, document, lightNode);
		}
	}

	/**
	* Save a point light node
	* @param node The node
	* @param document The xml document
	* @param parent The element parent
	*/
	void CSceneLoader::SavePointLightNode(ISceneNode * node, tinyxml2::XMLDocument & document, tinyxml2::XMLElement * parent) {

		parent->InsertEndChild(document.NewComment(" PointLight Node "));

		std::stringstream ss;
		auto* sceneNode = dynamic_cast<CPointLightSceneNode*>(node);
		auto* lightNode = document.NewElement("node");

		// Type
		lightNode->SetAttribute("type", SCENE_NODE_POINTLIGHT);

		// Id
		if(sceneNode->GetId() != SCENE_DEFAULT_ID) {
			lightNode->SetAttribute("id", sceneNode->GetId());
		}

		// Name
		lightNode->SetAttribute("name", sceneNode->GetName().c_str());

		// Enabled
		auto* child = document.NewElement("enabled");
		child->InsertEndChild(document.NewText(sceneNode->IsEnabled() ? "true" : "false"));
		lightNode->InsertEndChild(child);

		// Shadow
		child = document.NewElement("shadow_caster");
		child->InsertEndChild(document.NewText(sceneNode->IsShadowCaster() ? "true" : "false"));
		lightNode->InsertEndChild(child);

		// Position
		child = document.NewElement("position");
		child->InsertEndChild(document.NewText(sceneNode->GetPosition().ToString().c_str()));
		lightNode->InsertEndChild(child);

		// Rotation
		child = document.NewElement("rotation");
		child->InsertEndChild(document.NewText(sceneNode->GetRotation().ToString().c_str()));
		lightNode->InsertEndChild(child);

		// Color
		child = document.NewElement("color");
		child->InsertEndChild(document.NewText(sceneNode->GetColor().ToString().c_str()));
		lightNode->InsertEndChild(child);

		// Range
		child = document.NewElement("range");
		ss << sceneNode->GetRange();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		lightNode->InsertEndChild(child);

		// Falloff
		ss.str("");
		child = document.NewElement("falloff");
		ss << sceneNode->GetFalloff();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		lightNode->InsertEndChild(child);

		// Attributes
		SaveNodeAttributes(sceneNode, document, lightNode);

		parent->InsertEndChild(lightNode);

		// Save the children
		for (auto it : node->GetChildren()) {
			SaveNode(it, document, lightNode);
		}
	}

	/**
	* Save a directional light node
	* @param node The node
	* @param document The xml document
	* @param parent The element parent
	*/
	void CSceneLoader::SaveDirectionalLightNode(ISceneNode * node, tinyxml2::XMLDocument & document, tinyxml2::XMLElement * parent) {

		parent->InsertEndChild(document.NewComment(" DirectionalLight Node "));

		auto* sceneNode = dynamic_cast<CDirectionalLightSceneNode*>(node);
		auto* lightNode = document.NewElement("node");

		// Type
		lightNode->SetAttribute("type", SCENE_NODE_DIRECTIONALLIGHT);

		// Id
		if(sceneNode->GetId() != SCENE_DEFAULT_ID) {
			lightNode->SetAttribute("id", sceneNode->GetId());
		}

		// Name
		lightNode->SetAttribute("name", sceneNode->GetName().c_str());

		// Enabled
		auto* child = document.NewElement("enabled");
		child->InsertEndChild(document.NewText(sceneNode->IsEnabled() ? "true" : "false"));
		lightNode->InsertEndChild(child);

		// Shadow
		child = document.NewElement("shadow_caster");
		child->InsertEndChild(document.NewText(sceneNode->IsShadowCaster() ? "true" : "false"));
		lightNode->InsertEndChild(child);

		// Position
		child = document.NewElement("position");
		child->InsertEndChild(document.NewText(sceneNode->GetPosition().ToString().c_str()));
		lightNode->InsertEndChild(child);

		// Rotation
		child = document.NewElement("rotation");
		child->InsertEndChild(document.NewText(sceneNode->GetRotation().ToString().c_str()));
		lightNode->InsertEndChild(child);

		// Color
		child = document.NewElement("color");
		child->InsertEndChild(document.NewText(sceneNode->GetColor().ToString().c_str()));
		lightNode->InsertEndChild(child);

		// Attributes
		SaveNodeAttributes(sceneNode, document, lightNode);

		parent->InsertEndChild(lightNode);

		// Save the children
		for (auto it : node->GetChildren()) {
			SaveNode(it, document, lightNode);
		}
	}

	/**
	* Save a static mesh node
	* @param node The node
	* @param document The xml document
	* @param parent The element parent
	*/
	void CSceneLoader::SaveStaticMeshNode(ISceneNode * node, tinyxml2::XMLDocument & document, tinyxml2::XMLElement * parent) {

		parent->InsertEndChild(document.NewComment(" Static Mesh Node "));

		std::stringstream ss;
		auto* sceneNode = dynamic_cast<CStaticMeshSceneNode*>(node);
		auto* meshNode = document.NewElement("node");

		// Type
		meshNode->SetAttribute("type", SCENE_NODE_STATICMESH);

		// Id
		if(sceneNode->GetId() != SCENE_DEFAULT_ID) {
			meshNode->SetAttribute("id", sceneNode->GetId());
		}

		// Name
		meshNode->SetAttribute("name", sceneNode->GetName().c_str());

		// File
		meshNode->SetAttribute("file", sceneNode->GetMeshName().c_str());

		// Enabled
		auto* child = document.NewElement("enabled");
		child->InsertEndChild(document.NewText(sceneNode->IsEnabled() ? "true" : "false"));
		meshNode->InsertEndChild(child);

		// Shadow
		child = document.NewElement("shadow_caster");
		child->InsertEndChild(document.NewText(sceneNode->IsShadowCaster() ? "true" : "false"));
		meshNode->InsertEndChild(child);

		// Position
		child = document.NewElement("position");
		child->InsertEndChild(document.NewText(sceneNode->GetPosition().ToString().c_str()));
		meshNode->InsertEndChild(child);

		// Rotation
		child = document.NewElement("rotation");
		child->InsertEndChild(document.NewText(sceneNode->GetRotation().ToString().c_str()));
		meshNode->InsertEndChild(child);

		// Scale
		child = document.NewElement("scale");
		child->InsertEndChild(document.NewText(sceneNode->GetScale().ToString().c_str()));
		meshNode->InsertEndChild(child);

		// Diffuse texture
		if(!sceneNode->GetMeshTextureName(TEXTURE_TYPE_DIFFUSE).empty()) {
			child = document.NewElement("diffuse_texture");
			child->InsertEndChild(document.NewText(sceneNode->GetMeshTextureName(TEXTURE_TYPE_DIFFUSE).c_str()));
			meshNode->InsertEndChild(child);
		}

		// Normal texture
		if(!sceneNode->GetMeshTextureName(TEXTURE_TYPE_NORMAL).empty()) {
			child = document.NewElement("normal_texture");
			child->InsertEndChild(document.NewText(sceneNode->GetMeshTextureName(TEXTURE_TYPE_NORMAL).c_str()));
			meshNode->InsertEndChild(child);
		}

		// Mesh shading
		child = document.NewElement("meshShading");
		child->InsertEndChild(document.NewText(sceneNode->GetMeshShadingType() == MESH_SHADING_FLAT ? "0" : "1"));
		meshNode->InsertEndChild(child);

		// Material
		child = document.NewElement("material");
		child->InsertEndChild(document.NewText(sceneNode->GetMeshMaterialName().c_str()));
		meshNode->InsertEndChild(child);

		// Bounding box
		if(!sceneNode->GetBoundingBox().empty()) {
			child = document.NewElement("boundingBox");
			child->InsertEndChild(document.NewText(sceneNode->GetBoundingBox().c_str()));
			meshNode->InsertEndChild(child);
		}

		// Attributes
		SaveNodeAttributes(sceneNode, document, meshNode);

		parent->InsertEndChild(meshNode);

		// Save the children
		for (auto it : node->GetChildren()) {
			SaveNode(it, document, meshNode);
		}
	}

	/**
	* Save an animated mesh node
	* @param node The node
	* @param document The xml document
	* @param parent The element parent
	*/
	void CSceneLoader::SaveAnimatedMeshNode(ISceneNode * node, tinyxml2::XMLDocument & document, tinyxml2::XMLElement * parent) {

		parent->InsertEndChild(document.NewComment(" Animated Mesh Node "));

		auto* sceneNode = dynamic_cast<CAnimatedMeshSceneNode*>(node);
		auto* meshNode = document.NewElement("node");

		// Type
		meshNode->SetAttribute("type", SCENE_NODE_ANIMATEDMESH);

		// Id
		if(sceneNode->GetId() != SCENE_DEFAULT_ID) {
			meshNode->SetAttribute("id", sceneNode->GetId());
		}

		// Name
		meshNode->SetAttribute("name", sceneNode->GetName().c_str());

		// File
		meshNode->SetAttribute("file", sceneNode->GetMeshName().c_str());

		// Enabled
		auto* child = document.NewElement("enabled");
		child->InsertEndChild(document.NewText(sceneNode->IsEnabled() ? "true" : "false"));
		meshNode->InsertEndChild(child);

		// Shadow
		child = document.NewElement("shadow_caster");
		child->InsertEndChild(document.NewText(sceneNode->IsShadowCaster() ? "true" : "false"));
		meshNode->InsertEndChild(child);

		// Position
		child = document.NewElement("position");
		child->InsertEndChild(document.NewText(sceneNode->GetPosition().ToString().c_str()));
		meshNode->InsertEndChild(child);

		// Rotation
		child = document.NewElement("rotation");
		child->InsertEndChild(document.NewText(sceneNode->GetRotation().ToString().c_str()));
		meshNode->InsertEndChild(child);

		// Scale
		child = document.NewElement("scale");
		child->InsertEndChild(document.NewText(sceneNode->GetScale().ToString().c_str()));
		meshNode->InsertEndChild(child);

		// Diffuse texture
		if(!sceneNode->GetMeshTextureName(TEXTURE_TYPE_DIFFUSE).empty()) {
			child = document.NewElement("diffuse_texture");
			child->InsertEndChild(document.NewText(sceneNode->GetMeshTextureName(TEXTURE_TYPE_DIFFUSE).c_str()));
			meshNode->InsertEndChild(child);
		}

		// Normal texture
		if(!sceneNode->GetMeshTextureName(TEXTURE_TYPE_NORMAL).empty()) {
			child = document.NewElement("normal_texture");
			child->InsertEndChild(document.NewText(sceneNode->GetMeshTextureName(TEXTURE_TYPE_NORMAL).c_str()));
			meshNode->InsertEndChild(child);
		}

		// Mesh shading
		child = document.NewElement("meshShading");
		child->InsertEndChild(document.NewText(sceneNode->GetMeshShadingType() == MESH_SHADING_FLAT ? "0" : "1"));
		meshNode->InsertEndChild(child);

		// Material
		child = document.NewElement("material");
		child->InsertEndChild(document.NewText(sceneNode->GetMeshMaterialName().c_str()));
		meshNode->InsertEndChild(child);

		// Bounding box
		if(!sceneNode->GetBoundingBox().empty()) {
			child = document.NewElement("boundingBox");
			child->InsertEndChild(document.NewText(sceneNode->GetBoundingBox().c_str()));
			meshNode->InsertEndChild(child);
		}

		// Attributes
		SaveNodeAttributes(sceneNode, document, meshNode);

		parent->InsertEndChild(meshNode);

		// Save the children
		for (auto it : node->GetChildren()) {
			SaveNode(it, document, meshNode);
		}
	}

	/**
	* Save a sound node
	* @param node The node
	* @param document The xml document
	* @param parent The element parent
	*/
	void CSceneLoader::SaveSoundNode(ISceneNode * node, tinyxml2::XMLDocument & document, tinyxml2::XMLElement * parent) {

		parent->InsertEndChild(document.NewComment(" Sound Node "));

		std::stringstream ss;
		auto* sceneNode = dynamic_cast<CSoundSceneNode*>(node);
		auto* soundNode = document.NewElement("node");

		// Type
		soundNode->SetAttribute("type", SCENE_NODE_SOUND);

		// Id
		if(sceneNode->GetId() != SCENE_DEFAULT_ID) {
			soundNode->SetAttribute("id", sceneNode->GetId());
		}

		// Name
		soundNode->SetAttribute("name", sceneNode->GetName().c_str());

		// Position
		auto* child = document.NewElement("position");
		child->InsertEndChild(document.NewText(sceneNode->GetPosition().ToString().c_str()));
		soundNode->InsertEndChild(child);

		// Rotation
		child = document.NewElement("rotation");
		child->InsertEndChild(document.NewText(sceneNode->GetRotation().ToString().c_str()));
		soundNode->InsertEndChild(child);

		// Type
		ss.str("");
		child = document.NewElement("type");
		ss << sceneNode->GetSoundType();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		soundNode->InsertEndChild(child);

		// Category
		ss.str("");
		child = document.NewElement("category");
		ss << sceneNode->GetSoundCategory();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		soundNode->InsertEndChild(child);

		// Cone inner angle
		ss.str("");
		child = document.NewElement("cone_inner_angle");
		ss << sceneNode->GetSoundConeInnerAngle();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		soundNode->InsertEndChild(child);

		// Cone outer angle
		ss.str("");
		child = document.NewElement("cone_outer_angle");
		ss << sceneNode->GetSoundConeOuterAngle();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		soundNode->InsertEndChild(child);

		// Reference distance
		ss.str("");
		child = document.NewElement("reference_distance");
		ss << sceneNode->GetSoundReferenceDistance();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		soundNode->InsertEndChild(child);

		// Max distance
		ss.str("");
		child = document.NewElement("max_distance");
		ss << sceneNode->GetSoundMaxDistance();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		soundNode->InsertEndChild(child);

		// Relative to listener
		child = document.NewElement("relative_to_listener");
		child->InsertEndChild(document.NewText(sceneNode->IsSoundRelativeToListener() ? "true" : "false"));
		soundNode->InsertEndChild(child);

		// Looped
		child = document.NewElement("looped");
		child->InsertEndChild(document.NewText(sceneNode->IsSoundLooped() ? "true" : "false"));
		soundNode->InsertEndChild(child);

		// Play on startup
		ss.str("");
		child = document.NewElement("play_on_startup");
		ss << sceneNode->IsSoundAutoStart();
		child->InsertEndChild(document.NewText(sceneNode->IsSoundAutoStart() ? "true" : "false"));
		soundNode->InsertEndChild(child);

		// File
		child = document.NewElement("file");
		child->InsertEndChild(document.NewText(sceneNode->GetSoundName().c_str()));
		soundNode->InsertEndChild(child);

		// Attributes
		SaveNodeAttributes(sceneNode, document, soundNode);

		parent->InsertEndChild(soundNode);

		// Save the children
		for (auto it : node->GetChildren()) {
			SaveNode(it, document, soundNode);
		}
	}

	/**
	* Save an empty node
	* @param node The node
	* @param document The xml document
	* @param parent The element parent
	*/
	void CSceneLoader::SaveEmptyNode(ISceneNode * node, tinyxml2::XMLDocument & document, tinyxml2::XMLElement * parent) {

		parent->InsertEndChild(document.NewComment(" Empty Node "));

		auto* sceneNode = dynamic_cast<CEmptySceneNode*>(node);
		auto* emptyNode = document.NewElement("node");

		// Type
		emptyNode->SetAttribute("type", SCENE_NODE_EMPTY);

		// Id
		if(sceneNode->GetId() != SCENE_DEFAULT_ID) {
			emptyNode->SetAttribute("id", sceneNode->GetId());
		}

		// Name
		emptyNode->SetAttribute("name", sceneNode->GetName().c_str());

		// Position
		auto* child = document.NewElement("position");
		child->InsertEndChild(document.NewText(sceneNode->GetPosition().ToString().c_str()));
		emptyNode->InsertEndChild(child);

		// Rotation
		child = document.NewElement("rotation");
		child->InsertEndChild(document.NewText(sceneNode->GetRotation().ToString().c_str()));
		emptyNode->InsertEndChild(child);

		// Attributes
		SaveNodeAttributes(sceneNode, document, emptyNode);

		parent->InsertEndChild(emptyNode);

		// Save the children
		for (auto it : node->GetChildren()) {
			SaveNode(it, document, emptyNode);
		}
	}

	/**
	* Save a particle emitter node
	* @param node The node
	* @param document The xml document
	* @param parent The element parent
	*/
	void CSceneLoader::SaveParticleEmitterNode(ISceneNode * node, tinyxml2::XMLDocument & document, tinyxml2::XMLElement * parent) {

		parent->InsertEndChild(document.NewComment(" Particle Emitter Node "));

		std::stringstream ss;
		auto* sceneNode = dynamic_cast<CParticleEmitterSceneNode*>(node);
		auto* emitterNode = document.NewElement("node");

		// Type
		emitterNode->SetAttribute("type", SCENE_NODE_PARTICLE_EMITTER);

		// Id
		if(sceneNode->GetId() != SCENE_DEFAULT_ID) {
			emitterNode->SetAttribute("id", sceneNode->GetId());
		}

		// Name
		emitterNode->SetAttribute("name", sceneNode->GetName().c_str());

		// Template name
		if(!sceneNode->GetTemplateName().empty()) {
			emitterNode->SetAttribute("templateName", sceneNode->GetTemplateName().c_str());
		}

		// Emitter name
		if(!sceneNode->GetEmitterName().empty()) {
			emitterNode->SetAttribute("emitterName", sceneNode->GetEmitterName().c_str());
		}

		// Active
		ss.str("");
		auto* child = document.NewElement("active");
		ss << sceneNode->IsActive();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		emitterNode->InsertEndChild(child);

		// Position
		child = document.NewElement("position");
		child->InsertEndChild(document.NewText(sceneNode->GetPosition().ToString().c_str()));
		emitterNode->InsertEndChild(child);

		// Texture id
		ss.str("");
		child = document.NewElement("textureId");
		ss << sceneNode->GetEmitter()->GetTextureId();
		child->InsertEndChild(document.NewText(ss.str().c_str()));
		emitterNode->InsertEndChild(child);

		// Attributes
		SaveNodeAttributes(sceneNode, document, emitterNode);

		parent->InsertEndChild(emitterNode);

		// Save the children
		for (auto it : node->GetChildren()) {
			SaveNode(it, document, emitterNode);
		}
	}

	/**
	* Save the node attributes
	* @param node The node
	* @param document The xml document
	* @param parent The element
	*/
	void CSceneLoader::SaveNodeAttributes(ISceneNode * node, tinyxml2::XMLDocument & document, tinyxml2::XMLElement * parent) {

		auto attributes = node->GetAttributes();

		if(!attributes.empty()) {
			auto* child = document.NewElement("attributes");

			for (auto& attribute : attributes) {
				auto element = document.NewElement(attribute.first.c_str());
				element->InsertEndChild(document.NewText(attribute.second.c_str()));
				child->InsertEndChild(element);
			}

			parent->InsertEndChild(child);
		}
	}

	void CSceneLoader::GetNodeData(tinyxml2::XMLHandle & handle, const std::string & name, bool & data, const bool defaultValue) {
		LOG_TRACE();

		const auto text = handle.FirstChildElement(name.c_str()).FirstChild().ToText();
		if(text != nullptr) {
			auto tmpText = TrimString(text->Value());
			std::transform(tmpText.begin(), tmpText.end(), tmpText.begin(), tolower);

			if (tmpText == "true" || tmpText == "1") {
				data = true;
			}
			if (tmpText == "false" || tmpText == "0") {
				data = false;
			}
		} else {
			data = defaultValue;
		}
	}

	void CSceneLoader::GetNodeData(tinyxml2::XMLHandle & handle, const std::string & name, int & data, const int defaultValue) {
		LOG_TRACE();

		const auto text = handle.FirstChildElement(name.c_str()).FirstChild().ToText();
		if(text != nullptr) {
			std::istringstream ss(text->Value());
			ss >> data;
		} else {
			data = defaultValue;
		}
	}

	void CSceneLoader::GetNodeData(tinyxml2::XMLHandle & handle, const std::string & name, CVector2I & data, const CVector2I & defaultValue) {
		LOG_TRACE();

		data = defaultValue;
		const auto text = handle.FirstChildElement(name.c_str()).FirstChild().ToText();
		if(text != nullptr) {
			data = CVector2I::GetFrom(std::string(text->Value()), defaultValue);
		}
	}

	void CSceneLoader::GetNodeData(tinyxml2::XMLHandle & handle, const std::string & name, CVector3F & data, const CVector3F & defaultValue) {
		LOG_TRACE();

		data = defaultValue;
		const auto text = handle.FirstChildElement(name.c_str()).FirstChild().ToText();
		if(text != nullptr) {
			data = CVector3F::GetFrom(std::string(text->Value()), defaultValue);
		}
	}

	void CSceneLoader::GetNodeData(tinyxml2::XMLHandle & handle, const std::string & name, CVector4F & data, const CVector4F & defaultValue) {
		LOG_TRACE();

		data = defaultValue;
		const auto text = handle.FirstChildElement(name.c_str()).FirstChild().ToText();
		if(text != nullptr) {
			data = CVector4F::GetFrom(std::string(text->Value()), defaultValue);
		}
	}

	void CSceneLoader::GetNodeData(tinyxml2::XMLHandle & handle, const std::string & name, CColor & data, const CColor & defaultValue) {
		LOG_TRACE();

		CTokenizer tokenizer;
		const auto text = handle.FirstChildElement(name.c_str()).FirstChild().ToText();
		if (text != nullptr) {
			tokenizer.AddLine(text->Value());
		}

		if(tokenizer.Tokenize(":", 4) && tokenizer.GetTokens().size() == 4) {
			std::istringstream ss;
			float r;
			float g;
			float b;
			float a;

			ss.str(tokenizer.GetTokens()[0]);
			ss >> r;
			ss.clear();
			ss.str(tokenizer.GetTokens()[1]);
			ss >> g;
			ss.clear();
			ss.str(tokenizer.GetTokens()[2]);
			ss >> b;
			ss.clear();
			ss.str(tokenizer.GetTokens()[3]);
			ss >> a;

			data = CColor(r, g, b, a);
		} else {
			data = defaultValue;
		}
	}

	void CSceneLoader::GetNodeData(tinyxml2::XMLHandle & handle, const std::string & name, float & data, const float defaultValue) {
		LOG_TRACE();

		const auto text = handle.FirstChildElement(name.c_str()).FirstChild().ToText();
		if(text != nullptr) {
			std::istringstream ss(text->Value());
			ss >> data;
		} else {
			data = defaultValue;
		}
	}

	void CSceneLoader::GetNodeData(tinyxml2::XMLHandle & handle, const std::string & name, std::string & data, const std::string & defaultValue) {
		LOG_TRACE();

		const auto text = handle.FirstChildElement(name.c_str()).FirstChild().ToText();
		if(text != nullptr) {
			data = text->Value();
		} else {
			data = defaultValue;
		}
	}

	std::map<std::string, std::string> CSceneLoader::GetNodeAttributes(tinyxml2::XMLHandle & handle) {
		LOG_TRACE();

		std::map<std::string, std::string> attributes;

		auto* child = handle.FirstChildElement("attributes").FirstChild().ToElement();
		for (auto* childIt = child; childIt != nullptr; childIt = childIt->NextSiblingElement()) {
			tinyxml2::XMLHandle attributeHandle(childIt);

			// Get the attribute name
			const std::string attributeName = attributeHandle.ToElement()->Name();

			// Get the attribute value
			auto* valueText = attributeHandle.FirstChild().ToText();

			// Add the attribue only if the two elements are valid
			if(valueText != nullptr) {
				attributes[attributeName] = valueText->Value();
			}
		}

		return attributes;
	}
}
