/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#ifndef __CSPRITE_HPP
#define __CSPRITE_HPP

#include "../../Scene/2D/IView.hpp"
#include "../../Medias/CTexture2D.hpp"

namespace daidalosengine {
	/**
	* Basic class for sprites
	*/
	class CSprite : public IView {
	private:
		float m_rotation;
		CColor m_backgroundColor;
		CTexture2D m_texture;
		CRectangleF m_coordinates;

	public:
		/**
		* Constructor
		*/
		CSprite();

		/**
		* Destructor
		*/
		virtual ~CSprite();

		/**
		* Initialize the content
		*/
		virtual void Initialize() override;

		/**
		* Set the rotation in degree
		* @param rotation The rotation
		*/
		void SetRotation(const float rotation);

		/**
		* Set the texture
		* @param texture The texture name
		*/
		void SetTexture(const std::string& texture);

		/**
		* Set the coordinates
		* @param coordinates The coordinates
		*/
		void SetCoordinates(const CRectangleF & coordinates);

		/**
		* Set the background color
		*/
		void SetBackgroundColor(const CColor & color);

		/**
		* Update the state and the geometry
		* @param time The elapsed time
		*/
		virtual void Update(const float time) override;

		/**
		* Render the button
		*/
		virtual void Render() override;

		/**
		* Get the rotation
		* @return The rotation
		*/
		float GetRotation() const;
	};
}

#endif
