/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include "../../../Scene/Particles/Initializers/CRadiusPositionParticleInitializer.hpp"

namespace daidalosengine {

	/**
	* Constructor
	* @param innerRadius The inner radius
	* @param outerRadius The outer radius
	* @param axis The axis
	*/
	CRadiusPositionParticleInitializer::CRadiusPositionParticleInitializer(const float innerRadius, const float outerRadius, const CVector3F & axis) : IParticleInitializer(PARTICLE_INITIALIZER_TYPE_RADIUS_POSITION), m_innerRadius(innerRadius), m_outerRadius(outerRadius), m_axis(axis) {}

	/**
	* Destructor
	*/
	CRadiusPositionParticleInitializer::~CRadiusPositionParticleInitializer() = default;

	/**
	* Initialize the particle
	* @param buffer The particle buffer
	* @param particles The particles id to initialize
	*/
	void CRadiusPositionParticleInitializer::Initialize(CMemoryAllocator<CParticleEntity> & buffer, const std::vector<int> & particles) {

		for (auto it : particles) {
			auto particle = buffer.GetAt(it);

			// Randomized direction
			CVector3F direction;
			for (auto i = 0; i < 3; ++i) {
				if (this->m_axis[i] == 1) {
					direction[i] = RandomNumber<float>(-1, 1);
				}
			}
			direction = direction.Normalize();

			// Keep the current position that hold the emitter's position
			particle->SetPosition(particle->GetPosition() + (direction * RandomNumber<float>(this->m_innerRadius, this->m_outerRadius)));
		}
	}

	/**
	* Get the inner radius
	* @return The inner radius
	*/
	float CRadiusPositionParticleInitializer::GetInnerRadius() const {
		return this->m_innerRadius;
	}

	/**
	* Get the outer radius
	* @return The outer radius
	*/
	float CRadiusPositionParticleInitializer::GetOuterRadius() const {
		return this->m_outerRadius;
	}

	/**
	* Clone the class
	* @return The cloned class
	*/
	IClonable * CRadiusPositionParticleInitializer::Clone() {
		return new CRadiusPositionParticleInitializer(this->m_innerRadius, this->m_outerRadius, this->m_axis);
	}
}