/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include <map>
#include "../../Core/CResourceManager.hpp"
#include "../../Core/CMediaManager.hpp"
#include "../../Core/Logger/ILogger.hpp"
#include "../../Scene/Particles/CParticleEmittersTemplates.hpp"

namespace daidalosengine {
	/**
	* Constructor
	*/
	CParticleEmittersTemplates::CParticleEmittersTemplates() {
		LOG_TRACE();
	}

	/**
	* Destructor
	*/
	CParticleEmittersTemplates::~CParticleEmittersTemplates() {
		LOG_TRACE();

		// Release all the templates
		for (auto& particleEmittersTemplate : this->m_particleEmittersTemplates) {
			CResourceManager::GetInstance()->ReleaseResource(particleEmittersTemplate.first);
		}
	}

	/**
	* Load a particle emitters template file
	* @param filename The file name
	*/
	void CParticleEmittersTemplates::LoadFromFile(const std::string & filename) {
		LOG_TRACE();

		// Check the existence of the resource and load it if necessary
		const auto tmp = CResourceManager::GetInstance()->GetResource<IParticleEmittersTemplate>(filename);

		if(tmp == nullptr) {
			this->m_particleEmittersTemplates[filename] = CMediaManager::GetInstance()->LoadMediaFromFile<IParticleEmittersTemplate>(filename);
			CResourceManager::GetInstance()->AddResource(filename, this->m_particleEmittersTemplates[filename]);
		} else {
			this->m_particleEmittersTemplates[filename] = tmp;
		}
	}

	/**
	* Get a vector containing the templates names
	* @return The templates names in a vector
	*/
	std::vector<std::string> CParticleEmittersTemplates::GetTemplatesName() const {
		LOG_TRACE();

		std::vector<std::string> templateNames;
		for (const auto& particleEmittersTemplate : this->m_particleEmittersTemplates) {
			templateNames.push_back(particleEmittersTemplate.first);
		}

		return templateNames;
	}

	/**
	* Instanciate a particle emitter from a given template and with a given name
	* @param templateName The template name
	* @param emitterName The emitter name
	* @return The emitter or nullptr if no match found
	*/
	CParticleEmitter * CParticleEmittersTemplates::InstanciateEmitter(const std::string & templateName, const std::string & emitterName) {
		LOG_TRACE();

		// Check for the template
		auto emitterTemplate = this->m_particleEmittersTemplates[templateName];
		if(emitterTemplate != nullptr) {
			// Check the emitter name
			auto emitter = emitterTemplate->GetEmitter(emitterName);
			if(emitterTemplate != nullptr) {
				// Clone the emitter
				return dynamic_cast<CParticleEmitter*>(emitter->Clone());
			}
		}
		return nullptr;
	}
}