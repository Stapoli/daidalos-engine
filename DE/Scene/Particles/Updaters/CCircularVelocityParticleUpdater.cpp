/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include <utility>
#include "../../../Scene/Particles/Updaters/CCircularVelocityParticleUpdater.hpp"

namespace daidalosengine {

	/**
	* Constructor
	* @param circularVelocityChanges The circular velocity changes definition
	* @param axis The rotation axis
	* @param damping The damping factor
	*/
	CCircularVelocityParticleUpdater::CCircularVelocityParticleUpdater(std::vector<SCircularVelocityParticleChangeUpdater> circularVelocityChanges, const CVector3F & axis, const float damping) : IParticleUpdater(PARTICLE_UPDATER_TYPE_VELOCITY), m_damping(damping), m_axis(axis), m_circularVelocityChanges(std::move(circularVelocityChanges)) {}

	/**
	* Destructor
	*/
	CCircularVelocityParticleUpdater::~CCircularVelocityParticleUpdater() = default;

	/**
	* Update the particle
	* @param time The elapsed time
	* @param buffer The particle buffer
	* @param particles The particles id to initialize
	*/
	void CCircularVelocityParticleUpdater::Update(const float time, CMemoryAllocator<CParticleEntity> & buffer, const std::vector<int> & particles) {
		const auto timeSecond = time / 1000.0F;

		for (auto it : particles) {
			auto particle = buffer.GetAt(it);

			// Next normalized life
			const auto nextNormalizedLife = (particle->GetLife() + time) / particle->GetLifeMax();

			// Check if a new linear velocity must be applied
			for (auto& circularVelocityChange : this->m_circularVelocityChanges) {
				if (circularVelocityChange.time >= particle->GetLifeNormalized() && circularVelocityChange.time < nextNormalizedLife) {
					// A new circular velocity must be add to the current particle's velocity

					// Add the circular velocity
					particle->SetCircularVelocity(particle->GetCircularVelocity() + (RandomNumber<float>(circularVelocityChange.velocityMin, circularVelocityChange.velocityMax) * particle->GetMass()));
				}
			}

			if (particle->GetCircularVelocity() != 0) {
				// Calculate the emitter - particle distance
				auto distance = particle->GetPosition() - particle->GetEmitterPosition();

				// Axis bound check
				if (this->m_axis.Length() == 1) {
					for (auto i = 0; i < 3; ++i) {
						if (this->m_axis[i] == 1 || this->m_axis[i] == -1) {
							distance[i] = 0;
						}
					}
				}
				const auto distanceNormalized = distance.Normalize();

				// Calculate the velocity direction (tangent to the up vector and the distance)
				const auto circularVelocity = this->m_axis.CrossProduct(distanceNormalized).Normalize() * particle->GetCircularVelocity();

				// Update the particle position
				particle->SetPosition(particle->GetPosition() + circularVelocity * timeSecond);

				// Put back the particle in the correct radius
				auto distance2 = particle->GetPosition() - particle->GetEmitterPosition();
				if (this->m_axis.Length() == 1) {
					for (auto i = 0; i < 3; ++i) {
						if (this->m_axis[i] == 1 || this->m_axis[i] == -1) {
							distance2[i] = 0;
						}
					}
				}
				particle->SetPosition(particle->GetPosition() - distance2.Normalize() * (distance2.Length() - distance.Length()));

				// Apply damping factor
				particle->SetCircularVelocity(particle->GetCircularVelocity() - (particle->GetCircularVelocity() * (this->m_damping * timeSecond)));
			}
		}
	}

	/**
	* Get the damping
	* @return The damping
	*/
	float CCircularVelocityParticleUpdater::GetDamping() const {
		return this->m_damping;
	}

	/**
	* Get the linear velocity changes definition
	* @return The linear velocity changes definition
	*/
	const std::vector<SCircularVelocityParticleChangeUpdater> & CCircularVelocityParticleUpdater::GetCircularVelocityChanges() const {
		return this->m_circularVelocityChanges;
	}

	/**
	* Clone the class
	* @return The cloned class
	*/
	IClonable * CCircularVelocityParticleUpdater::Clone() {
		return new CCircularVelocityParticleUpdater(this->m_circularVelocityChanges, this->m_axis, this->m_damping);
	}
}
