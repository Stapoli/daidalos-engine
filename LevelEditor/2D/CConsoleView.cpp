/*
==============================================================================================================================================================

Author: Stephane Baudoux
Attribution-Noncommercial-Share Alike 3.0 Unported

You are free:
to Share - to copy, distribute and transmit the work
to Remix - to adapt the work

Under the following conditions:
Attribution - You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
Noncommercial - You may not use this work for commercial purposes.
Share Alike - If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.

With the understanding that:
Waiver - Any of the above conditions can be waived if you get permission from the copyright holder.
Public Domain - Where the work or any of its elements is in the public domain under applicable law, that status is in no way affected by the license.
Other Rights - In no way are any of the following rights affected by the license:
Your fair dealing or fair use rights, or other applicable copyright exceptions and limitations;
The author's moral rights;
Rights other persons may have either in the work itself or in how the work is used, such as publicity or privacy rights.
Notice - For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.

==============================================================================================================================================================
*/

#include <Core/Utility/CTokenizer.hpp>
#include "../2D/CConsoleView.hpp"

#define LABEL_HEIGHT 20
#define MARGIN 10
#define CONSOLE_INPUT_TEXT ">"

#define CONSOLE_INPUT_LOAD ">LOAD"
#define CONSOLE_INPUT_SAVE ">SAVE"

namespace daidaloseditor {
	/**
	* Constructor
	* @param numberOfLines The number of lines
	*/
	CConsoleView::CConsoleView(const int numberOfLines) : m_numberOfLines(numberOfLines) {
		Initialize();
	}

	/**
	* Initialize the content
	*/
	void CConsoleView::Initialize() {
		// Set the bounds
		SetBounds(daidalosengine::CRectangleI(0, 0, 400, (this->m_numberOfLines + 1) * LABEL_HEIGHT));

		// Set background color
		SetBackgroundColor(daidalosengine::CColor(0.2F, 0.2F, 0.2F));

		// Create the input label
		this->m_inputLabel.SetBounds(daidalosengine::CRectangleI(MARGIN, GetBoundingRectangle().GetHeight() - LABEL_HEIGHT, GetBoundingRectangle().GetWidth() - (MARGIN * 2), LABEL_HEIGHT));
		this->m_inputLabel.SetSize(LABEL_HEIGHT);
		this->m_inputLabel.SetColor(daidalosengine::CColor(255, 255, 255, 255));
		this->m_inputLabel.SetHorizontalAlignment(daidalosengine::TEXT_HORIZONTAL_ALIGNMENT_LEFT);
		this->m_inputLabel.SetVerticalAlignment(daidalosengine::TEXT_VERTICAL_ALIGNMENT_CENTERED);
		this->m_inputLabel.SetText(CONSOLE_INPUT_TEXT);
		AddSubview(&this->m_inputLabel);

		// Create the history labels
		this->m_historyLabel.resize(this->m_numberOfLines);
		auto i = 0;
		for (auto& it : this->m_historyLabel) {
			it.SetBounds(daidalosengine::CRectangleI(MARGIN, i * LABEL_HEIGHT, GetBoundingRectangle().GetWidth() - (MARGIN * 2), LABEL_HEIGHT));
			it.SetSize(LABEL_HEIGHT);
			it.SetColor(daidalosengine::CColor(255, 255, 255, 255));
			it.SetHorizontalAlignment(daidalosengine::TEXT_HORIZONTAL_ALIGNMENT_LEFT);
			it.SetVerticalAlignment(daidalosengine::TEXT_VERTICAL_ALIGNMENT_CENTERED);
			it.SetText("");
			AddSubview(&it);
			i++;
		}

		SetVisible(false);
	}

	/**
	* Set the visible value
	* @param visible The visible value
	*/
	void CConsoleView::SetVisible(bool visible) {
		IView::SetVisible(visible);

		if(visible) {
			daidalosengine::CInputDevice::GetInstance()->AddKeyboardListener(this);
		} else {
			daidalosengine::CInputDevice::GetInstance()->RemoveKeyboardListener(this);

			// Clean the console
			this->m_inputLabel.SetText(CONSOLE_INPUT_TEXT);
		}
	}

	/**
	* Add a listener
	* @param listener The listener to add
	*/
	void CConsoleView::AddListener(IConsoleListener * listener) {
		this->m_listeners.push_back(listener);
	}

	/**
	* Remove a listener
	* @param listener The listener to remove
	*/
	void CConsoleView::RemoveListener(IConsoleListener * listener) {

		const auto it = std::find(this->m_listeners.begin(), this->m_listeners.end(), listener);
		if(it != this->m_listeners.end()) {
			this->m_listeners.erase(it);
		}
	}

	/**
	* Called when a keyboard action is started
	* @param actionId The action id
	*/
	void CConsoleView::OnInputActionStarted(const unsigned int actionId) {
		// Nothing to do
	}

	/**
	* Called when a keyboard action is stopped
	* @param actionId The action id
	*/
	void CConsoleView::OnInputActionStopped(const unsigned int actionId) {
		// Nothing to do
	}

	/**
	* Called when a key has been pressed
	* @param key The key
	*/
	void CConsoleView::OnKeyPressed(const unsigned int key) {

		// Add a key
		if(key >= '0' && key <= '9' || key >= 'A' && key <= 'Z' || key == ' ') {

			if((this->m_inputLabel.GetBoundingRectangle().GetWidth() + LABEL_HEIGHT) < GetBoundingRectangle().GetWidth()) {
				std::ostringstream ss;
				ss << this->m_inputLabel.GetText() << static_cast<char>(key);

				this->m_inputLabel.SetText(ss.str());
			}
		}

		// Backspace
		if(key == 8 && this->m_inputLabel.GetText().size() > 1) {
			this->m_inputLabel.SetText(this->m_inputLabel.GetText().substr(0, this->m_inputLabel.GetText().size() - 1));
		}

		// Carriage return
		if(key == 13) {
			ValidInputLabel();
		}
	}

	/**
	* Check the input label for a valid command
	*/
	void CConsoleView::ValidInputLabel() {
		daidalosengine::CTokenizer tokenizer;

		tokenizer.AddLine(this->m_inputLabel.GetText());
		tokenizer.Tokenize(" ");

		const auto tokens = tokenizer.GetTokens();
		if(tokens.size() == 2) {
			// Check for known commands
			if(std::string(CONSOLE_INPUT_LOAD) == tokens[0]) {
				// Load a scene
				for (auto& listener : this->m_listeners) {
					auto filename = tokens[1] + ".des";
					std::transform(filename.begin(), filename.end(), filename.begin(), ::tolower);
					listener->OnConsoleCommandLoad(filename);
				}

			} else if(std::string(CONSOLE_INPUT_SAVE) == tokens[0]) {
				// Save a scene
				for (auto& listener : this->m_listeners) {
					auto filename = tokens[1] + ".des";
					std::transform(filename.begin(), filename.end(), filename.begin(), ::tolower);
					listener->OnConsoleCommandSave(filename);
				}
			}
		}

		// Move the input to history and shift the history labels
		ShiftLabels();
	}

	/**
	* Move the current input in the history labels and shift them
	*/
	void CConsoleView::ShiftLabels() {

		for(unsigned int i = 0; i < this->m_historyLabel.size(); i++) {

			if((i + 1) < this->m_historyLabel.size()) {
				// Shift the history label
				this->m_historyLabel[i].SetText(this->m_historyLabel[i + 1].GetText());
			} else {
				// Take the input label text
				this->m_historyLabel[i].SetText(this->m_inputLabel.GetText());
			}
		}
		this->m_inputLabel.SetText(CONSOLE_INPUT_TEXT);
	}
}
