uniform float4 cameraPositionDistance;
uniform float4 pointLightPositionRange;
uniform float4 pointLightColorFalloff;

struct PS_INPUT {
	float4 position 	: POSITION;
	float2 texcoord		: TEXCOORD0;
	float3 viewDistance	: TEXCOORD1;
};

struct PS_OUTPUT {
	float4 color : COLOR;
};

sampler2D normalSampler;
sampler2D specularSampler;
sampler2D depthSampler;

float pointDistanceFactor(float distance) {
	float result;
	
	if(distance >= pointLightPositionRange.w) {
		result = 0;
	} else {
		if(distance > pointLightPositionRange.w * pointLightColorFalloff.w) {
			result = 1 - ((distance - (pointLightPositionRange.w * pointLightColorFalloff.w)) / ((1 - pointLightColorFalloff.w) * pointLightPositionRange.w));
		} else { 
			result = 1;
		}
	}
	return result;
}

PS_OUTPUT main(PS_INPUT IN) {
	PS_OUTPUT OUT;
	OUT.color = float4(0,0,0,1);
	
	float3 normal = (tex2D(normalSampler, IN.texcoord).xyz * 2) - 1;
	float3 currentPosition = cameraPositionDistance.xyz + (tex2D(depthSampler, IN.texcoord).r * IN.viewDistance);
	float4 specularNs = tex2D(specularSampler, IN.texcoord);

	float3 L = normalize(currentPosition.xyz - pointLightPositionRange.xyz);
	float3 R = reflect(-L, normal);
	float3 E = normalize(cameraPositionDistance.xyz - currentPosition.xyz);
	float3 V = normalize(-E);
	
	float3 diffuse = pointLightColorFalloff.xyz * saturate(dot(normal, -L));// * pointDistanceFactor(distance(currentPosition.xyz, pointLightPositionRange.xyz));
	float3 specular = specularNs.xyz * pointLightColorFalloff.xyz * pow(saturate(dot(R, V)), specularNs.w * 128.0f);// * pointDistanceFactor(distance(currentPosition.xyz, pointLightPositionRange.xyz));

	OUT.color.xyz = saturate(diffuse + specular);

	return OUT;
}


